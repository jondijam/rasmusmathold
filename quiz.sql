-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2013 at 03:01 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rasmusmath`
--

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `QuizId` int(11) NOT NULL AUTO_INCREMENT,
  `PagesId` int(11) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UserId` int(11) NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`QuizId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`QuizId`, `PagesId`, `LanguageId`, `Created`, `Updated`, `UserId`, `Deleted`) VALUES
(45, 130, 10, '2013-02-25 12:21:38', '2013-02-25 12:21:38', 1, 0),
(46, 131, 10, '2013-02-25 12:22:39', '2013-02-25 18:32:40', 1, 0),
(47, 126, 10, '2013-02-25 12:24:09', '2013-02-25 12:24:09', 1, 0),
(48, 127, 10, '2013-02-25 12:28:04', '2013-02-25 12:31:15', 1, 0),
(49, 125, 10, '2013-02-25 16:26:41', '2013-02-25 18:31:00', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
