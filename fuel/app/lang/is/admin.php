<?php
return array(
    'LoginTitle' => 'Admin innskráning',
	'SignInHeading' => 'Skráðu þig inn',
	'RememberMe' => 'Muna eftir mér',
	'Username' => 'Notendanafn',
	'EmailAddress' => 'Netfang',
	'Password' => 'Lykilorð',
	'SignIn' => 'Sign In',
	'Messages' => 'Messages',
	'Analytics' => 'Greinigar',
	'Notification' => 'Tilkynningar',
	'FrontpageArticle' => 'Efni forsíðu',
	'Draft' => 'Uppkast',
	'Languages' => 'Tungumál',
	'Title' => 'Titill',
	'Save' => 'Vista',
	'Open' => 'Opna'
);

?>