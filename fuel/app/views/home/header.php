<div class="navbar">
	<div class="container">
		<div class="navbar-inner">
			<div class="clearfix">
			<a class="btn btn-navbar visible-phone" data-toggle="collapse" data-target=".nav-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </a>
			<a href="" class="brand"><img src="/assets/img/rasmus_logo_02.png" alt="Rasmus.is" class="logo pull-left" />
				<span class="pull-right headline">
				Rasmus.is
				</span>
			</a>
			<ul class="hidden-phone nav nav-languages">
					<?php 
						foreach ($languages as $languageId => $language) 
						{
							# code...

							?>
							<li>
								<a href="/<?php echo $language['Code'] ?>/">
									<img src="/assets/img/<?php echo $language['ImageSrc'] ?>" height="15px">
								</a>
							</li>
							<?php
						}
					?>
			</ul>
			</div>
			<div class="nav-collapse collapse">
				<ul class="nav visible-phone">
					<li><a href="<?php echo Uri::generate(''); ?>">Home</a></li>
					<li><a href="<?php echo Uri::generate('admin/school'); ?>"><?php echo Lang::get('School')?></a></li>
					<li><a href="<?php echo Uri::generate('admin/indvidual'); ?>"><?php echo Lang::get('Indvidual'); ?></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
