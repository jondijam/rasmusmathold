<div class="container">
	<div class="navbar">
		<div class="navbar-inner">
			<div class="clearfix">
			<a class="btn btn-navbar visible-phone" data-toggle="collapse" data-target=".nav-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </a>
			<a href="" class="brand"><img src="/assets/img/rasmus_logo_02.png" alt="Rasmus.is" class="logo pull-left" />
				<span class="pull-right headline">
				Rasmus.is
				</span>
			</a>
			<ul class="hidden-phone nav nav-languages">
					<?php 
						foreach ($languages as $languageId => $language) 
						{
							# code...

							?>
							<li>
								<a href="/<?php echo $language['Code'] ?>/<?php echo Uri::string() ?>">
									<img src="/assets/img/<?php echo $language['ImageSrc'] ?>" height="15px">
								</a>
							</li>
							<?php
						}
					?>
			</ul>
			<ul class="nav nav-pills nav-profile hidden-phone pull-right">
				<li <?php if($active=="") { ?> class="active" <?php } ?>><a href="<?php echo Uri::generate('home'); ?>"><?php echo Lang::get('Home'); ?></a></li>
				
				<?php 
				if ($auth->member(6) || $auth->member(5) || $auth->member(4) ) 
				{
					# code...
					foreach ($schoolResult as $schoolId => $schoolEntry) 
					{
						?>
						<li <?php if($active=="school") { ?> class="active" <?php } ?>> <a href="<?php echo Uri::generate('home/school/'.$schoolEntry["SchoolId"].''); ?>"><?php echo $schoolEntry["Name"]; ?></a></li>	
						<?php
					}
				} 
				?>
				<li <?php if($active=="profile") { ?> class="active" <?php } ?>><a href="<?php echo Uri::generate('home/profile/'); ?>"><?php echo Lang::get("Profile"); ?></a></li>
				<li><a href="/home/logout"><?php echo Lang::get('Logout'); ?></a></li>
			</ul>
			</div>
		</div>
	</div>
</div>