<meta charset="UTF-8" />
    
         <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="description" content="<? echo Lang::get('MetaDescription'); ?>" />
        <title></title>

        <meta name="keywords" content="<? echo Lang::get('MetaKeywords')?>" />
        <meta name="author" content="rasmus-math.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <!--  Mobile viewport optimized: j.mp/bplateviewport -->
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/home.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-responsive.css">
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>