<!DOCTYPE html>
<html lang="">
	<head>
		<?php echo $head;?>
	</head>
	<body>
		<header>
			<?php echo $header; ?>
		</header>
		<div class="banner hidden-phone">
			<div class="container">
				<div class="wrap row">
					<div class="span6">
						<h1>
							<?php echo Lang::get('Slogan'); ?>
						</h1>
					</div>
					
		            <div class="bannerImage span6" >
		                	<img src="/assets/img/computer.png" alt="" />
		             </div>
				</div>
            </div>
		</div>
		<nav class="hidden-phone">
			<?php echo $nav; ?>
		</nav>
		<div class="container">
			<?php echo $content; ?>
		</div>
		<footer class="navbar-fixed-bottom">
		</footer>

		<script type="text/javascript" src="/assets/js/jquery.js"></script>
		<script type="text/javascript" src="/assets/js/bootstrap.js"></script>		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
		<script type="text/javascript">
		$('.sidebar').height($('.content').height());

		</script>
		<script type="text/javascript">
			var schoolsNumber = 0;
			var studentsNumber = 0;	
			var teachersNumber = 0;
			var teacherUnitPrice = 0;
			var teacherUnitMonthPrice = 0;
			var teacherUnitYearPrice = 0;
			var teacherTotalYearPrice = 0;
			var teacherTotalMonthPrice = 0;
			var studentUnitMonthPrice = 0;
			var studentTotalMonthPrice = 0;
			var studentTotalYearPrice = 0;
			var studentUnitYearPrice = 0;
			var schoolUnitPrice = 0;
			var schoolUnitMonthPrice = 0;
			var schoolUnitYearPrice = 0;
			var totalMonthPrice = 0
			var totalYearPrice = 0;
			var addSchoolUrl = '<?php echo Uri::generate("home/addSchool") ?>';
			var currencyName = "";
			var vatMonth = 0;
			var vatYear = 0;
			var totalVatMonth = 0;
			var totalVatYear = 0; 
			schoolsNumber = $('.school').size();

			function addSchool()
			{
				schoolsNumber++;

				$('.schoolsNumber').text(schoolsNumber);

				$.post(addSchoolUrl ,{schoolNumber:schoolsNumber},function(data){
					$('.schools').append(data);
				})


				schoolUnitMonthPrice = $('.priceSchoolMonth').attr('data-priceSchoolMonth');
				schoolUnitYearPrice = $('.priceSchoolYear').attr('data-priceSchoolYear');

				schoolTotalMonthPrice = schoolsNumber * parseInt(schoolUnitMonthPrice);
				$('.priceSchoolMonth').text(schoolTotalMonthPrice);

				schoolTotalYearPrice = schoolsNumber * parseInt(schoolUnitYearPrice);
				$('.priceSchoolYear').text(schoolTotalYearPrice);

				studentTotalMonthPrice = parseInt($('.priceStudentMonth').text()); 
				teacherTotalMonthPrice = parseInt($('.priceTeacherMonth').text());

				studentTotalYearPrice = parseInt($('.priceStudentYear').text()); 
				teacherTotalYearPrice = parseInt($('.priceTeacherYear').text());

				TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPrice, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)
				

				//$('.studentsNumber').text(studentsNumber);
			}

			function removeSchool()
			{
				schoolsNumber--;
				$('.school:last').hide();
				$('.schoolsNumber').text(schoolsNumber);



				schoolUnitMonthPrice = $('.priceSchoolMonth').attr('data-priceSchoolMonth');
				schoolUnitYearPrice = $('.priceSchoolYear').attr('data-priceSchoolYear');

				schoolTotalMonthPrice = schoolsNumber * parseInt(schoolUnitMonthPrice);
				$('.priceSchoolMonth').text(schoolTotalMonthPrice);

				schoolTotalYearPrice = schoolsNumber * parseInt(schoolUnitYearPrice);
				$('.priceSchoolYear').text(schoolTotalYearPrice);

				studentTotalMonthPrice = parseInt($('.priceStudentMonth').text()); 
				teacherTotalMonthPrice = parseInt($('.priceTeacherMonth').text());

				studentTotalYearPrice = parseInt($('.priceStudentYear').text()); 
				teacherTotalYearPrice = parseInt($('.priceTeacherYear').text());

				TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPrice, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)
				
			}


			function TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPric, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)
			{
				totalMonthPrice = (teacherTotalMonthPrice + studentTotalMonthPrice + schoolTotalMonthPrice);
				totalYearPrice = (teacherTotalYearPrice + studentTotalYearPrice + schoolTotalYearPrice);

				$('.totalMonth').text(totalMonthPrice);
				$('.totalYear').text(totalYearPrice);

					vatMonth = Math.round((totalMonthPrice * 0.255)*100)/100;
					vatYear = Math.round((totalYearPrice * 0.255)*100)/100;
					
					totalVatMonth = Math.round((totalMonthPrice * 1.255) * 100)/100;
					totalVatYear = Math.round((totalYearPrice * 1.255)*100)/100;

					$('.monthVat').text(vatMonth);
					$('.yearVat').text(vatYear);

					$('.totalMonthVat').text(totalVatMonth);
					$('.totalYearVat').text(totalVatYear);
			}

			
			$(document).on('change', '.student', function(){
				
				studentsNumber == 0;

				for (var i = 1; i <= schoolsNumber; i++) 
				{	
					

					studentsNumber += parseInt($('#student'+i+'').val());

					
				};

			
				$('.studentsNumber').text(studentsNumber);
				

				studentUnitMonthPrice = $('.priceStudentMonth').attr('data-priceStudentMonth');
				studentUnitYearPrice = $('.priceStudentYear').attr('data-priceStudentYear');

				studentTotalMonthPrice = studentsNumber * parseInt(studentUnitMonthPrice);
				$('.priceStudentMonth').text(studentTotalMonthPrice);

				studentTotalYearPrice = studentsNumber * parseInt(studentUnitYearPrice);
				$('.priceStudentYear').text(studentTotalYearPrice);

				teacherTotalMonthPrice = parseInt($('.priceTeacherMonth').text()); 
				schoolTotalMonthPrice = parseInt($('.priceSchoolMonth').text());

				teacherTotalYearPrice = parseInt($('.priceTeacherYear').text()); 
				schoolTotalYearPrice = parseInt($('.priceSchoolYear').text());

				TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPrice, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)



				studentsNumber = 0;

			});

			$(document).on('change', '.teacher', function(){
				
				teachersNumber == 0;

				for (var i = 1; i <= schoolsNumber; i++) 
				{	
					teachersNumber += parseInt($('#teacher'+i+'').val());
				};

				teacherUnitMonthPrice = $('.priceTeacherMonth').attr('data-priceTeacherMonth');
				teacherUnitYearPrice = $('.priceTeacherYear').attr('data-priceTeacherYear');

				teacherTotalMonthPrice = teachersNumber * parseInt(teacherUnitMonthPrice);
				$('.priceTeacherMonth').text(teacherTotalMonthPrice);

				teacherTotalYearPrice = teachersNumber * parseInt(teacherUnitYearPrice);
				$('.priceTeacherYear').text(teacherTotalYearPrice);

				studentTotalMonthPrice = parseInt($('.priceStudentMonth').text()); 
				schoolTotalMonthPrice = parseInt($('.priceSchoolMonth').text());

				studentTotalYearPrice = parseInt($('.priceStudentYear').text()); 
				schoolTotalYearPrice = parseInt($('.priceSchoolYear').text());
				TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPrice, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)
				
				$('.teachersNumber').text(teachersNumber);
				teachersNumber = 0;

			});

			$('.currency').change(function(){

				currencyName = $(this).val();			
				$.getJSON('/home/school/priceList/'+currencyName+'',null,function(data){


					schoolsNumber = parseInt($('.schoolsNumber').text());
					studentsNumber = parseInt($('.studentsNumber').text());
					teachersNumber = parseInt($('.teachersNumber').text());

					schoolUnitMonthPrice = data.priceSchoolMonth;
					schoolTotalMonthPrice = schoolsNumber * schoolUnitMonthPrice;

					studentUnitMonthPrice = data.priceStudentMonth;
					studentTotalMonthPrice = data.priceStudentMonth * studentsNumber;

					teacherUnitMonthPrice = data.priceTeacherMonth;
					teacherTotalMonthPrice = data.priceTeacherMonth * teachersNumber;

					$('.priceSchoolMonth').attr('data-priceSchoolMonth', schoolUnitMonthPrice);
					$('.priceSchoolMonth').text(schoolTotalMonthPrice);

					$('.priceStudentMonth').attr('data-priceStudentMonth', studentUnitMonthPrice);
					$('.priceStudentMonth').text(studentTotalMonthPrice);

					$('.priceTeacherMonth').attr('data-priceTeacherMonth', teacherUnitMonthPrice);
					$('.priceTeacherMonth').text(teacherTotalMonthPrice);

					schoolUnitYearPrice = data.priceSchoolYear;
					schoolTotalYearPrice = schoolsNumber * schoolUnitYearPrice;

					studentUnitYearPrice = data.priceStudentYear;
					studentTotalYearPrice = data.priceStudentYear * studentsNumber;

					teacherUnitYearPrice = data.priceTeacherYear;
					teacherTotalYearPrice = data.priceTeacherYear * teachersNumber;

					$('.priceSchoolYear').attr('data-priceSchoolYear', schoolUnitYearPrice);
					$('.priceSchoolYear').text(schoolTotalYearPrice);

					$('.priceStudentYear').attr('data-priceStudentYear', studentUnitYearPrice);
					$('.priceStudentYear').text(studentTotalYearPrice);

					$('.priceTeacherYear').attr('data-priceTeacherYear', teacherUnitYearPrice);
					$('.priceTeacherYear').text(teacherTotalYearPrice);

					TotalCalculation(teacherTotalMonthPrice, studentTotalMonthPrice, schoolTotalMonthPrice, teacherTotalYearPrice, studentTotalYearPrice, schoolTotalYearPrice)
				



				}, "json");
			});

			$('.payerCountry').change(function(data){
				var countryId = $(this).val();

				if (countryId == '99') 
				{
					$('.vat').removeClass('hidden');
					$('.totalVat').removeClass('hidden');
				}
				else
				{
					$('.vat').addClass('hidden');
					$('.totalVat').addClass('hidden');
				}
			});

			var contactId = 0;
			var payerId = 0;
			var terms = 0;
			var schoolId = new Array();
			var contactName = "";
			var contactEmail = "";
			var contactPosition = "";
			var contactPhone = "";
			var contactUsername = "";
			var contactPassword = "";
			var contactPassConf = "";
			
			var payerName = "";
			var payerCustomerNumber = "";
			var payerAddress = "";
			var payerEmail = "";
			var payerZip = "";
			var payerCity = "";
			var payerCountry = 0;
			var term = 0;
			var plan = 0;
			var newsletter = 0;
			var schoolSize = 0;

			var schoolId = new Array();
			var schoolName = new Array();
			var schoolCustomerNumber = new Array();
			var schoolAddress = new Array();
			var schoolZip = new Array();
			var schoolCity  = new Array();
			var schoolCountry = new Array();
			var schoolReference = new Array();
			var schoolTeachers =  new Array();
			var schoolStudents = new Array(); 
			var subscriptionPlan =  new Array();
			var currency = new Array();
			var payment = new Array();
			var payerPayment = 0;
			var payerCurrency = 0;
			var EanNumber = new Array();
			var count = 0;

			$('.saveSchool').click(function(e)
			{
				e.preventDefault();

				contactName = $('.contactName').val();
				contactEmail = $('.contactEmail').val();
				contactPosition = $('.contactPosition').val();
				contactPhone = $('.contactPhone').val();
				contactUsername = $('.contactUsername').val();
				contactPassword = $('.contactPassword').val();
				contactPassConf = $('.contactPassConf').val();
				payerName = $('.payerName').val();
				payerCustomerNumber = $('.payerCustomerNumber').val();
				payerAddress = $('.payerAddress').val();
				payerEmail = $('.payerEmail').val();
				payerZip = $('.payerZip').val();
				payerCity = $('.payerCity').val();
				payerCountry = $('.payerCountry').val();
				plan = $('input:radio[name=plan]:checked').val();
				payerPaymentMethod = $('.payment').val();
				payerCurrency = $('.currency').val();

				schoolSize = $('.school').size();

				if ($('.newsletter').is(':checked')) 
				{
					newsletter = 1;
				};

				if ($('.term').is(':checked'))
				{
					term = 1;
				};

				if (contactId == 0)
				{
					 $.ajax({
					 	url:  '<?php echo Uri::generate("home/registration/saveContact") ?>',
			          	type: "POST",
			          	dataType:"json",
			          	data: {'contactName':contactName, 'contactEmail':contactEmail, 'contactPosition':contactPosition, 'contactPhone':contactPhone, 'contactUsername':contactUsername, 'contactPassword':contactPassword, 'contactPassConf':contactPassConf, 'newsletter':newsletter, 'terms':term },
			          	success: function(data)
			          	{

							if (data.errorBool == 0) 
							{
								contactId = data.userId;

								if (contactId > 0 && payerId == 0)
								{	
									$.ajax({
							          url:  '<?php echo Uri::generate("home/registration/savePayer") ?>',
							          type: "POST",
							          dataType:"json",
							          data: {'payerName':payerName, 'payerEmail':payerEmail,'payerCustomerNumber':payerCustomerNumber, 'payerAddress':payerAddress, 'payerZip':payerZip,'payerCity':payerCity, 'payerCountry':payerCountry, 'paymentMethod':payerPaymentMethod,'currency': payerCurrency ,'subscriptionPlan':plan},
							          async:false,
							          success: function(data)
							          {
							          		if (data.errorBool == 0) 
											{
												payerId = data.payerId;
												$('.erroPayerName').addClass('hidden');
												$('.errorPayerCustomerNumber').addClass('hidden');
												$('.errorPayerAddress').addClass('hidden');
												$('.errorPayerEmail').addClass('hidden');
												$('.errorPayerZip').addClass('hidden');
												$('.errorPayerCity').addClass('hidden');

											}
											else
											{
												payerId = 0;
											}

											if(data.errorBool == 1 && data.nameError != "")
											{
												$('.erroPayerName').removeClass('hidden');
												$('.errorPayerName').html(data.nameError);
											}

											if(data.errorBool == 1 && data.customerNumberError != "")
											{
												$('.errorPayerCustomerNumber').removeClass('hidden');
												$('.errorPayerCustomerNumber').html(data.customerNumberError);
											}

											if(data.errorBool == 1 && data.addressError != "")
											{
												$('.errorPayerAddress').removeClass('hidden');
												$('.errorPayerAddress').html(data.addressError);
											}


											if(data.errorBool == 1 && data.emailError != "")
											{
												$('.errorPayerEmail').removeClass('hidden');
												$('.errorPayerEmail').html(data.emailError);
											}

											if(data.errorBool == 1 && data.zipError != "")
											{
												$('.errorPayerZip').removeClass('hidden');
												$('.errorPayerZip').html(data.zipError);
											}

											if(data.errorBool == 1 && data.cityError != "")
											{
												$('.errorPayerCity').removeClass('hidden');
												$('.errorPayerCity').html(data.cityError);
											}	

							          }
							      });
									
								}

								if (contactId > 0 && payerId > 0)
								{	
									for (var i = 1; i <= schoolSize; i++) 
									{
										
										schoolName[i] = $('input[name="schoolName'+i+'"]').val();	
										schoolCustomerNumber[i] = $('input[name="schoolCustomerNumber'+i+'"]').val();	
										schoolAddress[i] = $('input[name="schoolAddress'+i+'"]').val();	
										schoolZip[i] = $('input[name="schoolZip'+i+'"]').val();	
										schoolCity[i] = $('input[name="schoolCity'+i+'"]').val();
										schoolCountry[i] = $('.schoolCountry'+i+'').val();
										schoolReference[i] = $('input[name="schoolReference'+i+'"]').val();	
										schoolTeachers[i] = $('#teacher'+i).val();	
										schoolStudents[i] = $('#student'+i).val();
										subscriptionPlan[i] = plan;
										payment[i] = $('.payment').val();
										currency[i] = $('.currency').val();
										count++; 

										if (typeof schoolId[i] == "undefined" || schoolId[i] == 0) 
										{
											$.ajax(
										 	{
										 		url:  '<?php echo Uri::generate("home/registration/saveSchool") ?>',
									          	type: "POST",
									          	dataType:"json",
									          	data: {'name': schoolName[i], 'customerNumber':schoolCustomerNumber[i], 'address':schoolAddress[i],'city': schoolCity[i]  ,'zip':schoolZip[i], 'country':schoolCountry[i], 'reference':schoolReference[i],'teacherCount':schoolTeachers[i],'studentCount':schoolStudents[i], 'payerId':payerId, 'contactId':contactId, 'contactName':contactName, 'contactEmail':contactEmail ,'subscriptionPlan':subscriptionPlan[i], 'currency':currency[i], 'paymentMethod': payment[i]  },
									          	async:false,
									          	success: function(data){
									          		if (data.errorBool == 0) 
													{
														schoolId[i] = data.schoolId;
														$('.schoolName'+i+'').addClass('hidden');
														$('.schoolCustomerNumber'+i+'').addClass('hidden');
														$('.schoolAddress'+i+'').addClass('hidden');
														$('.schoolZip'+i+'').addClass('hidden');
														$('.schoolCity'+i+'').addClass('hidden');

														if (count == schoolSize) {

															$.post('<?php echo Uri::generate("home/school/registration"); ?>',{}, function(data){
																$('.content').html(data);
															})
															

														};

													};
													if (data.errorBool == 1 && data.schoolName != "") 
													{
														console.log(data.schoolName);
														$('.schoolName'+i+'').removeClass('hidden');
														$('.schoolName'+i+'').html(data.schoolName);


													};

													if (data.errorBool == 1 && data.customerNumber != "") 
													{
														
														$('.schoolCustomerNumber'+i+'').addClass('hidden');
														$('.schoolCustomerNumber'+i+'').html(data.customerNumber);

													};


													if (data.errorBool == 1 && data.address != "") 
													{
														$('.schoolAddress'+i+'').removeClass('hidden');
														$('.schoolAddress'+i+'').html(data.customerNumber);
													};

													if (data.errorBool == 1 && data.schoolZip != "") 
													{
														$('.schoolZip'+i+'').removeClass('hidden');
														$('.schoolZip'+i+'').html(data.schoolZip);


													};

													if (data.errorBool == 1 && data.schoolCity != "") 
													{
														$('.schoolCity'+i+'').removeClass('hidden');
														$('.schoolCity'+i+'').html(data.schoolCity);
													};

									          	}
										 	});
										};

								
										
									}

								};
							}
							else
							{
								contactId = 0;
							}

							if(data.errorBool == 1 && data.nameError != "")
							{
								$('.errorContactName').removeClass('hidden');
								$('.errorContactName').html(data.nameError);
							}

							if(data.errorBool == 1 && data.passwordError != "")
							{
								$('.errorPassword').removeClass('hidden');
								$('.errorPassword').html(data.passwordError);
							}

							if(data.errorBool == 1 && data.passwordConf != "")
							{
								$('.errorPassConf').removeClass('hidden');
								$('.errorPassConf').html(data.passwordConf);
							}


							if(data.errorBool == 1 && data.emailError != "")
							{
								$('.errorEmail').removeClass('hidden');
								$('.errorEmail').html(data.emailError);
							}

							if(data.errorBool == 1 && data.usernameError != "")
							{
								$('.errorUsername').removeClass('hidden');
								$('.errorUsername').html(data.usernameError);
							}

							if(data.errorBool == 1 && data.termsError != "")
							{
								$('.errorTerm').removeClass('hidden');
								$('.errorTerm').text('<?php echo Lang::get('ErrorTermMessage'); ?>');
							}
							
			          	}


					 });
				}

		

				count = 0;

				

			});
				
		</script>
	</body>
</html>