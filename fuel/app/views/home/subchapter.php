<div class="container">
	<div class="row">
			<div class="span2">
				<ul class="nav nav-tabs nav-stacked">
					<?php foreach ($chapters as $chapterId => $chapter) 
					{
						?>
							<li <?php if( $chapterPage == $chapter['Pages']){?> class="active " <?php } ?>>
								<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter['Pages'].'');  ?>"><?php echo $chapter['Title']; ?></a>

								<?php if($chapterPage == $chapter['Pages']){
									
									if($subChapterList)
									{
									?>
									<ul class="nav nav-list" role="menu" aria-labelledby="dLabel">
									<?php
									foreach ($subChapterList as $subChapterId => $subChapterItem) 
									{
										?>
										<li <?php if($subChapterPage == $subChapterItem['Pages']){?> class="active" <?php } ?>>
											<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterItem['Pages'].'');  ?>"><?php echo $subChapterItem['Title'] ?></a>
										</li>
										<?php	# code...
									}
									?>
								</ul>
									<?php
									}
								}?>
							</li>
						<?php
					} ?>
				</ul>
			</div>
			<div class="span9 content">
				<ul class="nav nav-pills">
					<?php foreach ($lessonList as $lessonItem) 
					{
						?>
						<li>
							<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterPage.'/'.$lessonItem['Pages']);  ?>"><?php echo $lessonItem['Title'] ?></a>
						</li>
						<?php
					} ?>

					<?php 
						foreach ($quizList as $quizItem) 
						{
							?>
							<li>
								<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterPage.'/quiz/'.$quizItem['Pages']);  ?>"><?php echo $quizItem['Title'] ?></a>
							</li>
						<?php
						}
					?>
				</ul>
				<div class="entry">
				<?php 
					foreach ($descriptions as $entry) 
					{
						echo $entry['Description'];
					}
				?>
				</div>
			</div>
	</div>
</div>
