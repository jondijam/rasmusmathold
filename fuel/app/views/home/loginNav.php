<nav class="hidden-phone">
	<div class="container">
		 <div class="nav-inner">
		 	<ul class="nav nav-pills">
		 		<?php 
		 			foreach ($schoolLevels as $LinksId => $schoolLevel) 
		 			{
		 				?>
		 					<li <?php if($schoolLevelPage == $schoolLevel["Pages"]){?> class="active" <?php } ?> >
		 						<a href="<?php echo  Uri::generate('home/index/'.$schoolLevel["Pages"].''); ?>"><?php echo $schoolLevel['Title'] ?></a>
		 					</li>
		 				<?php
		 			}
		 		?>
		 	</ul>
		 </div>
	</div>
</nav>