			<div class="school school<?php echo $schoolNumber; ?>">
							<h2><?php echo Lang::get('NewSchool',array('Number'=> $schoolNumber)); ?></h2>
							<div>
							<label><?php echo Lang::get('Name') ?></label>
								<input type="text" name="schoolName<?php echo $schoolNumber; ?>" />
								<div class="alert alert-error hidden schoolName<?php echo $schoolNumber; ?>"></div>
							</div>
							<div>
								<label><?php echo Lang::get('CustomerNumber') ?></label>
								<input type="text" name="schoolCustomerNumber<?php echo $schoolNumber; ?>" />
								<div class="alert alert-error hidden schoolCustomerNumber<?php echo $schoolNumber; ?>"></div>
							</div>
							<div>
								<label><?php echo Lang::get('Address') ?></label>
								<input type="text" name="schoolAddress<?php echo $schoolNumber; ?>" />
								<div class="alert alert-error hidden schoolAddress<?php echo $schoolNumber; ?>"></div>
							</div>
							<div>
								<label><?php echo Lang::get('Zip') ?></label>
								<input type="text" name="schoolZip<?php echo $schoolNumber; ?>" />
								<div class="alert alert-error hidden schoolZip<?php echo $schoolNumber; ?>"></div>
							</div>
							<div>
								<label><?php echo Lang::get('City') ?></label>
								<input type="text" name="schoolCity<?php echo $schoolNumber; ?>" />
								<div class="alert alert-error hidden schoolCity<?php echo $schoolNumber; ?>"></div>
							</div>
							<div>
								<label><?php echo Lang::get('Country') ?></label>
								<select name="schoolCountry<?php echo $schoolNumber; ?>" class="schoolCountry<?php echo $schoolNumber; ?>">
									<?php foreach ($country as $countryId => $countryItem) 
									{
										?>
										<option value="<?php echo $countryId ?>" ><?php echo $countryItem['name_en']; ?></option>
										<?php
										# code...
									} 
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Reference') ?></label>
								<input type="text" name="schoolReference<?php echo $schoolNumber; ?>" />
							</div>
							<div>
								<label><?php echo Lang::get('Teachers') ?></label>
								<select name="schooolTeacher<?php echo $schoolNumber; ?>" id="teacher<?php echo $schoolNumber; ?>" class="teacher teacher<?php echo $schoolNumber; ?>">
										<?php
										for ($i = 0; $i <= 1000; $i++)
										{
											?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>	
											<?php
										}
										?>
									
								</select>
							</div>
							<div>
								<label class="schoolstudents"><?php echo Lang::get('Students') ?></label>
									<select name="student<?php echo $schoolNumber; ?>" id="student<?php echo $schoolNumber; ?>" class="student">
										<?php
										for ($i = 0; $i <= 1000; $i++)
										{
											?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>	
											<?php
										}
										?>
								</select>
							</div>
						</div>