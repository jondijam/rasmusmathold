<div class="container">
	<div class="row">
			<div class="span2">
				<?php 
				if(isset($chapters))
				{
					?>
					<ul class="nav nav-tabs nav-stacked">
					<?php foreach ($chapters as $chapterId => $chapter) 
					{
						?>
							<li><a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter['Pages'].'');  ?>"><?php echo $chapter['Title']; ?></a></li>
						<?php
					} ?>
				</ul>

					<?php
				}
				?>
			</div>
			<div class="span9 content">
				<div class="entry">
					<?php 
					foreach ($descriptions as $entry) 
					{
						echo $entry['Description'];
					}
				?> 
				</div>
				
			</div>
	</div>
</div>
