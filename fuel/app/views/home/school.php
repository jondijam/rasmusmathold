<div class="container">
	<div class="row">
		<div class="span8 content">
			<form>
				<fieldset>
					<h1><?php echo Lang::get("Contact"); ?></h1>
					<div>
						<label>
							<?php echo Lang::get('Name') ?>
						</label>
						<input type="text" name="contactName" class="contactName" /> 
						<div class="alert alert-error hidden errorContactName"></div>						
					</div>
					<div>
						<label><?php echo Lang::get('Email') ?></label>
						<input type="text" name="contactEmail" class="contactEmail" />
						<div class="alert alert-error hidden errorEmail"></div>
					</div>
					<div>
						<label class="checkbox"><input type="checkbox" name="newsletter" class="newsletter" />
							<?php echo Lang::get('Newsletter'); ?>
						</label>
					</div>
					<div>
						<label><?php echo Lang::get('Position') ?></label>
						<input type="text" name="contactposition" class="contactPosition" />
					</div>
					<div>
						<label><?php echo Lang::get('Phone') ?></label>
						<input type="text" name="contactPhone" class="contactPhone" />
					</div>
					<div>
						<label><?php echo Lang::get('Username') ?></label>
						<input type="text" name="contactUsername" class="contactUsername" />
						<div class="alert alert-error hidden errorUsername"></div>
					</div>
					<div>
						<label><?php echo Lang::get('Password') ?></label>
						<input type="password" name="contactPassword" class="contactPassword" />
						<div class="alert alert-error hidden errorPassword"></div>
					</div>
					<div>
						<label><?php echo Lang::get('PasswordConfirmation') ?></label>
						<input type="password" name="contactPassConf" class="contactPassConf" />
						<div class="alert alert-error hidden errorPassConf"></div>
					</div>
					<h1><?php echo Lang::get("Payer"); ?></h1>
					<div>
						<label>
							<?php echo Lang::get('Name') ?>
						</label>
						<input type="text" name="payerName" class="payerName" />
						<div class="alert alert-error hidden errorPayerName"></div> 						
					</div>
					<div>
						<label>
							<?php echo Lang::get('CustomerNumber') ?>
						</label>
						<input type="text" name="payerCustomerNumber" class="payerCustomerNumber" /> 	
						<div class="alert alert-error hidden errorPayerCustomerNumber"></div> 					
					</div>
					<div>
						<label>
							<?php echo Lang::get('Address') ?>
						</label>
						<input type="text" name="payerAddress" class="payerAddress" /> 	
						<div class="alert alert-error hidden errorPayerAddress"></div> 					
					</div>
					<div>
						<label>
							<?php echo Lang::get('Email') ?>
						</label>
						<input type="text" name="payerEmail" class="payerEmail" /> 	
						<div class="alert alert-error hidden errorPayerEmail"></div> 					
					</div>
					<div>
						<label>
							<?php echo Lang::get('Zip') ?>
						</label>
						<input type="text" name="payerZip" class="payerZip" /> 
						<div class="alert alert-error hidden errorPayerZip"></div> 						
					</div>
					<div>
						<label>
							<?php echo Lang::get('City') ?>
						</label>
						<input type="text" name="payerCity" class="payerCity" /> 
						<div class="alert alert-error hidden errorPayerCity"></div> 						
					</div>
					<div>
						<label>
							<?php echo Lang::get('Country') ?>
						</label>
						<select name="payerCountry" class="payerCountry">
							<?php foreach ($country as $countryId => $countryItem) 
									{
										?>
										<option value="<?php echo $countryId ?>" ><?php echo $countryItem['name_en']; ?></option>
										<?php
										# code...
									} 
									?>
						</select>							
					</div>
					<h2><?php echo Lang::get('School'); ?></h2>
					<div class="schools">
						<div class="school school1">
							<div>
								<label><?php echo Lang::get('Name') ?></label>
								<input type="text" name="schoolName1" />
								<div class="alert alert-error hidden schoolName1"></div>
							</div>
							<div>
								<label><?php echo Lang::get('CustomerNumber') ?></label>
								<input type="text" name="schoolCustomerNumber1" />
								<div class="alert alert-error hidden schoolCustomerNumber1"></div>
	
							</div>
							<div>
								<label><?php echo Lang::get('Address') ?></label>
								<input type="text" name="schoolAddress1" />
								<div class="alert alert-error hidden schoolAddress1"></div>
							</div>
							<div>
								<label><?php echo Lang::get('Zip') ?></label>
								<input type="text" name="schoolZip1" />
								<div class="alert alert-error hidden schoolZip1"></div>
							</div>
							<div>
								<label><?php echo Lang::get('City') ?></label>
								<input type="text" name="schoolCity1" />
								<div class="alert alert-error hidden schoolCity1"></div>
							</div>
							<div>								
								<label><?php echo Lang::get('Country') ?></label>
								<select name="schoolCountry1" class="schoolCountry1">
									<?php foreach ($country as $countryId => $countryItem) 
									{
										?>
										<option value="<?php echo $countryId ?>" ><?php echo $countryItem['name_en']; ?></option>
										<?php
										# code...
									} 
									?>

									<option value="">Iceland</option>
								</select>
							</div>
							<div class="hidden">
								<label><?php echo Lang::get('EANNumber') ?></label>
								<input type="text" name="EanNumber1" /> 
							</div>
							<div>
								<label><?php echo Lang::get('Reference') ?></label>
								<input type="text" name="schoolReference1" />
							</div>
							<div>
								<label><?php echo Lang::get('Teachers') ?></label>
								<select name="schooolTeacher1" id="teacher1" class="teacher teacher1">
										<?php
										for ($i = 1; $i <= 1000; $i++)
										{
											?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>	
											<?php
										}
										?>
									
								</select>
							</div>
							<div>
								<label class="schoolstudents"><?php echo Lang::get('Students') ?></label>
									<select name="student1" id="student1" class="student student1">
										<?php
										for ($i = 1; $i <= 1000; $i++)
										{
											?>
											<option value="<?php echo $i; ?>">
												<?php echo $i; ?>
											</option>	
											<?php
										}
										?>
								</select>
							</div>
						</div>
					</div>
					<div>
						<ul class="nav nav-pills">
							<li><a href="javascript:addSchool()"><?php echo Lang::get('AddASchool') ?></a></li>
							<li><a href="javascript:removeSchool()"><?php echo Lang::get('RemoveSchool') ?></a></li>
						</ul>
					</div>
					<h1><?php echo Lang::get("Payement"); ?></h1>
					<div>
						<label class="checkbox">
							<input type="checkbox" name="term" class="term" value="1"  />
							<?php echo Lang::get('Terms') ?>
							<div class="alert alert-error hidden errorTerm"></div>
						</label>
					</div>
					<div>
					<label><?php echo Lang::get('ChoiceCurrency') ?></label>
					<select class="currency" name="currency">
						<?php foreach ($currencyList as $currencyId => $currencyItem) 
						{
						?>
							<option value="<?php echo $currencyItem['CurrencyName']; ?>"><?php echo $currencyItem['CurrencyCode']; ?></option>
								<?php
								# code...
						} ?>
					</select>
					</div>
					<div>
						<label><?php echo Lang::get('Payment') ?></label>
						<select name="payment" class="payment">
							<option value="1"><?php echo Lang::get('Invoice') ?></option>
						</select>
					</div>
					<div>
					<table class="table">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Number') ?></th>
								<th><?php echo Lang::get('Month') ?></th>
								<th><?php echo Lang::get('Year') ?></th>
							</tr>
						</thead>
						<tbody data-unitPrice="<?php echo $priceSchoolMonth; ?>,<?php echo  $priceSchoolYear; ?>,<?php echo $priceTeacherMonth; ?>,<?php echo $priceTeacherYear; ?>,<?php echo $priceStudentMonth; ?>,<?php echo $priceStudentYear; ?>">
							<tr>
								<td><?php echo Lang::get('School') ?></td>
								<td class="schoolsNumber">1</td>
								<td class="priceSchoolMonth" data-priceSchoolMonth="<?php echo $priceSchoolMonth; ?>"><?php echo $priceSchoolMonth; ?> €</td>
								<td class="priceSchoolYear" data-priceSchoolYear="<?php echo  $priceSchoolYear; ?>"><?php echo  $priceSchoolYear; ?> €</td>
							</tr>
							<tr>
								<td><?php echo Lang::get('Teachers') ?></td>
								<td class="teachersNumber">1</td>
								<td class="priceTeacherMonth" data-priceTeacherMonth="<?php echo $priceTeacherMonth; ?>"><?php echo $priceTeacherMonth; ?> €</td>
								<td class="priceTeacherYear" data-priceTeacherYear="<?php echo $priceTeacherYear; ?>"><?php echo $priceTeacherYear; ?> €</td>
							</tr>
							<tr>
								<td><?php echo Lang::get('Students') ?></td>
								<td class="studentsNumber">1</td>
								<td class="priceStudentMonth" data-priceStudentMonth="<?php echo $priceStudentMonth; ?>"><?php echo $priceStudentMonth; ?> €</td>
								<td class="priceStudentYear" data-priceStudentYear="<?php echo $priceStudentYear; ?>"><?php echo $priceStudentYear; ?> €</td>
							</tr>
							<tr>
								<td><?php echo Lang::get('Total') ?></td>
								<td></td>
								<td class="totalMonth"><?php echo $totalPriceMonth; ?> €</td>
								<td class="totalYear"><?php echo $totalPriceYear; ?> €</td>
							</tr>
							<tr class="vat hidden">
								<td>
									<?php echo Lang::get('VAT') ?>
								</td>
								<td>
									25,5%
								</td>
								<td class="monthVat">
									<?php echo round($totalPriceMonth * 0.255); ?> €
								</td>
								<td class="yearVat">
									<?php echo round($totalPriceYear * 0.255); ?> €
								</td>
							</tr>
							<tr class="totalVat hidden">
								<td>
									<?php echo Lang::get('TotalWithVat') ?>
								</td>
								<td>

								</td>
								<td class="totalMonthVat">
									<?php echo round($totalPriceMonth * 1.255); ?> €
								</td>
								<td class="totalYearVat">
									<?php echo round($totalPriceYear * 1.255); ?> €
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
								</td>
								<td>
									<input type="radio" name="plan" value="1" checked />
								</td>
								<td>
									<input type="radio" name="plan" value="0" />
								</td>
							</tr>
						</tbody>
					</table>
					</div>	
				</fieldset>
				<div clas="buttonrow">
					<div class="saveImage hidden"><img src="/assets/img/ajax-loader.gif"></div>
					<input type="submit" class="btn btn-primary saveSchool" value="<?php echo Lang::get('Save'); ?>">
				</div>
			</form>

		</div>
		<div class="span3 sidebar">
			<h2><?php echo Lang::get('SignIn'); ?></h2>
			<form action="<?php echo Uri::generate('home/login'); ?>" method="post">
				<fieldset>
					<div>
						<label><?php echo Lang::get('Username'); ?></label>
						<input type="text" name="username" value="" />
					</div>
					<div>
						<label><?php echo Lang::get('Password'); ?></label>
						<input type="password" name="password" value="" /> 
					</div>
				</fieldset>
				<div class="buttonrow">
					<input type="submit" class="btn btn-primary" value="<?php echo Lang::get('SignIn'); ?>"/> 
				</div>
			</form>
					<h2><?php echo Lang::get('Notification'); ?></h2>
				<?php 
				foreach ($notifications as $notification) 
				{
					?>
					<div class="story">
						<h4><?php echo $notification['Title']; ?></h4>
						<?php echo $notification['Text'] ?>
					</div>
					<?php
				}
				?>
		</div>
	</div>
</div>