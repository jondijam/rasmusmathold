<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
     google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Month', <?php for ($i=1; $i <= $attemptCount ; $i++) {?> '<?php echo Lang::get("Attempt", array("Number" => $i)); ?>', <?php  } ?>],
          
          <?php 
          foreach ($userQuizResult as $userQuizKey => $userQuizEntry) 
          {
            ?>
            ['<?php echo $userQuizEntry["SubChapter"]; ?> / <?php echo $userQuizEntry["Title"] ;?>', <?php for ($i=1; $i <= $attemptCount ; $i++) { ?> <?php if(isset($grade[$userQuizKey][$i])) { echo $grade[$userQuizKey][$i]; }else{ echo 0; } ?>, <?php  } ?>],
            <?php
            # code...
          }
          ?>
        ]);

        var options = {
          title : '<?php echo Lang::get("YourGrade"); ?>',
          vAxis: {title: "<?php echo Lang::get('Grades'); ?>"},
          hAxis: {title: "<?php echo Lang::get('Quiz'); ?>"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
      
</script>
<div class="container">
	<div class="row">
		<div class="span12 content">
			<div class="span10">
			<!-- Kynning á rasmus þegar notandi kemur hingað fyrsta skipti annars sýna honum tölfræði -->
			<h1><?php echo Lang::get("Welcome"); ?></h1>

			
			<!-- Check google graph if user has logged in -->			
      <?php 
      if ($firstTimeLogin == 1) 
      {
        # code...
        ?>
        <div>
            <?php echo Lang::get('Content'); ?>
        </div>
        <?php
      }
      else
      {
        if(count($userQuizResult) > 0)
        {
          ?>
          <h2><?php echo Lang::get('YourStatistics'); ?></h2>
          <div id="chart_div"></div>
          <?php
        }
      }
      ?>
    

			</div>
		
		</div>
	</div>
</div>