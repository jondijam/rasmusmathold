<div class="wrapper">
	<div class="row">
		<div class="span8 content">
			<?php 
			foreach ($articles as $article) 
			{
				?>
				<article>

				<h3><?php echo $article['Title']; ?></h3>
				<?php
				echo $article['Text'];
				?>
				</article>
				<?php
			}
		?>
		</div>
		<div class="span3 sidebar">
			<h2><?php echo Lang::get('SignIn'); ?></h2>
			<form action="<?php echo Uri::generate('home/login'); ?>" method="post">
				<fieldset>
					<div>
						<label><?php echo Lang::get('Username'); ?></label>
						<input type="text" id="inputUsername" name="username" class="input" value="" />
					</div>
					<div>
						<label>
							<?php echo Lang::get('Password'); ?>
						</label>
						<input type="password" id="inputPassword" name="password" class="input" value="" /> 
					</div>
				</fieldset>
				<div class="buttonrow control-group">
					<div class="controls">
					<input type="submit" class="btn btn-primary" value="<?php echo Lang::get('SignIn'); ?>"/> 
					</div>
				</div>
			</form>
			<h2><?php echo Lang::get('Notification'); ?></h2>
				<?php 
				foreach ($notifications as $notification) 
				{
					?>
					<div class="story">
						<h4><?php echo $notification['Title']; ?></h4>
						<?php echo $notification['Text'] ?>
					</div>
					<?php
				}
				?>
		</div>
	</div>
</div>