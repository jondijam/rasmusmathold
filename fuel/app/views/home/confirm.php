<div class="container">
	<div class="row">
		<div class="span8 content">
			<h2>Confirmation</h2>
			<p>We have now confirm your email</p>
		</div>
		<div class="span3 sidebar">
			<h2><?php echo Lang::get('SignIn'); ?></h2>
			<form action="<?php echo Uri::generate('home/login'); ?>" method="post">
				<fieldset>
					<div>
						<label><?php echo Lang::get('Username'); ?></label>
						<input type="text" name="username" value="" />
					</div>
					<div>
						<label><?php echo Lang::get('Password'); ?></label>
						<input type="password" name="password" value="" /> 
					</div>
				</fieldset>
				<div class="buttonrow">
					<input type="submit" class="btn btn-primary" value="<?php echo Lang::get('SignIn'); ?>"/> 
				</div>
			</form>
					<h2><?php echo Lang::get('Notification'); ?></h2>
				<?php 
				foreach ($notifications as $notification) 
				{
					?>
					<div class="story">
						<h4><?php echo $notification['Title']; ?></h4>
						<?php echo $notification['Text'] ?>
					</div>
					<?php
				}
				?>
		</div>
	</div>
</div>