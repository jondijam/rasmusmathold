<div class="container">
	<div class="row">
			<div class="span2">
				<ul class="nav nav-tabs nav-stacked">
					<?php foreach ($chapters as $chapterId => $chapter) 
					{
						?>
							<li <?php if( $chapterPage == $chapter['Pages']){?> class="active " <?php } ?>>
								<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter['Pages'].'');  ?>"><?php echo $chapter['Title']; ?></a>

								<?php if($chapterPage == $chapter['Pages']){
									
									if($subChapterList)
									{
									?>
									<ul class="nav nav-list" role="menu" aria-labelledby="dLabel">
									<?php
									foreach ($subChapterList as $subChapterId => $subChapterItem) 
									{
										?>
										<li <?php if($subChapterPage == $subChapterItem['Pages']){?> class="active" <?php } ?>>
											<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterItem['Pages'].'');  ?>"><?php echo $subChapterItem['Title'] ?></a>
										</li>
										<?php	# code...
									}
									?>
								</ul>
									<?php
									}
								}?>
							</li>
						<?php
					} ?>
				</ul>
			</div>
			<div class="span9">
				<ul class="nav nav-pills">
					<?php foreach ($lessonList as $lessonItem) 
					{
						?>
						<li>
							<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterPage.'/'.$lessonItem['Pages']);  ?>"><?php echo $lessonItem['Title'] ?></a>
						</li>
						<?php
					} ?>

					<?php 
						foreach ($quizList as $quizItem) 
						{
							?>
							<li>
								<a href="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterPage.'/quiz/'.$quizItem['Pages']);  ?>"><?php echo $quizItem['Title'] ?></a>
							</li>
						<?php
						}
					?>
				</ul>
				<div class="entry">
				
						<?php 
						foreach ($quiz as $quizId => $quizEntry) 
						{
							?>
							<form action="<?php echo Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapterPage.'/'.$subChapterPage.'/quiz/'.$quizPage.'/answers') ?>" method="post">
							<fieldset>

							<?php
							foreach ($questions[$quizId] as $questionId => $question) 
							{
							?>
								<div class="problems problem<?php echo $questionId; ?>">
									<div class="problemHeader">
										<div class="proplemHeadline">
											<h2><?php echo Lang::get('Problem') ?><?php echo $question['Problem']; ?></h2>
										</div>
									</div>
									<div class="questions">
											<?php 
											echo $question['Text'];
											?>
									</div>
									<div class="options">
										<?php
										$counter = 0;
										$answercount = 0;
										$sum = 0;
										foreach ($options[$quizId][$questionId] as $optionId => $option)
										{
											
											if ($option['CorrectAnswer'] == 1) 
											{
												$answercount++;		# code...
											}
											$option['CorrectAnswer'];
										
											//echo $answercounter[$questionId];
											
										}
										$answercounter[$questionId] = $answercount;

										foreach ($options[$quizId][$questionId] as $optionId => $option) 
										{
											$counter++;

											?>
											<div class="option option<?php echo $question['Problem']; ?> clearfix" data-answercounter="<?php echo $answercount; ?>">
												<div class="optionCounter">
													<label class="pull-left" style="padding-top:1px; padding-right:3px;"><?php echo $counter; ?>.</label>
													<input type="<?php if ($answercount == 1) {?>radio<?php } else{ ?>checkbox<?php } ?>" class="pull-left correct<?php echo $option["Problem"]; if ($answercount != 1) { echo $counter; } ?>" name="<?php echo $option["Problem"]; if ($answercount != 1) { echo $counter; } ?>" <?php if($answercount == 1 && $counter == 1){?> checked <?php } ?> value="<?php echo $optionId; ?>" />
											
												</div>
												<div class="optionText pull-left">
													<?php echo $option["Text"]; ?>
												</div>
											</div>

											<?php
										}
										
										?>
									</div>
								</div>
							<?php
								# code...
							}
							?>
							</fieldset>
							<div class="buttonrow">
								<input type="submit" class="btn btn-primary sendAnswers" data-quizId="<?php echo $quizId; ?>"/>
							</div>
						</form>

							<?php
						}
						?>
				</div>
			</div>
	</div>
</div>
