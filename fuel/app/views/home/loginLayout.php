<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php echo $head; ?>
</head>
<body>
	<header>
	<?php 
		echo $header;
	?>
	</header>
	<?php 
		echo $nav;
	?>
	<?php 
		echo $content;
	?>


<?php echo $footer; ?>
</body>
</html>