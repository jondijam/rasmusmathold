<div class="container">
	<div class="wrap">
		<div class="row">
			<div class="box span12">
				<div class="box_headline"><h3><?php echo Lang::get('Pages'); ?></h3></div>
				<div class="box_content">
					<table class="table table-striped clearfix" id="schoolLevelPages">
						<thead>
							
							<tr>
								<th></th>
								<th><?php echo Lang::get('Name'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($pages as $pagesId => $page) 
							{
								?>
									<tr id="<?php echo $pagesId; ?>" data-id="<?php echo $pagesId; ?>">
										<td><?php echo $pagesId; ?></td>
										<td data-oldpagesname="<?php echo $page['Name']?>"><?php echo $page['Name']?></td>
										<td>
											<a href="<?php echo Uri::generate('admin/pages/delete') ?>" class="table-action-deletelink btn btn-danger "><?php echo Lang::get('Delete'); ?></a>
										</td>
									</tr>
								<?php								
							} 
							?>
						</tbody>
					</table>
				</div>
				<div class="box_new">
					<form action="<?php echo Uri::generate('admin/pages/save') ?>" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Name'); ?></label>
								<input type="text" name="pagesName" class="pagesName" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary savePages" data-branchOrder="1" value="<?php echo Lang::get('Save'); ?>" />
						</div>
					</form>
				</div>
			</div>
			<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Links'); ?></h3>
				</div>
				<div class="box_content">
					<table class="table table-striped clearfix" id="schoolLevelLinks">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Language'); ?></th>
								<th><?php echo Lang::get('Title'); ?></th>
								<th><?php echo Lang::get('Pages'); ?></th>
								<th><?php echo Lang::get('Order'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($links as $linksId => $link) 
							{
							?>
								<tr id="links<?php echo $linksId; ?>" data-id="<?php echo $linksId; ?>" class="links<?php echo $linksId; ?>">
									<td><?php echo $linksId?></td>
									<td><?php echo $link['Code']; ?></td>
									<td><?php echo $link['Title']; ?></td>
									<td><?php echo $link['Name']; ?></td>
									<td><?php echo $link['Orders']?></td>
									<td><a href="<?php echo Uri::generate('admin/links/delete') ?>" class="table-action-deletelink btn btn-danger" data-id="1"><?php echo Lang::get('Delete'); ?></a></td>
								</tr>
							<?php
							} ?>
						</tbody>					
					</table>
				</div>
				<div class="box_new">
					<form action="/admin/links/save" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Pages'); ?></label>
								<select name="pages" class="pages">
									<?php foreach ($pages as $pagesId => $page) 
									{
										?>
										<option value="<?php echo $pagesId; ?>"><?php echo $page['Name']; ?></option>
										<?php

									} ?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Languages'); ?></label>
								<select name="languages" class="languages">
									<?php
									foreach ($languages as $languageId => $language) 
									{?>

										<option value="<?php echo $languageId; ?>"><?php echo $language['Name']; ?></option>
									<?php
										# code...
									}
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Order'); ?></label>
								<input name="order" class="order" type="text" />
							</div>
							<div>
								<label><?php echo Lang::get('Title'); ?></label>
								<input type="text" class="title" name="title" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary saveLinks" data-branchOrder="1" value="<?php echo Lang::get('Save'); ?>" /> 
						</div>
					</form>
				</div>
			</div>
			<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Description'); ?></h3>
				</div>
				<div class="box_content">
					<?php 
						foreach ($descriptions as $descriptionId => $description) 
						{
							?>
							<div class="gadget" id="descriptionGadget<?php echo $descriptionId; ?>">
								<div class="titlebar">
									<h3>
										<span class="descriptionOpen" data-id="<?php echo $descriptionId; ?>"><?php echo $description['LanguageName'] ?> / <?php echo $description['Title'] ?></span>
										<span class="descriptionClosed" data-id="<?php echo $descriptionId; ?>" style="display:none;"><?php echo $description['LanguageName'] ?> / <?php echo $description['Title'] ?></span>
										<span class="label label-important deleteDescription pull-right" data-id="<?php echo $descriptionId; ?>"><?php echo Lang::get('Delete'); ?></span>
									
									</h3>
								</div>
								<div class="gadgetblock" id="descriptionGadgetblock<?php echo $descriptionId; ?>" style="display:none;" >
									<form action="<?php echo Uri::generate('admin/description/save');  ?>" method="post">
										<fieldset>
											<div>
												<label><?php echo Lang::get('Links'); ?></label>
												<select name="linksId<?php echo $descriptionId; ?>" class="linksId<?php echo $descriptionId; ?>">
												<?php 
												foreach ($links as $linksId => $link) 
												{
													?>
													<option value="<?php echo $linksId; ?>" <?php if ($description['LinksId'] == $linksId) {?> selected="selected" <?php } ?>>
														<?php echo $link['Code'] ?> / <?php echo $link['Title'] ?>
													</option>
													<?php
												}
												?>
												</select>
											</div>
											<div>
												<label class="checkbox">
													<input type="checkbox" name="draft<?php echo $descriptionId; ?>" class="draft<?php echo $descriptionId; ?>" value="1" <?php if ($description['Draft']) {?> checked <?php } ?> />
													<?php echo Lang::get('Draft'); ?>
												</label>
											</div>
											<div>
												<textarea name="description<?php echo $descriptionId; ?>" id="description<?php echo $descriptionId; ?>">
													<?php echo $description['Description']; ?>
												</textarea>
												<script type="text/javascript">
													CKEDITOR.replace( 'description<?php echo $descriptionId; ?>',{

														    filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
														    filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
														    filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
														    filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
														    filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
														    filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

													});
												</script>
											</div>
										</fieldset>
										<div class="buttonrow">
											<input type="submit" class="btn btn-primary updateDescription" data-id="<?php echo $descriptionId; ?>" value="<?php echo Lang::Get('Save'); ?>" />
										</div>

									</form>
								</div>
							</div>

							<?php
						}
					?>
				</div>
				<div class="box_new">
					<form action="" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Links'); ?></label>
								<select name="linksId" class="linksId">
								<?php 
								foreach ($links as $linksId => $link) 
								{
									?>
									<option value="<?php echo $linksId; ?>">
										<?php echo $link['Code'] ?> / <?php echo $link['Title'] ?>
									</option>
									<?php
								}
								?>
								</select>
							</div>
							<div>
								<label class="checkbox">
									<input type="checkbox" name="draft" class="draft" />
									<?php echo Lang::get('Draft'); ?>
								</label>
							</div>
							<div>
								<textarea name="description" id="description" class="description"></textarea>
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary saveDescription" data-branchOrder="1" value="<?php echo Lang::get('Save'); ?>" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>