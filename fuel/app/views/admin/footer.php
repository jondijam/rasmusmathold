<script src="/assets/js/vendor/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="/assets/js/vendor/jquery-ui.min.js"></script>
<script src="/assets/js/plugins.js"></script>
<script src="/assets/js/bootstrap.js"></script>

<script type="text/javascript" src="/assets/js/jquery-1.4.4.min.js"></script>
<script src="/assets/js/jquery.dataTables.js"></script>
<script src="/assets/js/jquery.jeditable.js"></script>
<script src="/assets/js/jquery.dataTables.editable.js"></script>
<script type="text/javascript">
	var closeText = '<?php echo Lang::get('Close'); ?>';
	var openText = '<?php echo Lang::get('Open'); ?>';
	var confirmText = '<?php echo Lang::get('YouAreDeleteing'); ?>'
	var deleteNotificationUrl = '<?php echo Uri::generate("admin/notification/delete"); ?>';
	var deleteArticleUrl = '<?php echo Uri::generate("admin/article/delete"); ?>';
	var updateNotificationUrl = '<?php echo Uri::generate("admin/notification/save"); ?>';
	var updateArticleUrl = '<?php echo Uri::generate("admin/article/save"); ?>';
	var savePagesUrl = '<?php echo Uri::generate("admin/pages/save"); ?>';
	var schoolLevelUrl = '<?php echo Uri::generate("admin/schoolLevel"); ?>';
	var chapterUrl = '<?php echo Uri::generate("admin/chapter"); ?>';
	var subChapterUrl = '<?php echo Uri::generate("admin/subChapter"); ?>';
	var lessonUrl = '<?php echo Uri::generate("admin/lesson"); ?>';
	var quizUrl = '<?php echo Uri::generate("admin/quiz"); ?>';
	
	var saveLinksUrl = '<?php echo Uri::generate("admin/links/save"); ?>';
	var saveLessonUrl = '<?php echo Uri::generate("admin/lesson/save"); ?>';
	var deletePagesUrl = '<?php echo Uri::generate("admin/pages/delete"); ?>';
	var deleteLinksUrl = '<?php echo Uri::generate("admin/links/delete"); ?>';
	var deleteQuizUrl = '<?php echo Uri::generate("admin/quiz/delete"); ?>';

	var savePagesDescription = '<?php echo Uri::generate("admin/description/save"); ?>';
	var deleteDescriptionUrl = '<?php echo Uri::generate("admin/description/delete"); ?>';
	var quizProblemsUrl = '<?php echo Uri::generate("admin/problems"); ?>';
	var quizOptionsUrl = '<?php echo Uri::generate("admin/options"); ?>';
	var saveQuestionUrl = '<?php echo Uri::generate("admin/quiz/save/question"); ?>';
	var saveOptionUrl = '<?php echo Uri::generate("admin/quiz/save/option"); ?>';
	var saveQuizUrl = '<?php echo Uri::generate("admin/quiz/save"); ?>';
</script>
<script src="/assets/js/main.js"></script>

<?php 
	if (Uri::Segment(2) == "") 
	{
		?>
		<script type="text/javascript" src="/assets/js/index.js"></script>
		<?php
	}
	elseif (Uri::Segment(2) == "schoolLevel") 
	{
		?>
		<script type="text/javascript" src="/assets/js/schoolLevel.js"></script>
		<?php
	}
	elseif (Uri::Segment(2) == "chapter") 
	{
		?>
		<script type="text/javascript" src="/assets/js/chapter.js"></script>
		<?php
	}
	elseif (Uri::Segment(2) == "subChapter") 
	{
		?>
		<script type="text/javascript" src="/assets/js/subChapter.js"></script>
		<?php
	}
	elseif (Uri::Segment(2) == "lesson") 
	{
		?>
		<script type="text/javascript" src="/assets/js/lesson.js"></script>
		<?php
	}
	elseif (Uri::Segment(2) == "quiz") 
	{
		?>
		<script type="text/javascript" src="/assets/js/quiz.js"></script>
		<?php
	}
?>
