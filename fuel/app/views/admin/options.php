<div class="option option<?php echo $quizId; ?><?php echo $problem; ?>" data-optionId="<?php echo $optionId; ?>" id="option<?php echo $quizId; ?><?php echo $problem; ?><?php echo $optionCount; ?>">
	<h3><?php echo Lang::get('Option', array('optionNumber' => $optionCount)); ?></h3>
	<div>
		<label class="checkbox">
			<input type="checkbox" name="correctBool<?php echo $quizId; ?><?php echo $problem ?><?php echo $optionCount; ?>" class="correctBool<?php echo $quizId; ?><?php echo $problem ?><?php echo $optionCount; ?>">
			<?php echo Lang::get('RightAnswer') ?></label>
	</div>
	<textarea name="optionText<?php echo $quizId; ?><?php echo $problem ?><?php echo $optionCount; ?>">
	</textarea>
	<script type="text/javascript">
		CKEDITOR.replace( 'optionText<?php echo $quizId; ?><?php echo $problem ?><?php echo $optionCount; ?>',{
    filebrowserBrowseUrl: '/assets/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: '/assets/ckfinder/ckfinder.html?Type=Images',
    filebrowserFlashBrowseUrl: '/assets/ckfinder/ckfinder.html?Type=Flash',
    filebrowserUploadUrl: '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
} );	
	</script>
			
</div>