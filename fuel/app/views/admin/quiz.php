<div class="container">
	<div class="wrap">
		<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Pages'); ?></h3>
				</div>
				<div class="box_content">
					<table class="table table-striped clearfix" id="quizPages">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Name'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($pages as $pagesId => $page) 
							{
								?>
									<tr id="<?php echo $pagesId; ?>" data-id="<?php echo $pagesId; ?>">
										<td><?php echo $pagesId; ?></td>
										<td data-oldpagesname="<?php echo $page['Name']?>"><?php echo $page['Name']?></td>
										<td>
											<a href="<?php echo Uri::generate('admin/pages/delete') ?>" class="table-action-deletelink btn btn-danger "><?php echo Lang::get('Delete'); ?></a>
										</td>
									</tr>
								<?php								
							} 
							?>
						</tbody>
					</table>
				</div>
				<div class="box_new">
					<form action="<?php echo Uri::generate('admin/pages/save') ?>" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Name'); ?></label>
								<input type="text" name="pagesName" class="pagesName" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary savePages" data-branchOrder="5" value="<?php echo Lang::get('Save'); ?>" />
						</div>
					</form>
				</div>
		</div>
		<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Links'); ?></h3>
				</div>
				<div class="box_content">
					<?php if($linksNumRows > 0 ){?>
					<table class="table table-striped clearfix" id="quizLinks">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Language'); ?></th>
								<th><?php echo Lang::get('Title'); ?></th>
								<th><?php echo Lang::get('Pages'); ?></th>
								<th><?php echo Lang::get('Order'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							
							<?php foreach ($links as $linksId => $link) 
							{
							?>
								<tr id="links<?php echo $linksId; ?>" data-id="<?php echo $linksId; ?>" class="links<?php echo $linksId; ?>">
									<td><?php echo $linksId?></td>
									<td><?php echo $link['Language']; ?> / <?php echo $link['SchoolLevel']; ?> / <?php echo $link['Chapter']; ?> / <?php echo $link['SubChapter']; ?> </td>
									<td><?php echo $link['Title']; ?></td>
									<td><?php echo $link['Name']; ?></td>
									<td><?php echo $link['Orders']?></td>
									<td><a href="<?php echo Uri::generate('admin/links/delete') ?>" class="table-action-deletelink btn btn-danger" data-id="<?php echo $linksId; ?>"><?php echo Lang::get('Delete'); ?></a></td>
								</tr>
							<?php
							} ?>
						</tbody>					
					</table>
					<?php  } ?>
				</div>
				<div class="box_new">
					<form action="/admin/links/save" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Pages'); ?></label>
								<select name="pages" class="pages">
									<?php foreach ($pages as $pagesId => $page) 
									{
										?>
										<option value="<?php echo $pagesId; ?>"><?php echo $page['Name']; ?></option>
										<?php

									} ?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Languages'); ?> / <?php echo Lang::get('SchoolLevel'); ?></label>
								<select name="Parent" class="parent">
									
									<?php

									foreach ($linksParents as $linksParentsId => $linksParent) 
									{?>

										<option value="<?php echo $linksParentsId; ?>"><?php echo $linksParent['Code']; ?> / <?php echo $linksParent['SchoolLevel']; ?> / <?php echo $linksParent['Chapter']; ?> / <?php echo $linksParent['Title']; ?></option>
									<?php
										# code...
									}
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Order'); ?></label>
								<input name="order" class="order" type="text" />
							</div>
							<div>
								<label><?php echo Lang::get('Title'); ?></label>
								<input type="text" class="title" name="title" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary saveLinks" data-branchOrder="5" value="<?php echo Lang::get('Save'); ?>" /> 
						</div>
					</form>
				</div>
		</div>
		<div class="box span12">
			<div class="box_headline">
				<h3><?php echo Lang::get('Quiz'); ?></h3>
				<img src="/assets/img/ajax-loader.gif" class="loader" alt="" />
			</div>
			<div class="box_content">
				<input type='hidden' id='current_page' />  
				<input type='hidden' id='show_per_page' />
				
				<div class="paginationContent" data-currentPage="0" data-showPerPage="5" data-quizCount="<?php echo $quizCount; ?>">
				</div>
				<div class="pagination">
					<ul id='page_navigation'>
					<ul>
				</div>

			</div>
			<div class="box_new">
				<form action="" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Pages') ?></label>
								<select name="pages" class="pagesId">
										<?php foreach ($pages as $pagesId => $page) 
									{
										?>
										<option value="<?php echo $pagesId; ?>"><?php echo $page['Name']; ?></option>
										<?php

									} ?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Languages'); ?></label>
								<select name="languages" class="languages">
									<?php
									foreach ($languages as $languageId => $language) 
									{?>

										<option value="<?php echo $languageId; ?>"><?php echo $language['Name']; ?></option>
									<?php
										# code...
									}
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Problems');  ?></label>
								<select name="problemsCount" class="problemsCount">
									<?php 
									for ($i=1; $i <= 20; $i++) 
									{ ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									
									<?php
									}
									?>
								</select>
							</div>
							<div>
								<label class="checkbox">
									<input type="checkbox" name="draft0" value="1" class="draft0" />
									<?php echo Lang::get('Draft'); ?></label>
								
							</div>
							<div class="quiz">
								<?php $i = 1; ?>
								<div class="problems" id="problem0<?php echo $i; ?>">
									<div class="headline">
										<h2>
											<?php echo Lang::get('Problem',array('ProblemNumber' => $i)); ?>
										</h2>
									</div>
									<div class="question">
									<textarea name="questionText0<?php echo $i; ?>" id="questionText0<?php echo $i; ?>" class="neweditor"></textarea>
									</div>
									<div class="options options0<?php echo $i; ?>">	
										<div class="option option0<?php echo $i; ?>" id="option0<?php echo $i; ?>1">
											<h3><?php echo Lang::get('Option', array('optionNumber' => 1)); ?></h3>
											<div>
												<label class="checkbox">
												<input type="checkbox" name="correctBool0<?php echo $i ?>1"><?php echo Lang::get('RightAnswer') ?></label>
											</div>
											<textarea name="optionText0<?php echo $i; ?>1" id="optionText0<?php echo $i; ?>1" class="neweditor">
											</textarea>
											
										</div>
									</div>
									<div class="optionNavbar">
										<ul class="nav nav-pills">
											<li><a href="javascript:addOption(0, <?php echo $i; ?>)"><?php echo Lang::get('AddOption'); ?></a></li>
											<li><a href="javascript:removeOption(0, <?php echo $i; ?>)"><?php echo Lang::get('RemoveOption'); ?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary savequiz" />
						</div>
				</form>
			</div>
		</div>
	</div
></div>