<!doctyp>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title><?php echo $loginTitle; ?></title>
		<meta name="description" content="" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-responsive.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/admin.css">
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
	</head>
	<body>

		<div class="container">
			<form class="form-signin" action="<?php echo Uri::generate('admin/login'); ?>" method="post">
	        	<h2 class="form-signin-heading"><?php echo $signInHeading; ?></h2>
	        	<input type="text" name="username" class="input-block-level" placeholder="<?php echo $emailAddress; ?>">
	        	<input type="password" name="password" class="input-block-level" placeholder="<?php echo $password; ?>">
	        	<label class="checkbox">
	          		<input type="checkbox" value="remember-me"><?php echo $rememberMe; ?>
	        	</label>
	        	<button class="btn btn-large btn-primary" type="submit"><?php echo $signIn; ?></button>
	        	<a href="/is/<?php echo Uri::string(); ?>">íslenska</a>
	        	<a href="/en/<?php echo Uri::string(); ?>">English</a>
	      </form>

	    </div> <!-- /container -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="/assets/js/plugins.js"></script>
        <script src="/assets/js/main.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
	</body>
</html>