<div class="container">
	<div class="wrap">
		<div class="row">
			<div class="box span12">
				<div class="box_headline"><h3><?php echo Lang::get('Pages'); ?></h3></div>
				<div class="box_content">
					<table class="table table-striped clearfix" id="lessonPages">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Name'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($pages as $pagesId => $page) 
							{
								?>
									<tr id="<?php echo $pagesId; ?>" data-id="<?php echo $pagesId; ?>">
										<td><?php echo $pagesId; ?></td>
										<td data-oldpagesname="<?php echo $page['Name']?>"><?php echo $page['Name']?></td>
										<td>
											<a href="<?php echo Uri::generate('admin/pages/delete') ?>" class="table-action-deletelink btn btn-danger "><?php echo Lang::get('Delete'); ?></a>
										</td>
									</tr>
								<?php								
							} 
							?>
						</tbody>
					</table>
				</div>
				<div class="box_new">
					<form action="<?php echo Uri::generate('admin/pages/save') ?>" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Name'); ?></label>
								<input type="text" name="pagesName" class="pagesName" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary savePages" data-branchOrder="4" value="<?php echo Lang::get('Save'); ?>" />
						</div>
					</form>
				</div>
			</div>
			<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Links'); ?></h3>
				</div>
				<div class="box_content">
					<?php if($linksNumRows > 0 ){?>
					<table class="table table-striped clearfix" id="lessonLinks">
						<thead>
							<tr>
								<th></th>
								<th><?php echo Lang::get('Language'); ?></th>
								<th><?php echo Lang::get('Title'); ?></th>
								<th><?php echo Lang::get('Pages'); ?></th>
								<th><?php echo Lang::get('Order'); ?></th>
								<th><?php echo Lang::get('Methods'); ?></th>
							</tr>
						</thead>
						<tbody>
							
							<?php foreach ($links as $linksId => $link) 
							{
							?>
								<tr id="links<?php echo $linksId; ?>" data-id="<?php echo $linksId; ?>" class="links<?php echo $linksId; ?>">
									<td><?php echo $linksId?></td>
									<td><?php echo $link['Language']; ?> / <?php echo $link['SchoolLevel']; ?> / <?php echo $link['Chapter']; ?> / <?php echo $link['SubChapter']; ?> </td>
									<td><?php echo $link['Title']; ?></td>
									<td><?php echo $link['Name']; ?></td>
									<td><?php echo $link['Orders']?></td>
									<td><a href="<?php echo Uri::generate('admin/links/delete') ?>" class="table-action-deletelink btn btn-danger" data-id="<?php echo $linksId; ?>"><?php echo Lang::get('Delete'); ?></a></td>
								</tr>
							<?php
							} ?>
						</tbody>					
					</table>
					<?php  } ?>
				</div>
				<div class="box_new">
					<form action="/admin/links/save" method="post">
						<fieldset>
							<div>
								<label><?php echo Lang::get('Pages'); ?></label>
								<select name="pages" class="pages">
									<?php foreach ($pages as $pagesId => $page) 
									{
										?>
										<option value="<?php echo $pagesId; ?>"><?php echo $page['Name']; ?></option>
										<?php

									} ?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Languages'); ?> / <?php echo Lang::get('SchoolLevel'); ?></label>
								<select name="Parent" class="parent">
									
									<?php

									foreach ($linksParents as $linksParentsId => $linksParent) 
									{?>

										<option value="<?php echo $linksParentsId; ?>"><?php echo $linksParent['Code']; ?> / <?php echo $linksParent['SchoolLevel']; ?> / <?php echo $linksParent['Chapter']; ?> / <?php echo $linksParent['Title']; ?></option>
									<?php
										# code...
									}
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Order'); ?></label>
								<input name="order" class="order" type="text" />
							</div>
							<div>
								<label><?php echo Lang::get('Title'); ?></label>
								<input type="text" class="title" name="title" />
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary saveLinks" data-branchOrder="4" value="<?php echo Lang::get('Save'); ?>" /> 
						</div>
					</form>
				</div>
			</div>
			<div class="box span12">
				<div class="box_headline">
					<h3><?php echo Lang::get('Lessons'); ?></h3>
				</div>
				<div class="box_content">
					<?php 
					
						foreach ($lessons as $lessonId => $lesson) 
						{

						
							?>
							<div class="gadget" id="lessonGadget<?php echo $lessonId; ?>">
								<div class="titlebar">
									<h3>
									
									<?php

									foreach ($lessonLinks[$lessonId] as $lessonLinksId => $lessonLink) 
									{
										?>
										<span class="lessonOpen" data-id="<?php echo $lessonId; ?>">
										<?php echo $lessonLink['SchoolLevel']; ?> / <?php echo $lessonLink['SubChapter']; ?> / <?php echo $lessonLink['Title']; ?>
										</span>
										<span class="lessonClosed hidden" data-id="<?php echo $lessonId; ?>">
										<?php echo $lessonLink['SchoolLevel']; ?> / <?php echo $lessonLink['SubChapter']; ?> / <?php echo $lessonLink['Title']; ?>
										</span>
										<?php
									}


								?>
						
								<span class="label label-important deleteLesson pull-right" data-id="<?php echo $lessonId; ?>"><?php echo Lang::get('Delete'); ?></span>
								</h3>
								</div>
								<div class="gadgetblock" id="lessonGadgetblock<?php echo $lessonId; ?>" style="display:none;" >
									<h4>
										<?php

										foreach ($lessonLinks[$lessonId] as $lessonLinksId => $lessonLink) 
										{
										?>
											<?php echo $lessonLink['Language']; ?> / <?php echo $lessonLink['SchoolLevel']; ?> / <?php echo $lessonLink['Chapter']; ?> / <?php echo $lessonLink['SubChapter']; ?> / <?php echo $lessonLink['Title']; ?> <br />
										<?php
										}

										?>	
						
									</h4>
									<form action="" method="post">
										<fieldset>
											<div>
												<label class="checkbox oldlessonCheck<?php echo $lessonId; ?>">
													<input type="checkbox" name="iframeCheck<?php echo $lessonId; ?>" data-id="<?php echo $lessonId; ?>" class="iframeCheck iframeCheck<?php echo $lessonId; ?>" value="0" <?php if($lesson['OldLesson']){ ?> checked <?php } ?>  />
													<?php echo Lang::get('OldLesson'); ?>
												</label>
											</div>
											<div class="oldlesson<?php echo $lessonId; ?> <?php if(!$lesson['OldLesson']){ ?> hidden <?php } ?> ">
													<label><?php echo Lang::get('IframeUrl'); ?></label>
													<input type="text" class="iframeUrl<?php echo $lessonId; ?>" value="<?php echo $lesson['IFrame']; ?>" name="iframeUrl<?php echo $lessonId; ?>" />

											</div>

											<div class="newlesson<?php echo $lessonId; ?>">
												<label><?php echo Lang::get('Languages'); ?></label>
												<select name="languages<?php echo $lessonId; ?>" class="languages<?php echo $lessonId; ?>">
													<?php
													foreach ($languages as $languageId => $language) 
													{?>

														<option value="<?php echo $languageId; ?>" <?php if($lesson['LanguageId'] == $languageId){ ?> selected="selected" <?php } ?> ><?php echo $language['Name']; ?></option>
													<?php
														# code...
													}
													?>
												</select>
											</div>
											<div>
												<label><?php echo Lang::get('Pages'); ?></label>
												<select class="pagesId<?php echo $lessonId; ?>" name="pages<?php echo $lessonId; ?>">
													<?php foreach ($pages as $pagesId => $page) 
													{
														?>
														<option value="<?php echo $pagesId; ?>" <?php if($lesson['PagesId'] == $pagesId){ ?> selected="selected" <?php } ?>  ><?php echo $page['Name']; ?></option>
														<?php

													} ?>



												</select>
											</div>
											<div>
												<label class="checkbox">
													<input type="checkbox" name="draft<?php echo $lessonId; ?>" class="draft<?php echo $lessonId; ?>" value="0" <?php if($lesson['Draft']){ ?> checked <?php } ?> >
													<?php echo Lang::get('Draft');?> 
												</label>
											</div>
											<div>
												<textarea name="lesson<?php echo $lessonId; ?>" id="lesson<?php echo $lessonId; ?>">
													<?php echo $lesson['Text'] ?>
												</textarea>
												<script type="text/javascript">
													 CKEDITOR.replace( 'lesson<?php echo $lessonId; ?>',{

															    filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
															    filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
															    filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
															    filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
															    filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
															    filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

													});
												</script>
											</div>
										</fieldset>
										<div class="buttonrow">
											<input type="submit" class="btn btn-primary updatelesson" name="UpdateLesson" data-id="<?php echo $lessonId; ?>" value="<?php echo Lang::Get('Save'); ?>" /> 
										</div>
									</form>

								</div>
								</div>
							<?php
						}
					?>

				</div>
				<div class="box_new">
					<form>
						<fieldset>
							<div>
								<label class="checkbox oldlessonCheck">
									<input type="checkbox" name="oldlesson" class="oldlessoncheckbox" value="0" />
									<?php echo Lang::get('OldLesson'); ?>
								</label>
							</div>
							<div class="oldlesson hidden">
									<label><?php echo Lang::get('IframeUrl'); ?></label>
									<input type="text" class="iframeUrl" name="iframeUrl" />

							</div>

							<div class="newlesson">
								<label><?php echo Lang::get('Languages'); ?></label>
								<select name="languages" class="languages">
									<?php
									foreach ($languages as $languageId => $language) 
									{?>

										<option value="<?php echo $languageId; ?>"><?php echo $language['Name']; ?></option>
									<?php
										# code...
									}
									?>
								</select>
							</div>
							<div>
								<label><?php echo Lang::get('Pages'); ?></label>
								<select class="pagesId" name="pages">
										<?php foreach ($pages as $pagesId => $page) 
									{
										?>
										<option value="<?php echo $pagesId; ?>"><?php echo $page['Name']; ?></option>
										<?php

									} ?>



								</select>
							</div>
							<div>
								<label class="checkbox">
									<input type="checkbox" name="draft" class="draft" value="0">
									<?php echo Lang::get('Draft');?> 
								</label>
							</div>
							<div>
								<textarea name="lesson" id="lesson">
								</textarea>
							</div>
						</fieldset>
						<div class="buttonrow">
							<input type="submit" class="btn btn-primary savelesson" name="SaveLesson" value="<?php echo Lang::Get('Save'); ?>" /> 
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>