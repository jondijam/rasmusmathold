<div class="container">
	<div class="wrap">
		<div class="row">
			<div class="span12">
				<div class="box">
					<div class="box_headline">
						<h3><?php echo Lang::get('Messages'); ?></h3>
					</div>
					<div class="box_content">
						
					</div>
					<div class="box_new">

					</div>
				</div>
				<div class="box">
					<div class="box_headline">
						<h3><?php echo Lang::get('Analytics'); ?></h3>
					</div>
					<div class="box_content">

					</div>
					<div class="box_new">
					</div>
				</div>
				<div class="box">
					<div class="box_headline">
						<h3><?php echo Lang::get('FrontpageArticle'); ?></h3>
					</div>
					<div class="box_content">
						<?php foreach ($articles as $articleId => $article) 
						{	
						?>
							<div class="gadget" id="articleGadget<?php echo $articleId; ?>">
								<div class="titlebar">
									<h3>
										<span class="articleOpen" data-id="<?php echo $articleId; ?>"><?php echo $article['Name'] ?> <?php echo $article['Title'] ?></span>
										<span class="articleClosed" data-id="<?php echo $articleId; ?>" style="display:none;"><?php echo $article['Name'] ?> <?php echo $article['Title'] ?></span>
										<span class="label label-important deleteArticle pull-right" data-id="<?php echo $articleId; ?>"><?php echo Lang::get('Delete'); ?></span>
									
									</h3>
								</div>
								<div class="gadgetblock" id="articleGadgetblock<?php echo $articleId; ?>" style="display:none;" >
								<form action="<?php echo Uri::generate('admin/article/save');  ?>" method="post">
									<fieldset>
										<div>
											<label><?php echo Lang::get('Title'); ?></label>
											<input name="title<?php echo $articleId; ?>" id="articleTitle<?php echo $articleId; ?>" value="<?php echo $article['Title']; ?>" /> 
										</div>
									
										<div>
											<label for="articleLanguages<?php echo $articleId; ?>"><?php echo Lang::get('Languages'); ?></label>
											<select name="languages<?php echo $articleId; ?>" id="articleLanguages<?php echo $articleId; ?>">
												<?php 
												foreach ($languages as $languageId => $language) 
												{
													?>
													<option value="<?php echo $languageId; ?>" <?php if($article['LanguageId'] == $languageId){ ?> selected="selected" <?php } ?>  ><?php echo $language['Name']; ?></option>
													<?php
												}
												?>
												
											</select>
										</div>
										<div>
				
											<label class="checkbox"><input type="checkbox" name="draft<?php echo $articleId; ?>" id="articleDraft<?php echo $articleId; ?>" <?php if(!$article['Publish']){ ?> checked <?php } ?> value="1" /> <?php echo Lang::get('Draft'); ?></label>
										</div>
										<div>
											<textarea name="article<?php echo $articleId; ?>" id="article<?php echo $articleId; ?>"><?php echo $article['Text']; ?></textarea>
											<script type="text/javascript">
											 CKEDITOR.replace( 'article<?php echo $articleId; ?>',{

															 	filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
																filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
																filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
																filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
																filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
																filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

															 });

											</script>
										</div>
									</fieldset>
									<div class="buttonrow">
										<input type="submit" name="save" class="btn btn-primary saveArticle" data-itemId="<?php echo $articleId; ?>" value="<?php echo Lang::get('Save'); ?>">
									</div>
								</form>
							</div>
							</div>
						<?php 
						} 
						?>	
					</div>
					<div class="box_new">
						<form action="<?php echo Uri::generate('admin/article/save');  ?>" method="post">
							<fieldset>
								<div>
									<label><?php echo Lang::get('Title'); ?></label>
									<input type="text" name="articleTitle" />  
								</div>
								<div>
									<label><?php echo Lang::get('Languages'); ?></label>
									<select name="languages" id="languages">
										<?php 
										foreach ($languages as $id => $item) 
										{
											?>
											<option value="<?php echo $id; ?>"><?php echo $item['Name']; ?></option>
											<?php
										}
										?>
										
									</select>
								</div>
								<div>
									<label class="checkbox"><input type="checkbox" name="articleDraft" value="1" /><?php echo Lang::get('Draft'); ?></label>
								</div>
								<div>
									<textarea name="article"></textarea>
								</div>
							</fieldset>
							<div class="buttonrow">
								<input type="submit" class="btn btn-primary" value="<?php echo Lang::get('Save'); ?>" />
							</div>
						</form>
					</div>
				</div>
				<div class="box">
					<div class="box_headline">
						<h3><?php echo Lang::get('Notification'); ?></h3>
					</div>
					<div class="box_content">
						<?php foreach ($notifications as $notificationId => $notification) 
						{
							?>
						
						<div class="gadget" id="gadget<?php echo $notificationId; ?>">
							<div class="titlebar">
								<h3>
									<span class="open" data-id="<?php echo $notificationId; ?>"><?php echo $notification['Name'] ?> <?php echo $notification['Title'] ?></span>
									<span class="closed" data-id="<?php echo $notificationId; ?>" style="display:none;"><?php echo $notification['Name'] ?> <?php echo $notification['Title'] ?></span>
									<span class="label label-important deleteNotification pull-right" data-id="<?php echo $notificationId; ?>"><?php echo Lang::get('Delete'); ?></span>			
								</h3>
							</div>
							<div class="gadgetblock" id="gadgetblock<?php echo $notificationId; ?>" style="display:none;" >
								<form action="<?php echo Uri::generate('admin/notification/save');  ?>" method="post">
									<fieldset>
										<div>
											<label><?php echo Lang::get('Title'); ?></label>
											<input name="title<?php echo $notificationId; ?>" id="notificationTitle<?php echo $notificationId; ?>" value="<?php echo $notification['Title']; ?>" /> 
										</div>
									
										<div>
											<label for="languages<?php echo $notificationId; ?>"><?php echo Lang::get('Languages'); ?></label>
											<select name="languages<?php echo $notificationId; ?>" id="languages<?php echo $notificationId; ?>">
												<?php 
												foreach ($languages as $languageId => $language) 
												{
													?>
													<option value="<?php echo $languageId; ?>" <?php if($notification['LanguageId'] == $languageId){ ?> selected="selected" <?php } ?>  ><?php echo $language['Name']; ?></option>
													<?php
												}
												?>
												
											</select>
										</div>
										<div>
				
											<label class="checkbox"><input type="checkbox" name="draft<?php echo $notificationId; ?>" id="draft<?php echo $notificationId; ?>" <?php if(!$notification['Publish']){ ?> checked <?php } ?> value="1" /> <?php echo Lang::get('Draft'); ?></label>
										</div>
										<div>
											<textarea name="notification<?php echo $notificationId; ?>" id="notification<?php echo $notificationId; ?>"><?php echo $notification['Text']; ?></textarea>
											<script type="text/javascript">
											 CKEDITOR.replace( 'notification<?php echo $notificationId; ?>',{

															 	filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
																filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
																filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
																filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
																filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
																filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

															 });

											</script>
										</div>
									</fieldset>
									<div class="buttonrow">
										<input type="submit" name="save" class="btn btn-primary saveNotification" data-itemId="<?php echo $notificationId; ?>" value="<?php echo Lang::get('Save'); ?>">
									</div>
								</form>
							</div>
						</div>
						<?php }?>
					</div>
					<div class="box_new ">
						<form action="<?php echo Uri::generate('admin/notification/save');  ?>" method="post">
							<fieldset>
								
								<div>
									<label for="notificationTitle"><?php echo Lang::get('Title'); ?></label>
									<input type="text" id="notificationTitle" name="title" />
								</div>
								<div>
									<label for="languages"><?php echo Lang::get('Languages'); ?></label>
									<select name="languages" id="languages">
										<?php 
										foreach ($languages as $languageId => $language) 
										{
											?>
											<option value="<?php echo $languageId; ?>"><?php echo $language['Name']; ?></option>
											<?php
										}
										?>
										
									</select>
								</div>
								<div>
									<label class="checkbox">
										<input type="checkbox" name="draft" value="1" />
										<?php echo Lang::get('Draft'); ?>
									</label>
								</div>
								<div>
									<textarea id="notification" name="notification"></textarea>
							
								</div>
							</fieldset>
							<div class="buttonrow">
								<input type="submit" name="save" class="btn btn-primary" value="<?php echo Lang::get('Save'); ?>">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>