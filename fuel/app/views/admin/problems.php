<?php 
for ($i=1; $i <= $problemsCount; $i++) 
{ 
	?>
	<div class="problems problems0" id="problem0<?php echo $i; ?>">
		<div class="headline">
			<h2>
				<?php echo Lang::get('Problem',array('ProblemNumber' => $i)); ?>
			</h2>
		</div>
		<div class="question question0<?php echo $i; ?>" data-questionId="0" >
		<textarea name="questionText0<?php echo $i; ?>" id="questionText0<?php echo $i; ?>" class="neweditor">
		</textarea>
		</div>
		<div class="options options0<?php echo $i; ?>">	
			<div class="option option0<?php echo $i; ?>" data-optionId="0" id="option0<?php echo $i; ?>1">
				<h3><?php echo Lang::get('Option', array('optionNumber' => 1)); ?></h3>
				<div>
					<label class="checkbox">
						<input type="checkbox" name="correctBool0<?php echo $i ?>1" class="correctBool0<?php echo $i ?>1">
						<?php echo Lang::get('RightAnswer') ?>
					</label>
				</div>
				<textarea name="optionText0<?php echo $i; ?>1" id="optionText0<?php echo $i; ?>1" class="neweditor">
				</textarea>
			</div>
		</div>
		<div class="optionNavbar">
			<ul class="nav nav-pills">
					<li><a href="javascript:addOption(0, <?php echo $i; ?>)"><?php echo Lang::get('AddOption'); ?></a></li>
					<li><a href="javascript:removeOption(0, <?php echo $i; ?>)"><?php echo Lang::get('RemoveOption'); ?></a></li>
			</ul>
		</div>
	</div>

	<?php
}

?>
<script type="text/javascript">
loadnewEditors();
</script>