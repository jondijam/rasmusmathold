<nav>
	<div class="navbar-inner navbar-static-top">
	 	<div class="container">
			<ul class="nav nav-pills">
				<li <?php if($active == ""){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin') ?>"><?php echo Lang::get('Home'); ?></a></li>
				<li <?php if($active == "schoolLevel"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/schoolLevel') ?>"><?php echo Lang::get('SchoolLevel'); ?></a></li>
				<li <?php if($active == "chapter"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/chapter') ?>"><?php echo Lang::get('Chapter'); ?></a></li>
				<li <?php if($active == "subChapter"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/subChapter') ?>"><?php echo Lang::get('SubChapter'); ?></a></li>
				<li <?php if($active == "lesson"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/lesson') ?>"><?php echo Lang::get('Lesson'); ?></a></li>
				<li <?php if($active == "quiz"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/quiz') ?>"><?php echo Lang::get('Quiz'); ?></a></li>
				<li <?php if($active == "language"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/language') ?>"><?php echo Lang::get('Language'); ?></a></li>
				<li <?php if($active == "registration"){?> class="active" <?php } ?>><a href="<?php echo Uri::generate('admin/registration') ?>"><?php echo Lang::get('Registration'); ?></a></li>
				

			</ul>
		</div>
	</div>
</nav>