<?php 
					$quizCounter = 0;
					foreach ($quizResult as $quizId => $quizEntry)
					{
						?>
						<div class="gadget" id="gadget<?php echo $quizId; ?>">
							<div class="titlebar">
								<h3>
									<span class="quizOpen" data-id="<?php echo $quizId; ?>">
											<?php 
												foreach ($quizLinks[$quizId] as $LinkId => $linkEntry) 
												{
													?>
														<?php echo $linkEntry['SchoolLevel']; ?> / <?php echo $linkEntry['SubChapter']; ?> / <?php echo $linkEntry['Title']; ?>

													<?php
												}
											?>
									</span>
									<span class="quizClosed hidden" data-id="<?php echo $quizId; ?>">
											<?php 
												foreach ($quizLinks[$quizId] as $LinkId => $linkEntry) 
												{
													?>
														<?php echo $linkEntry['SchoolLevel']; ?> / <?php echo $linkEntry['SubChapter']; ?> / <?php echo $linkEntry['Title']; ?>

													<?php
												}
											?>
									</span>
									<span class="label label-important deleteQuiz pull-right"  data-id="<?php echo $quizId; ?>">
										<?php echo Lang::get('Delete'); ?>
									</span>
								</h3>
							</div>
							<div class="gadgetblock" id="quizGadgetblock<?php echo $quizId; ?>" style="display:none;">
								<form action="<?php echo Uri::generate('admin/quiz/save'); ?>" method="post">
									<fieldset>
										<div>
											<label><?php echo Lang::get('Pages') ?></label>
											<select name="pages<?php echo $quizId; ?>" class="pagesId<?php echo $quizId; ?>">
												<?php foreach ($pages as $pagesId => $page) 
												{
												?>
													<option value="<?php echo $pagesId; ?>" <?php if($quizEntry['PagesId'] == $pagesId){?> selected="selected" <?php } ?> ><?php echo $page['Name']; ?></option>
												<?php
												} ?>
											</select>
										</div>
										<div>
											<label><?php echo Lang::get('Languages'); ?></label>
											<select name="languages<?php echo $quizId; ?>" class="languages<?php echo $quizId; ?>">
												<?php
												foreach ($languages as $languageId => $language) 
												{?>

														<option value="<?php echo $languageId; ?>" <?php if($quizEntry['LanguageId'] == $languageId){?> selected="selected" <?php } ?>><?php echo $language['Name']; ?></option>
												<?php
												# code...
												}	
												?>
											</select>
										</div>
										<div>
											<label class="checkbox">
												<input type="checkbox" name="draft<?php echo $quizId; ?>" class="draft<?php echo $quizId; ?>" value="1" <?php if($quizEntry['Draft']){?> checked="checked"<?php } ?> />
												<?php echo Lang::get('Draft'); ?>
											</label>
										</div>
										<div>
											<label class="checkbox">
												<input type="checkbox" name="translate<?php echo $quizId; ?>" class="translate<?php echo $quizId; ?>" value="1"  />
												<?php echo Lang::get('Translate'); ?>
											</label>
										</div>
										<div class="quiz<?php echo $quizId; ?>">
												<?php
													$problemCount = 0;
													foreach ($questionList[$quizId] as $questionId => $questionEntry) 
													{
														$problemCount = $questionEntry['Problem'];
														?>
														<div class="problems problems<?php echo $quizId; ?>" id="problem<?php echo $quizId; ?><?php echo $problemCount; ?>">
															<div class="headline">
																<h2>
																	<?php echo Lang::get('Problem',array('ProblemNumber' => $problemCount)); ?>
																</h2>
															</div>
															<div class="question question<?php echo $quizId; ?><?php echo $problemCount; ?>" data-questionId="<?php echo $questionId; ?>">
																<textarea name="questionText<?php echo $quizId; ?><?php echo $problemCount; ?>" id="questionText<?php echo $quizId; ?><?php echo $problemCount; ?>" class="editor<?php echo $quizId; ?>">
																	<?php echo $questionEntry['Text'] ?>
																</textarea>
															</div>
															<div class="options  options<?php echo $quizId; ?><?php echo $problemCount; ?>">
																<?php 
																	$optionCounter = 0;

																foreach ($options[$quizId][$questionId] as $optionId => $option) 
																{
																	$optionCounter++;
																?>
																<div class="option option<?php echo $quizId; ?><?php echo $problemCount; ?>" data-optionId="<?php echo $optionId; ?>" id="option<?php echo $quizId ?><?php echo $problemCount; ?><?php echo $optionCounter; ?>">
																	<h2><?php echo Lang::get('Option', array('optionNumber' => $optionCounter)); ?></h2>
																	<div>
																			<label class="checkbox">
																				<input type="checkbox" name="correctBool<?php echo $quizId; ?><?php echo $problemCount ?><?php echo $optionCounter; ?>" class="correctBool<?php echo $quizId; ?><?php echo $problemCount ?><?php echo $optionCounter; ?>" <?php if($option['CorrectAnswer']){ ?> checked <?php } ?>>
																				<?php echo Lang::get('RightAnswer') ?>
																			</label>
																	</div>
																	<textarea name="optionText<?php echo $quizId; ?><?php echo $problemCount; ?><?php echo $optionCounter; ?>" id="optionText<?php echo $quizId; ?><?php echo $problemCount ?><?php echo $optionCounter; ?>" class="editor<?php echo $quizId; ?>" >
																		<?php echo $option['Text']; ?>
																	</textarea>
																	<script type="text/javascript"></script>
																</div>
																<?php
																}
																?>
															</div>
															<div class="optionNavbar">
																<ul class="nav nav-pills">
																	<li><a href="javascript:addOption(<?php echo $quizId; ?>, <?php echo $problemCount; ?>)"><?php echo Lang::get('AddOption'); ?></a></li>
																	<li><a href="javascript:removeOption(<?php echo $quizId; ?>, <?php echo $problemCount; ?>)"><?php echo Lang::get('RemoveOption'); ?></a></li>
																</ul>
															</div>
														</div>
														<?php
													}
												
												?>
										</div>
									</fieldset>
									<div class="buttonrow">
										<input type="submit" class="btn btn-primary updateQuiz" data-id="<?php echo $quizId; ?>" value="<?php echo Lang::get('Save'); ?>">
										<img src="/assets/img/ajax-loader.gif" class="loader hidden" alt="" />
									</div>	
								</form>
							</div>
						</div>
						<?php
					}
?>

				