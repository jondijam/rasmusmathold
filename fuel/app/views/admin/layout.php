<!Doctype html>
<html lang="en">
	<head>
		 <?php echo $head; ?>
	</head>
	<body>
		<?php echo $header; ?>
        <?php echo $nav; ?>
        <?php echo $content; ?>
        <?php echo $footer; ?>
	</body>
</html>
