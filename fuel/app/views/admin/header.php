<header>
	<div class="container">
		<div class="row">
			<div class="span4">
				<h1>Rasmusmath.com admin</h1>
			</div>
			<div class="span5">
				<ul class="nav nav-pills">
					<li><a href="/is/<?php echo Uri::string(); ?>">íslenska</a></li>
	        		<li><a href="/en/<?php echo Uri::string(); ?>">English</a></li>
				</ul>
			</div>
			<div class="span3 pull-right">
				<ul class="nav nav-pills pull-right">
					<li><a href="<?php echo Uri::generate('admin/logout'); ?>"><?php echo Lang::get('Logout'); ?></a></li>	

				</ul>
			</div>
		</div>
	</div>
</header>
