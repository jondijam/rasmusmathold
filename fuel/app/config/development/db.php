<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=rasmusmath',
			'username'   => 'rasmusmath',
			'password'   => 'rasmusmath',
			'port' => '3306'
		),
	),
	'livedev' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=mysql.rasmusmath.com; dbname=rasmusmath',
			'username'   => 'rasmusmath',
			'password'   => '07!EldfrettA',
			'port' => '3306'
		),
	),
);
