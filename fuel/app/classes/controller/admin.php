
<?php

use \Model\AdminLogin;
use \Model\Admin;

class Controller_Admin extends Controller
{

	public function before()
    {
        // check for admin
    }

	public function action_index()
	{
		$auth = Auth::instance();
		Lang::load('admin');

		$data['loginTitle'] = Lang::get('LoginTitle');
		$data['signInHeading'] = Lang::get('SignInHeading');
		$data['rememberMe'] = Lang::get('RememberMe');
		$data['signIn'] = Lang::get('SignIn');
		$data['username'] = Lang::get('Username');
		$data['password'] = Lang::get('Password');
		$data['emailAddress'] = Lang::get('EmailAddress');
		
		$languageId = 10;
		$group = 6;

		if ($auth->check() && $auth->member($group) ) 
		{
			$data['UrlSaveNotification'] = Uri::generate('admin/notification/save');
			$notifications = Admin::Notifications($languageId);
			$num_rows = count($notifications);
			$data['notifications'] = $notifications->as_array('NotificationId'); 
			$articles = Admin::Articles();
			$data['articles'] = $articles->as_array('ArticleId'); 
			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');
			
			$data['active'] = Uri::Segment(2);

			

			$view = View::forge('admin/layout');
			$view->head = View::forge('admin/head');
			$view->header = View::forge('admin/header');
			$view->nav = View::forge('admin/nav', $data, false);
			$view->content = View::forge('admin/content', $data);  
			$view->footer = View::forge('admin/footer');

			return $view;
			
		}
		else
		{
			//$view->content = View::forge('admin/login');
			return View::forge('admin/login', $data);
		}

        // return the view object to the Request      
	}

	public function action_notification()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group) ) 
		{
			(int)$notificationId = 0;
			$notificationId = Input::post('notificationId');

			if (Uri::segment(4) == 'save' || Uri::segment(3) == 'save') 
			{
				
				$draft = Input::post('draft');
				$title = Input::post('title');

				$languageId = Input::post('languages');
				$message = Input::post('notification');
				$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");
				$userId = $auth->get_user_id(1);

				if ($notificationId > 0) 
				{
					Admin::UpdateNotification($notificationId, $title, $languageId, $draft, $message, $time);
				}
				else
				{
					Admin::SaveNotification($title, $languageId, $draft, $message, $time, $userId[1]);
					
					$url = Uri::generate('admin');
					Response::redirect($url);
				}			

			}
			elseif(Uri::segment(4) == 'delete' || Uri::segment(3) == 'delete')
			{
				Admin::DeleteNotification($notificationId);
			}

		}			
	}

	public function action_article()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->get_user_id() && $auth->member($group) ) 
		{
			(int)$articleId = 0;
			(int)$articleId = Input::post('articleId');
			(string)$title = Input::post('articleTitle');
			(string)$text = Input::post('article');
			(int)$languageId = Input::post('languages');
			$draft = Input::post('articleDraft');
			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");
			$userId = $auth->get_user_id(1);


			if (Uri::segment(4) == 'save' || Uri::segment(3) == 'save') 
			{
				if ($articleId > 0) 
				{
					Admin::UpdateArticle($articleId, $title, $text, $languageId, $draft, $time);
				}
				else
				{
					Admin::SaveArticle($title, $text, $languageId, $draft, $time, $userId[1]);
					
					$url = Uri::generate('admin');
					Response::redirect($url);
				}
			}
			elseif(Uri::segment(4) == 'delete' || Uri::segment(3) == 'delete')
			{
				Admin::DeleteArticle($articleId);
			}

		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_login()
	{
		Lang::load('admin');
		$data['loginTitle'] = Lang::get('LoginTitle');
		$data['signInHeading'] = Lang::get('SignInHeading');
		$data['rememberMe'] = Lang::get('RememberMe');
		$data['signIn'] = Lang::get('SignIn');
		$data['username'] = Lang::get('Username');
		$data['password'] = Lang::get('Password');
		$data['emailAddress'] = Lang::get('EmailAddress');

		$auth = Auth::instance();
		
		$val = Validation::forge('my_validation');
		$val->add_field('username', Lang::get('Username'), 'required');
		$val->add_field('password', Lang::get('Password'), 'required');
		$group = 6;

		// run validation on just post
		if ($val->run())
		{
		    // process your stuff when validation succeeds
		    if ($auth->login(Input::post('username'), Input::post('password')) && $auth->member($group, $user = null))
	        {

	        	
	        	$url = Uri::generate('admin');
	             //credentials ok, go right in
	            Response::redirect($url);
	        }
	        else
	        {
	            // Oops, no soup for you. try to login again. Set some values to
	            // repopulate the username field and give some error text back to the view
	            $data['usernamePost']    = Input::post('username');
	            $data['login_error'] = 'Wrong username/password combo. Try again';
	        }
		}

	    echo View::forge('admin/login');
	}

	public function action_schoolLevel()
	{
		Lang::load('admin');

		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			$branchOrder = 1;
			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');

			$links = Admin::Links($branchOrder);
			$linksResult = $links->as_array('LinksId');

			$data['links'] = $linksResult;
			$data['pages'] = $pagesResult;
			$data['active'] = Uri::Segment(2);

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$descriptions = Admin::Descriptions($branchOrder);

			$descriptionsResult = $descriptions->as_array('DescriptionId');
			$data['descriptions'] = $descriptionsResult;

			$segment = Uri::Segment(3);

			if ($segment == "select") 
			{
				//get languages and save to Json

				

				foreach ($linksResult as $linksId => $link) 
				{
					$array[''.$linksId.''] = ''.$link['Language'].' / '.$link['Title'].'';
				}

				$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
				print json_encode($array);
			}
			else
			{

				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/schoolLevel', $data);  
				$view->footer = View::forge('admin/footer');



				return $view;
			}

		}
		else
		{
			echo 'de';
			$url = Uri::generate('admin');
	    	//credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_chapter()
	{
		Lang::load('admin');
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			$data['active'] = Uri::Segment(2);

			$branchOrder = 2;
			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');


			$links = Admin::Links($branchOrder);
			$linksResult = $links->as_array('LinksId');


			$data['links'] = $linksResult;
			$data['pages'] = $pagesResult;

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$linksParent = Admin::Links(1);
			$linksParentResult = $linksParent->as_array('LinksId');
			$data['linksParents'] =  $linksParentResult;

			$descriptions = Admin::Descriptions($branchOrder);

			$descriptionsResult = $descriptions->as_array('DescriptionId');
			$data['descriptions'] = $descriptionsResult;

			$descriptions = Admin::Descriptions($branchOrder);

			$descriptionsResult = $descriptions->as_array('DescriptionId');
			$data['descriptions'] = $descriptionsResult;

			$segment = Uri::Segment(3);

			if ($segment == "select") 
			{

				foreach ($linksResult as $linksId => $link) 
				{
					$array[''.$linksId.''] = ''.$link['Language'].' / '.$link['SchoolLevel'].' / '.$link['Title'].'';
				}

				$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
				print json_encode($array);
			}
			else
			{

				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/chapter', $data);  
				$view->footer = View::forge('admin/footer');
				return $view;
			}

			
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_subChapter()
	{
		Lang::load('admin');
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			$data['active'] = Uri::Segment(2);

			$branchOrder = 3;
			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');


			$links = Admin::Links($branchOrder);
			$linksResult = $links->as_array('LinksId');


			$data['links'] = $linksResult;
			$data['pages'] = $pagesResult;


			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$linksParent = Admin::Links(2);
			$linksParentResult = $linksParent->as_array('LinksId');
			$data['linksParents'] =  $linksParentResult;

			$descriptions = Admin::Descriptions($branchOrder);

			$descriptionsResult = $descriptions->as_array('DescriptionId');
			$data['descriptions'] = $descriptionsResult;

			$descriptions = Admin::Descriptions($branchOrder);

			$descriptionsResult = $descriptions->as_array('DescriptionId');
			$data['descriptions'] = $descriptionsResult;

			$segment = Uri::Segment(3);

			if ($segment == "select")
			{
				foreach ($linksResult as $linksId => $link) 
				{
					$array[''.$linksId.''] = ''.$link['Language'].' / '.$link['SchoolLevel'].'/'.$link['Chapter'].'/'.$link['Title'].'';
				}

				print json_encode($array);
			}
			else
			{
				

				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/subchapter', $data);  
				$view->footer = View::forge('admin/footer');
				return $view;
				
			}

		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_lesson()
	{
		Lang::load('admin');
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			$data['active'] = Uri::Segment(2);
			
			$segment = Uri::Segment(3);
			$branchOrder = 4;

			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');
			$data['pages'] = $pagesResult;

			$links = Admin::Links($branchOrder);
			$num_rows = count($links);
			$data['linksNumRows'] = $num_rows;

			if ($num_rows > 0) 
			{
				# code...
				$linksResult = $links->as_array('LinksId');
				$data['links'] = $linksResult;
			}
			else
			{
				$data['links'] = "";
			}

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$linksParent = Admin::Links(3);
			$linksParentResult = $linksParent->as_array('LinksId');
			$data['linksParents'] =  $linksParentResult;

			//get lessons

			$lessons = Admin::Lessons($branchOrder);
			$lessonsResult = $lessons->as_array('LessonId');

			foreach ($lessonsResult as $lessonId => $lesson) 
			{
				$lessonlinks = Admin::LessonLinks($lesson['PagesId'], $lesson['LanguageId']);
				$lessonslinksResult = $lessonlinks->as_array('LinksId');

				$data['lessonLinks'][$lessonId] = $lessonslinksResult;
			}

			$data['lessons'] = $lessonsResult;

			if ($segment == 'save') 
			{
				# code...

					$pagesId = 0;
					$lessonId = 0;
					$lessonText = "";
					$languageId = 0;
					$oldlesson = 0;
					$iframe = "";

					$pagesId = Input::post('pagesId');
					$lessonId = Input::post('lessonId');
					$lessonText = Input::post('lessonText');
					$languageId = Input::post('languageId');
					$oldlesson = Input::post('oldLesson');
					$draft = Input::post('draft');
					$iframe = Input::post('iframeUrl');
					$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");


					if ($lessonId > 0) 
					{
						Admin::UpdateLesson($lessonId, $pagesId, $languageId, $oldlesson, $iframe, $draft, $lessonText, $time);
					}
					else
					{
						Admin::SaveLesson($pagesId, $languageId, $oldlesson, $iframe, $draft, $lessonText, $time);
					}		
			}
			else
			{
				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/lessons', $data);  
				$view->footer = View::forge('admin/footer');
				return $view;
			}


			/*
			$data['active'] = Uri::Segment(2);
			$segment = Uri::Segment(3);
	
			$branchOrder = 4;
			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');
			$data['pages'] = $pagesResult;

			$links = Admin::Links($branchOrder);
			$num_rows = count($links);
			$data['linksNumRows'] = $num_rows;

			if ($num_rows > 0) 
			{
				# code...
				$linksResult = $links->as_array('LinksId');
				$data['links'] = $linksResult;
				

			}

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$linksParent = Admin::Links(3);
			$linksParentResult = $linksParent->as_array('LinksId');
			$data['linksParents'] =  $linksParentResult;

			$lessons = Admin::Lessons($branchOrder);
			$lessonsResult = $lessons->as_array('LessonId');

			foreach ($lessonsResult as $lessonId => $lesson) 
			{
				$lessonlinks = Admin::LessonLinks($lesson['PagesId'], $lesson['LanguageId']);
				$lessonslinksResult = $lessonlinks->as_array('LinksId');

				$data['lessonLinks'][$lessonId] = $lessonslinksResult;
			}

			$data['lessons'] = $lessonsResult;


			if ($segment == 'save') 
			{
				# code...

					$pagesId = 0;
					$lessonId = 0;
					$lessonText = "";
					$languageId = 0;
					$oldlesson = 0;
					$iframe = "";

					$pagesId = Input::post('pagesId');
					$lessonId = Input::post('lessonId');
					$lessonText = Input::post('lessonText');
					$languageId = Input::post('languageId');
					$oldlesson = Input::post('oldLesson');
					$draft = Input::post('draft');
					$iframe = Input::post('iframeUrl');
					$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");


					if ($lessonId > 0) 
					{
						Admin::UpdateLesson($lessonId, $pagesId, $languageId, $oldlesson, $iframe, $draft, $lessonText, $time);
					}
					else
					{
						Admin::SaveLesson($pagesId, $languageId, $oldlesson, $iframe, $draft, $lessonText, $time);
					}		
			}
			else
			{
				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/lessons', $data);  
				$view->footer = View::forge('admin/footer');
				return $view;
			}

			*/
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_quiz()
	{
		Lang::load('admin');
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			$data['active'] = Uri::Segment(2);
			$segment = Uri::Segment(3);
			$quiz = Uri::Segment(4);
			$userId = $auth->get_user_id(1);
			$branchOrder = 5;
			$pages = Admin::Pages($branchOrder);
			$pagesResult = $pages->as_array('PagesId');
			$data['pages'] = $pagesResult;

			$links = Admin::Links($branchOrder);
			$num_rows = count($links);
			$data['linksNumRows'] = $num_rows;

			if ($num_rows > 0) 
			{
				# code...
				$linksResult = $links->as_array('LinksId');
				$data['links'] = $linksResult;
				

			}

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$linksParent = Admin::Links(3);
			$linksParentResult = $linksParent->as_array('LinksId');
			$data['linksParents'] =  $linksParentResult;

			$problem = Input::post('problem');
			$pageId = Input::post('pageId');
			$languageId = Input::post('languageId');
			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");
			$quizId = Input::post('quizId');


			$quizzes = Admin::CountQuiz($branchOrder)->as_array('QuizId');
			$data['quizCount'] = Count($quizzes);

			

			if ($segment == 'save' && $quiz == 'question') 
			{
				$questionId = 0;
				$questionId = Input::post('questionId');
				$question = Input::post('question');
				
				

				if($questionId > 0)
				{
					Admin::UpdateQuestion($problem, $pageId, $languageId, $question, $time, $userId[1], $questionId, $quizId);
				}
				else
				{
					Admin::SaveQuestion($problem, $pageId, $languageId, $question, $time, $userId[1], $quizId);
				}
			}
			elseif ($segment == 'save' && $quiz == 'option')
			{
				$quizId = 0;
				$quizId = Input::post('quizId');

				$optionId = 0;
				$optionId = Input::post('optionId');
				$correctAnswer = Input::post('correctAnswer');
				$optionText = Input::post('optionText');
			
				

				if ($optionId > 0) 
				{
					# code...
					Admin::UpdateOption($problem, $pageId, $languageId, $correctAnswer, $optionText, $time, $userId[1], $optionId, $quizId);

				}
				else
				{
					Admin::SaveOption($problem, $pageId, $languageId, $correctAnswer, $optionText, $time, $userId[1], $quizId);
					echo $quizId;
				}				
			}
			elseif($segment == 'save' && !$quiz)
			{
				$quizId = 0;
				$quizId = Input::post('quizId');
				$draft = Input::post('draft');
				$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");
				if($quizId > 0)
				{

					Admin::UpdateQuiz($pageId, $languageId, $time, $draft, $userId[1], $quizId);

				}
				else
				{
					$qId = Admin::SaveQuiz($pageId, $languageId, $time, $draft, $userId[1]);
					echo $qId;
				}	
			}
			elseif($segment == 'delete' && !$quiz)
			{
				$quizId = 0;
				$quizId = Input::post('quizId');

				Admin::DeleteQuiz($quizId);
			}
			elseif($segment == 'loading' && !$quiz)
			{
				$show_per_page = input::post('show_per_page');
				$currentPage = input::post('currentPage');

				$quizzes = Admin::Quiz($branchOrder, $show_per_page, $currentPage)->as_array('QuizId');
				$data['quizResult'] = $quizzes;

				foreach ($quizzes as $quizId => $quizEntry) 
				{
					$quizlinks = Admin::LessonLinks($quizEntry['PagesId'], $quizEntry['LanguageId'])->as_array('LinksId');
					$data['quizLinks'][$quizId] = $quizlinks;

					$questionsList = Admin::Questions($quizId)->as_array('QuizQuestionId');
					
					$data['questionList'][$quizId] = $questionsList;

					foreach ($questionsList as $questionId => $questionItem) 
					{
						$questionProblem = $questionItem['Problem'];

						$optionsList = Admin::Options($quizId, $questionProblem)->as_array('QuizOptionsId');

						$data['options'][$quizId][$questionId] =  $optionsList;

					}
				}
				$view = View::forge('admin/loadquiz', $data, false);
				return $view;
			}
			else
			{
				$view = View::forge('admin/layout');
				$view->head = View::forge('admin/head');
				$view->header = View::forge('admin/header');
				$view->nav = View::forge('admin/nav', $data, false);
				$view->content = View::forge('admin/quiz', $data);  
				$view->footer = View::forge('admin/footer');
				return $view;
			}

			
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_problems()
	{
		Lang::load('admin');
		$auth = Auth::instance();
		$group = 6;
		$problemsCount = Input::post('problemsCount');
		if ($auth->check() && $auth->member($group) && $problemsCount > 0)
		{
			$data['problemsCount'] = $problemsCount;
			$view = View::forge('admin/problems', $data, false);
			return $view;

		}
		else
		{
			$url = Uri::generate('admin');
	    		// credentials ok, go right in
	    	Response::redirect($url);
		}
	}

	public function action_options()
	{
		Lang::load('admin');

		$auth = Auth::instance();
		$group = 6;
		$optionsCount = Input::post('optionsCount');
		$problem = Input::post('problem');
		$quizId = Input::post('quizId');
		if ($auth->check() && $auth->member($group) && $optionsCount > 0)
		{
			$data['optionCount'] = $optionsCount;
			$data['problem'] = $problem;
			$data['quizId'] = 0;
			$data['optionId'] = 0;
			
			if($quizId)
			{
				$data['quizId'] = $quizId;
			}

			$view = View::forge('admin/options', $data, false);
			return $view;

		}
		else
		{
			$url = Uri::generate('admin');
	    		// credentials ok, go right in
	    	Response::redirect($url);
		}
	}

	public function action_language()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			//language/select/
			$segment = Uri::Segment(3);

			if ($segment == "select") 
			{
				//get languages and save to Json

				$languages = Admin::Languages()->as_array('LanguageId');;

				foreach ($languages as $languageId => $language) 
				{
					$array[''.$languageId.''] = ''.$language['Name'].'';
				}

				$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
				print json_encode($array);
			}
			else
			{

			}


		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_registration()
	{
		$auth = Auth::instance();
		$val = Validation::forge();

		Lang::load('admin');

		$group = 6;
		if ($auth->check() && $auth->member($group))
		{
			

			$data['loginTitle'] = Lang::get('LoginTitle');
			$data['signInHeading'] = Lang::get('SignInHeading');
			$data['rememberMe'] = Lang::get('RememberMe');
			$data['signIn'] = Lang::get('SignIn');
			$data['username'] = Lang::get('Username');
			$data['password'] = Lang::get('Password');
			$data['emailAddress'] = Lang::get('EmailAddress');
			$data['active'] = Uri::Segment(2);
			$site = Uri::Segment(3);

			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");


			if ($site == 'saveCurrency') 
			{
				$val->add_field('currencyName', 'Currency Name', 'required');
				$val->add_field('currencyCode', 'Currency Code', 'required');
				if ($val->run())
				{
					$currencyName = input::post('currencyName');
					$currencyCode = input::post('currencyCode');

					Admin::SaveCurrency($currencyCode, $currencyName, $time);

					$url = Uri::generate('admin/registration');
	    			// credentials ok, go right in
	    			Response::redirect($url);
		
				}
				else
				{

				}

				
			}
			elseif ($site == 'saveAdmin')
			{
				$val->add_field('username', 'Your username', 'required');
				$val->add_field('password', 'Your password', 'required|min_length[3]|max_length[10]');

				
				if ($val->run())
				{
					$username = input::post('username');
					$password = input::post('password');
					$email = input::post('email');
					$group = 6;

				    // process your stuff when validation succeeds
				    $auth->create_user($username, $password, $email, $group);
				    $url = Uri::generate('admin/registration');
	    			// credentials ok, go right in
	    			Response::redirect($url);
				}
				else
				{
				    // validation failed
				}
			} 
			elseif($site == 'savePriceList')
			{
				$val->add_field('priceperstudentpermonth', 'Price per student per month', 'required');
				$val->add_field('priceperschoolpermonth', 'Price per school per month', 'required');
				$val->add_field('priceperteacherpermonth', 'Price per teacher per month', 'required');
				$val->add_field('priceperstudentperyear', 'Price per student per year', 'required');
				$val->add_field('priceperschoolperyear', 'Price per school per year', 'required');
				$val->add_field('priceperteacherperyear', 'Price per teacher per year', 'required');
		

				if ($val->run())
				{
					$priceperstudentpermonth = input::post('priceperstudentpermonth');
					$priceperschoolpermonth = input::post('priceperschoolpermonth');
					$priceperteacherpermonth = input::post('priceperteacherpermonth');
					$priceperstudentperyear = input::post('priceperstudentperyear');
					$priceperschoolperyear = input::post('priceperschoolperyear');
					$priceperteacherperyear = input::post('priceperteacherperyear');
					$currencyId = input::post('currencyId');

					Admin::savePriceList($priceperstudentpermonth, $priceperschoolpermonth, $priceperteacherpermonth, $priceperstudentperyear, $priceperschoolperyear, $priceperteacherperyear, $currencyId, $time);

				}
			}

			$languages = Admin::Languages();
			$data['languages'] = $languages->as_array('LanguageId');

			$currency = Admin::Currency()->as_array('CurrencyId');
			$data['currencyList'] = $currency;

			$priceList = Admin::PriceList()->as_array('PriceListId');
			$data['priceListList'] = $priceList;

			$superAdmin = Admin::Users(6)->as_array();
			$data['superAdminList'] = $superAdmin;

			$view = View::forge('admin/layout');
			$view->head = View::forge('admin/head');
			$view->header = View::forge('admin/header');
			$view->nav = View::forge('admin/nav', $data, false);
			$view->content = View::forge('admin/registration', $data);  
			$view->footer = View::forge('admin/footer');

			return $view;
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_logout()
	{
		$auth = Auth::instance();
		$auth->logout();

		$url = Uri::generate('admin');
	    // credentials ok, go right in
	           
	    Response::redirect($url);
	}

	public function action_pages()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group, $user = null) ) 
		{
			(string)$segment = Uri::Segment(3);
			(int)$pagesId = 0;


			$pagesId = Input::post('pagesId');
			$pagesName = Input::post('pagesName');
			$branchOrder = Input::post('branchOrder');
			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");

			$val = Validation::forge();
			$val->add_callable('Unique');
			$val->add('pagesName', 'Name')->add_rule('required')->add_rule('unique', 'Pages.Name');

			if ($segment == "save" ) 
			{
				if ($val->run() && $pagesId > 0)
				{
					Admin::UpdatePages($pagesId, $pagesName,$time);
				}
				elseif ($val->run() && $pagesId == 0)
				{
					Admin::SavePages($pagesName, $branchOrder, $time);
				}
				else
				{
					echo $val->error('pagesName');
				}
			}
			elseif ($segment == "select") 
			{
					$branchOrder = Uri::Segment(4);
		
					$pages = Admin::Pages($branchOrder);
					$pagesResult = $pages->as_array('PagesId');

					foreach ($pagesResult as $pagesId => $page) 
					{
						$array[''.$pagesId.''] = ''.$page['Name'].'';	
					}

					print json_encode($array);
			}
			elseif ($segment == 'delete') 
			{
				$pagesId = Input::post('pagesId');
				Admin::DeletePages($pagesId);
				echo "OK";
			}
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_links()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group, $user = null) ) 
		{

			$segment = Uri::Segment(3);
			$parent = Input::post('parent');
			$languageId = Input::post('languageId');
			$title = Input::post('title');
			$pagesId = Input::post('pagesId');
			$linksId = Input::post('linksId');
			$order = Input::post('order');
			
			$userId = $auth->get_user_id(1);
			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");

			$val = Validation::forge();
			$val->add('title', 'Title')->add_rule('required');

			if ($segment == 'save') 
			{
				if ($val->run() && $linksId > 0) 
				{
					$column = Input::post('columnName');
					
					Admin::UpdateLink($linksId, $column, $title, $time);
				}
				elseif($val->run() && $linksId == 0)
				{
					Admin::SaveLink($title, $languageId, $pagesId, $parent, $order, $userId[1], $time);
				}
				else
				{
					echo $val->error('title');
				}
				
			}
			elseif ($segment == 'delete') 
			{
				echo 'tókst';
				Admin::DeleteLinks($linksId);
			}

		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}
	}

	public function action_description()
	{
		$auth = Auth::instance();
		$group = 6;
		if ($auth->check() && $auth->member($group, $user = null)) 
		{
			$descriptionId = 0;
			$userId = $auth->get_user_id(1);
			$time = Date::forge()->format("%Y-%m-%d %H:%M:%S");
			$linksId = Input::post('linksId');
			$description = Input::post('description');
			$draft = Input::post('draft');
			$descriptionId = Input::post('descriptionId');
			$segment = Uri::Segment(3);

			$val = Validation::forge();
			$val->add('description', 'Description')->add_rule('required');

			if ($segment == "save") 
			{
				if ($val->run() && $descriptionId > 0) 
				{
					Admin::UpdateDescription($linksId, $descriptionId, $description, $time, $draft);
				}
				else if($val->run() && $description == 0)
				{
					
					Admin::SaveDescription($linksId, $description, $time, $userId[1], $draft);
				}
				else
				{
					echo $val->error('description');
				}
			}
			elseif($segment = "delete")
			{
				
				echo $descriptionId;
				Admin::DeleteDescription($descriptionId);


			}
			
		}
		else
		{
			$url = Uri::generate('admin');
	    	// credentials ok, go right in
	           
	    	Response::redirect($url);
		}	
	}
}
?>