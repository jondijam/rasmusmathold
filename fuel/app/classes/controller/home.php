<?php
use \Model\Home;
use \Model\Admin;
use \Lang;
class Controller_Home extends Controller
{
	public function action_index()
	{
		//define variables
		(string) $lastLogin = "";
		(string) $language = "";
		$language = Config::get('language');

		//load db, language, Auth
		$auth = Auth::instance();
		$data['auth'] = $auth;

		Lang::load('home');
		$languages = Home::Languages();
		$data['languages'] = $languages->as_array('LanguageId');
		if ($auth->check())
		{
			$timestamp = $auth->get_user_last_login();

			$firstTimeLogin = Session::get('new');
			$data['firstTimeLogin'] = $firstTimeLogin;

			$schoolLevels = Home::SchoolLevelLinks($language);
			$schoolLevelsList = $schoolLevels->as_array('LinksId');
			$data['schoolLevels'] = $schoolLevelsList;

			$data['active'] = Uri::Segment(2);
			$schoolLevelPage = Uri::segment(3);
			$chapter = Uri::segment(4);
			$subchapter = Uri::segment(5);
			$lesson = Uri::segment(6);
			$quizPage = Uri::segment(7);

			$data['chapterPage'] = $chapter;
			$data['schoolLevelPage'] = $schoolLevelPage;
			$data['subChapterPage'] = $subchapter;
			$data['lessonPage'] = $lesson;
			$data['quizPage'] = $quizPage;

			$user = $auth->get_user_id(1);
			$data['userId'] = $user[1];
			$userId = $user[1];

			foreach ($schoolLevelsList as $linksId => $entry) 
			{
				if ($schoolLevelPage == $entry['Pages']) 
				{
					$branchOrder = 2;
					$chaptersList = Home::Chapters($linksId, $branchOrder);
					$chaptersResult = $chaptersList->as_array('LinksId');
					$data['chapters'] = $chaptersResult;

					foreach ($chaptersResult as $chapterId => $chapterItem) 
					{
						if ($chapter == $chapterItem['Pages']) 
						{
							$branchOrder = 3;
							$subChapterList = Home::Chapters($chapterId, $branchOrder);
							$subChapterResult = $subChapterList->as_array('LinksId');
							$data['subChapterList'] = $subChapterResult;

							foreach ($subChapterResult as $subchapterId => $subchapterEntry) 
							{
								
								if($subchapterEntry['Pages'] == $subchapter)
								{
									$branchOrder = 4;
									$lessonList = Home::Chapters($subchapterId, $branchOrder);
									$lessonResult = $lessonList->as_array('LinksId');
									$data['lessonList'] = $lessonResult;



									$branchOrder = 5;
									$quizList = Home::Chapters($subchapterId, $branchOrder);
									$quizResult = $quizList->as_array('LinksId');
									$data['quizList'] = $quizResult;

								}								
							}
						}
					}
				}
			}

			$schoolResult = Home::School($userId)->as_array('SchoolId');
			$data['schoolResult'] = $schoolResult;


			//Home::chapters($language, $schoolLevelPage);


			$view = View::forge('home/loginLayout');
			$view->head = View::forge('home/head');
			$view->header = View::forge('home/loginHeader', $data, false);
			$view->nav = View::forge('home/loginNav', $data, false);
			
			if ($schoolLevelPage && !$chapter && !$subchapter && !$lesson && !$quizPage) 
			{

				$branchOrder = 1;

				$link = Home::CheckLink($language, $schoolLevelPage);

				if ($link == 0 && $schoolLevelPage == 'ys') 
				{
					$url = Uri::generate('home/index/ms');
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}
				
				if ($link == 0 && $schoolLevelPage == 'ms') 
				{
					$url = Uri::generate('home/index/us');
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}
				
				if ($link == 0 && $schoolLevelPage == 'us') 
				{
					$url = Uri::generate('home/index/fs');
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}
				
				if ($link == 0 && $schoolLevelPage == 'fs') 
				{
					$url = Uri::generate('home/index');
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}
			
				$descriptionsList = Home::Descriptions($language, $schoolLevelPage, $branchOrder);
				$descriptionsResult = $descriptionsList->as_array();
				$data['descriptions'] = $descriptionsResult;
				
				$view->content = View::forge('home/schoolLevel', $data, false);  
			}
			elseif($schoolLevelPage && $chapter && !$subchapter && !$lesson && !$quizPage)
			{
				
				$link = Home::CheckLink($language, $chapter);

				if ($link == 0) 
				{
					$url = Uri::generate('home/index/'.$schoolLevelPage);
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}

				$branchOrder = 2;
				$descriptionsList = Home::Descriptions($language, $chapter, $branchOrder);
				$descriptionsResult = $descriptionsList->as_array();
				$data['descriptions'] = $descriptionsResult;

				$view->content = View::forge('home/chapter', $data, false);
			}
			elseif($schoolLevelPage && $chapter && $subchapter && !$lesson && !$quizPage)
			{
				
				$link = Home::CheckLink($language, $subchapter);

				if ($link == 0) 
				{
					$url = Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter);
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}


				$branchOrder = 3;
				
				$descriptionsList = Home::Descriptions($language, $subchapter, $branchOrder);
				$descriptionsResult = $descriptionsList->as_array();
				$data['descriptions'] = $descriptionsResult;

				$view->content = View::forge('home/subchapter', $data, false);
			}
			elseif($schoolLevelPage && $chapter && $subchapter && $lesson && !$quizPage)
			{
				
				$link = Home::CheckLink($language, $lesson);

				if ($link == 0) 
				{
					$url = Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter.'/'.$subchapter);
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}



				$branchOrder = 4;
				$data['questions'] = "";
				$data['lesson'] = "";
				
				$lessons = Home::Lesson($language, $lesson)->as_array();

				$data['lessons'] = $lessons;
				$view->content = View::forge('home/lesson', $data, false);
			}
			elseif($schoolLevelPage && $chapter && $subchapter && $lesson && $quizPage)
			{
				
				$link = Home::CheckLink($language, $quizPage);

				if ($link == 0) 
				{
					$url = Uri::generate('home/index/'.$schoolLevelPage.'/'.$chapter.'/'.$subchapter);
			    	// credentials ok, go right in
			           
			    	Response::redirect($url);
				}


				$branchOrder = 5;
				$quiz =	Home::Quiz($language, $quizPage)->as_array('QuizId');
				$data['quiz'] = $quiz;

				foreach ($quiz as $quizId => $quizItem) 
				{

					$questions = Home::Questions($quizId)->as_array('QuizQuestionId');

					$data['questions'][$quizId] =  $questions;
					foreach ($questions as $questionId => $question) 
					{
						# code...
						$options = Home::Options($quizId, $question['Problem'])->as_array('QuizOptionsId');
						$data['options'][$quizId][$questionId] = $options;

					}

					
				}

				

				$view->content = View::forge('home/quiz', $data, false);
			}
			else
			{
				
				$userQuizResult = Home::UsersQuiz($userId)->as_array();
				$count = 0;

				$data['userQuizResult'] = $userQuizResult;
				foreach ($userQuizResult as $userQuizKey => $userQuizEntry) 
				{
					$attemptResult[$userQuizKey] = Home::UsersQuizAttempt($userQuizEntry['QuizId'])->as_array();
					$countAttempt = 0;
					
					foreach ($attemptResult[$userQuizKey] as $attemptkey => $attemptEntry) 
					{
						$countAttempt++;
						$countGrades = 0;
						$grades[$userQuizKey][$attemptkey] = Home::Grades($attemptEntry['Attempt'], $userQuizEntry['QuizId'])->as_array();
						$data['grade'][$userQuizKey][$countAttempt] = count($grades[$userQuizKey][$attemptkey]);

					}

					$attemptCount[$userQuizKey] = COUNT($attemptResult[$userQuizKey]);
					$data['quizAttemptCount'][$userQuizKey] = $attemptCount[$userQuizKey];
					$max = max($attemptCount);


					if ($max == $attemptCount[$userQuizKey]) 
					{
						$count = $max;
					}				
				}

				$data['attemptCount'] = $count;

				$view->content = View::forge('home/loginIndex', $data, false);  
			}
			
			
			
			$view->footer = View::forge('home/footer', $data, false);

			return $view;
		}
		else
		{		
			$notifications = Home::Notifications($language);	
			$data['notifications'] = $notifications->as_array();

			$articles = Home::Articles($language);

			$data['articles'] = $articles->as_array();

			$view = View::forge('home/layout');
			$view->head = View::forge('home/head');
			$view->header = View::forge('home/header', $data, false);
			$view->nav = View::forge('home/nav');
			$view->content = View::forge('home/content', $data, false);  
			$view->footer = View::forge('home/footer');



			return $view;
		}
	}

	public function action_login()
	{
		Lang::load('Home');
		$auth = Auth::instance();

		$languages = Home::Languages();
		$data['languages'] = $languages->as_array('LanguageId');

		if ($auth->login(Input::post('username'), Input::post('password')))
		{
			$url = Uri::generate('home');
	        
	       
	        //credentials ok, go right in
	       	Response::redirect($url);
		}
		else
		{

			$data['username'] = Input::post('username');
	        $data['login_error'] = 'Wrong username/password combo. Try again';

	        Uri::generate();
			$language = Config::get('language');
			$notifications = Home::Notifications($language);	
			$data['notifications'] = $notifications->as_array();

			$articles = Home::Articles($language);
			$data['articles'] = $articles->as_array();

			$view = View::forge('home/layout');
			$view->head = View::forge('home/head');
			$view->header = View::forge('home/header', $data, false);
			$view->nav = View::forge('home/nav');
			$view->content = View::forge('home/login', $data, false);  
			$view->footer = View::forge('home/footer');

			return $view;
		}
	}

	public function action_profile()
	{

	}

	public function action_school()
	{
		Lang::load('home');
		$auth = Auth::instance();
		$data['auth'] = $auth;

		$languages = Home::Languages();
		$data['languages'] = $languages->as_array('LanguageId'); 

		$language = Config::get('language');
		$data['language'] = $language;

		
		if ($auth->check()) 
		{
			$group = 6;
			$user = $auth->get_user_id(1);
			$data['userId'] = $user[1];;
			$userId = $user[1];
			# code..
			if ($auth->member(6) || $auth->member(5) || $auth->member(4)) 
			{
				# code...
				$data['active'] = Uri::Segment(2);

				$schoolResult = Home::School($userId)->as_array('SchoolId');
				$data['schoolResult'] = $schoolResult;

				$view = View::forge('home/loginLayout');
				$view->head = View::forge('home/head');
				$view->header = View::forge('home/loginHeader', $data, false);
				$view->nav = View::forge('home/loginSchoolNav', $data, false);
				$view->content = View::forge('home/schoolHome', $data, false);
				$view->footer = View::forge('home/schoolFooter', $data, false);

				return $view;
			}
			else
			{
				$url = Uri::generate('home');
		    	// credentials ok, go right in
		           
		    	Response::redirect($url);
			}

			

		}
		else
		{

		
		$site = Uri::segment(3);
		//$country_code = geoip_country_code_by_name(Input::real_ip());


		$currency = Admin::Currency()->as_array('CurrencyId');
		$data['currencyList'] = $currency;

		$priceList = Admin::PriceList()->as_array('PriceListId');
		$data['priceList'] = $priceList;

		$countries = Home::Country()->as_array('id');
		$data['country'] = $countries;

		$priceStudentMonth = 0;
		$priceSchoolMonth = 0;
		$priceTeacherMonth = 0;
		$priceStudentYear = 0;
		$priceSchoolYear = 0;
		$priceTeacherYear = 0;


		foreach ($priceList as $pricelistId => $pricelistItem) 
		{
			if($pricelistItem['CurrencyName'] == "euro")
			{
				$priceStudentMonth = $pricelistItem['StudentPriceMonth'];
				$priceSchoolMonth = $pricelistItem['SchoolPriceMonth'];
				$priceTeacherMonth = $pricelistItem['TeacherPriceMonth'];
				$priceStudentYear = $pricelistItem['StudentPriceYear'];
				$priceSchoolYear = $pricelistItem['SchoolPriceYear'];
				$priceTeacherYear = $pricelistItem['TeacherPriceYear'];
			}					# code...
		}

		$data['priceStudentMonth'] = $priceStudentMonth;
		$data['priceSchoolMonth'] = $priceSchoolMonth;
		$data['priceTeacherMonth'] = $priceTeacherMonth;
		$data['priceStudentYear'] = $priceStudentYear;
		$data['priceSchoolYear'] = $priceSchoolYear;
		$data['priceTeacherYear'] = $priceTeacherYear;
		$data['totalPriceMonth'] = $priceSchoolMonth + $priceStudentMonth + $priceTeacherMonth;
		$data['totalPriceYear'] = $priceSchoolYear + $priceStudentMonth + $priceTeacherYear;


	
		$notifications = Home::Notifications($language);	
		$data['notifications'] = $notifications->as_array();

		$articles = Home::Articles($language);
		$data['articles'] = $articles->as_array();
		if ($site == 'priceList') 
		{
			$currencyName = Uri::segment(4);	

			$CurrencyPriceList = Home::CurrencyPriceList($currencyName)->as_array('PriceListId');

			

			foreach ($CurrencyPriceList as $CurrencyPriceListId => $CurrencyPriceItem) 
			{
					# code...
					$priceStudentMonth = $CurrencyPriceItem['StudentPriceMonth'];
					$priceSchoolMonth = $CurrencyPriceItem['SchoolPriceMonth'];
					$priceTeacherMonth = $CurrencyPriceItem['TeacherPriceMonth'];
					$priceStudentYear = $CurrencyPriceItem['StudentPriceYear'];
					$priceSchoolYear = $CurrencyPriceItem['SchoolPriceYear'];
					$priceTeacherYear = $CurrencyPriceItem['TeacherPriceYear'];

					$array = array("priceStudentMonth" => $priceStudentMonth, 
						"priceSchoolMonth" => $priceSchoolMonth,
						"priceTeacherMonth" => $priceTeacherMonth, 
						"priceStudentYear" => $priceStudentYear,
						"priceSchoolYear" => $priceSchoolYear , 
						"priceTeacherYear" => $priceTeacherYear);

					echo json_encode($array);
				

			}	
		}
		elseif($site == 'registration')
		{
			Lang::load('home');
			$view = View::forge('home/registration', $data, false);
			return $view;
		}
		else
		{
			$view = View::forge('home/layout');
			$view->head = View::forge('home/head');
			$view->header = View::forge('home/header', $data, false);
			$view->nav = View::forge('home/nav');
			$view->content = View::forge('home/school', $data, false);  
			$view->footer = View::forge('home/footer');
			
			return $view;
		}
		}	

		
	}

	public function action_indvidual()
	{
		Lang::load('home');
		$languages = Home::Languages();
		$data['languages'] = $languages->as_array('LanguageId'); 
		$language = Config::get('language');

		$notifications = Home::Notifications($language);	
			$data['notifications'] = $notifications->as_array();

			$articles = Home::Articles($language);
			$data['articles'] = $articles->as_array();

		$view = View::forge('home/layout');
			$view->head = View::forge('home/head');
			$view->header = View::forge('home/header', $data, false);
			$view->nav = View::forge('home/nav');
			$view->content = View::forge('home/indvidual', $data, false);  
			$view->footer = View::forge('home/footer');

			return $view;
	}

	public function action_addSchool()
	{
		$schoolNumber = input::post('schoolNumber');
		Lang::load('home');

		if(isset($schoolNumber))
		{
			$data['schoolNumber'] = $schoolNumber;
			$countries = Home::Country()->as_array('id');
			$data['country'] = $countries;


			$view = View::forge('home/addSchool', $data, false);
		}
		else
		{
			$url = Uri::generate('home/school');
	    		// credentials ok, go right in
	    	Response::redirect($url);
		}
		
		return $view;
	}

	public function action_email()
	{
		Lang::load('home');

			$email = Email::forge();

				// Set the from address
				$email->from('noreply@rasmusmath.com', 'rasmusmath.com');
				$email->priority(\Email::P_HIGH);
				// Set the to address
				$email->to('jondi@formastudios.com', 'Jondi');

				// Set a subject
				$email->subject(Lang::get('ConfirmationMail'));

				// Set multiple to addresses
				$email_data['name'] = 'Jondi';
				$email_data['password'] = 'Jondi';
				$email_data['username'] = '123';
				$email_data['userId'] = 2;
				$email_data['activeCode'] = 'ddddaead';

				$email->html_body(\View::forge('email/confirmTemplate', $email_data, false));

				try
				{
				    $email->send();
				    echo 'tókst';
				}
				catch(\EmailValidationFailedException $e)
				{
				    // The validation failed
				    echo 'failed';
				    echo $e;
				}
				catch(\EmailSendingFailedException $e)
				{
				    // The driver could not send the email
				    echo 'Driver did not work.';
				    echo $e;
				}
	}

	public function action_registration()
	{
		Lang::load('home');
		$site = Uri::segment(3);
		$auth = Auth::instance();
		$val = Validation::forge();
		$val->add_callable('Unique');
		$email = Email::forge();
		
		if ($site == 'saveContact') 
		{
			
			$username = Input::post('contactUsername');
			$password = Input::post('contactPassword');
			$contactEmail = Input::post('contactEmail');
			$name = Input::post('contactName');
			$phone = Input::post('contactPhone');
			$position = Input::post('contactPosition');
			$newsletter = Input::post('newsletter');
			$terms = Input::post('terms');
		

			$val->add('contactUsername', Lang::get('Username'), array(), array('trim', 'strip_tags', 'required'))->add_rule('unique', 'users.username');
			$val->add('contactEmail', Lang::get('Email'))->add_rule('valid_email')->add_rule('required')->add_rule('unique', 'users.email');
			$val->add('contactName', Lang::get('Name'))->add_rule('required');
			$val->add('contactPassword', Lang::get('Password'))->add_rule('required');
			$val->add('contactPassConf', Lang::get('PasswordConfirmation'))->add_rule('required')->add_rule('match_value',$password , true);
			$val->add('terms', Lang::get('Terms'))->add_rule('match_value', '1' , true);
			if ($val->run())
			{	
				$active = $auth->hash_password(rand(30000, 50000000));

				$userId = $auth->create_user($username, $password, $contactEmail, $group = 5, array('name' => $name, 'phone' => $phone, 'position' => $position, 'newsletter' => $newsletter), $active);

				$json =  array('errorBool' => '0', 'userId' => $userId);

				echo json_encode($json);

				//send email to contact

				// Set the from address
				$email->from('noreply@rasmusmath.com', 'rasmusmath.com');
				$email->priority(\Email::P_HIGH);
				// Set the to address
				$email->to($contactEmail, $name);

				// Set a subject
				$email->subject(Lang::get('ConfirmationMail'));

				// Set multiple to addresses
				$email_data['name'] = $name;
				$email_data['password'] = $username;
				$email_data['username'] = $password;
				$email_data['userId'] = $userId;
				$email_data['activeCode'] = $active;

				$email->html_body(\View::forge('email/confirmTemplate', $email_data));

				try
				{
				    $email->send();
				}
				catch(\EmailValidationFailedException $e)
				{
				    // The validation failed
				}
				catch(\EmailSendingFailedException $e)
				{
				    // The driver could not send the email
				}
								
			}
			else
			{
				$nameError = "";
				$usernameError = ""; 
				$emailError = "";	
				$passwordConf = "";
				$passwordError = "";
				$termsError = "";

				if ($val->error('contactUsername')) 
				{
					$usernameError = $val->error('contactUsername')->get_message(false, '','');
				}

				if ($val->error('contactEmail')) 
				{
					$emailError = $val->error('contactEmail')->get_message(false, '','');
				}

				if ($val->error('contactPassword')) 
				{
					$passwordError = $val->error('contactPassword')->get_message(false, '','');
				}

				if ($val->error('contactName')) 
				{
					$nameError = $val->error('contactName')->get_message(false, '','');
				}

				if ($val->error('contactPassConf')) 
				{
					# code...
					$passwordConf = $val->error('contactPassConf')->get_message(false, '','');
				}
				
				if ($val->error('terms')) 
				{
					# code...
					$termsError = $val->error('terms')->get_message(false, '','');
				}
				
				$json  = array('errorBool'=>'1','usernameError' => $usernameError, 'emailError' => $emailError, 'passwordError' => $passwordError, 'passwordConf' => $passwordConf, 'nameError'=>$nameError, 'termsError'=> $termsError );
				echo json_encode($json);
			}
		
			# code...
		}
		elseif($site == 'savePayer')
		{

			$payerEmail = Input::post('payerEmail');
			$name = Input::post('payerName');
			$customerNumber = Input::post('payerCustomerNumber');
			$ssn = Input::post('payerCustomerNumber');

			$address = Input::post('payerAddress');
			$zip = Input::post('payerZip');
			$city = Input::post('payerCity');
			$country = Input::post('payerCountry');
			$paymentMethod = Input::post('paymentMethod');
			$currency = Input::post('currency');
			
			$val->add('payerEmail', Lang::get('Email'))->add_rule('valid_email')->add_rule('required')->add_rule('unique', 'Payer.Email');
			$val->add('payerName', Lang::get('Name'))->add_rule('required');
			$val->add('payerCustomerNumber', Lang::get('CustomerNumber'))->add_rule('required')->add_rule('unique', 'Payer.SSN');
			$val->add('payerAddress', Lang::get('Address'))->add_rule('required');
			$val->add('payerZip', Lang::get('Zip'))->add_rule('required');
			$val->add('payerCity', Lang::get('City'))->add_rule('required');

			if ($val->run()) 
			{				
				$active = $auth->hash_password(rand(30000, 50000000));
				# code...
				$payerId = Home::RegisterPayer($payerEmail, $name, $customerNumber, $ssn, $address, $zip, $city, $country, $active, $paymentMethod);
				$json = array('payerId' => $payerId, 'errorBool' => 0 );
				echo json_encode($json);

				$email = Email::forge();

				// Set the from address
				$email->from('noreply@rasmusmath.com', 'rasmusmath.com');
				$email->priority(\Email::P_HIGH);
				// Set the to address
				$email->to($payerEmail, $name);

				// Set a subject
				$email->subject(Lang::get('ConfirmationMail'));

				// Set multiple to addresses
				$email_data['name'] = $name;
				$email_data['payerId'] = $payerId;
				$email_data['activeCode'] = $active;

				$email->html_body(\View::forge('email/payerConfirmTemplate', $email_data));

				try
				{
				    $email->send();
				}
				catch(\EmailValidationFailedException $e)
				{
				    // The validation failed
				}
				catch(\EmailSendingFailedException $e)
				{
				    // The driver could not send the email
				}

				//send payer email


			}
			else
			{
				(string) $payerNameError = "";
				(string) $emailError = "";
				(string) $customerNumberError = "";
				(string) $addressError = "";
				(string) $zipError = "";
				(string) $cityError = "";


				if ($val->error('payerEmail')) 
				{
					$emailError = $val->error('payerEmail')->get_message(false, '','');
				}

				if ($val->error('payerName')) 
				{
					$payerNameError = $val->error('payerName')->get_message(false, '','');
				}

				if ($val->error('payerCustomerNumber')) 
				{
					$customerNumberError = $val->error('payerCustomerNumber')->get_message(false, '','');
				}

				if ($val->error('payerAddress')) 
				{
					$addressError = $val->error('payerAddress')->get_message(false, '','');
				}
				
				if ($val->error('payerZip')) 
				{
					$zipError = $val->error('payerZip')->get_message(false, '','');
				}
				
				if ($val->error('payerCity')) 
				{
					$cityError = $val->error('payerCity')->get_message(false, '','');
				}

				$json  = array('errorBool'=>'1','payerName' => $payerNameError, 'emailError' => $emailError, 'customerNumberError' => $customerNumberError, 'addressError' => $addressError, 'zipError'=>$zipError,'cityError' => $cityError);

				echo json_encode($json);
			}	
		}
		elseif($site == 'saveSchool')
		{
	
			$name = Input::post('name');
			$customerNumber = Input::post('customerNumber');
			$address = Input::post('address');
			$zip = Input::post('zip');
			$city = Input::post('city');
			$country = Input::post('country');
			$reference = Input::post('reference');
			$teacherCount = Input::post('teacherCount');
			$studentCount = Input::post('studentCount');

			$contactId = Input::post('contactId');
			$contactName = Input::post('contactName');
			$contactEmail = Input::post('contactEmail');
			$payerId = Input::post('payerId');
			$subscriptionPlan = Input::post('subscriptionPlan');

			$active = $auth->hash_password(rand(30000, 50000000));

			$val->add('name', Lang::get('Name'))->add_rule('required');
			$val->add('customerNumber', Lang::get('CustomerNumber'))->add_rule('required')->add_rule('unique', 'Schools.SSN');;
			$val->add('address', Lang::get('Address'))->add_rule('required');
			$val->add('zip', Lang::get('Zip'))->add_rule('required');
			$val->add('city', Lang::get('City'))->add_rule('required');

			if ($val->run())
			{	
				$schoolId = Home::saveSchool($name, $customerNumber, $address, $zip, $city, $country, $reference, $teacherCount, $studentCount, $payerId, $contactId, $subscriptionPlan);
				
				for ($s=1; $s <= $studentCount ; $s++) 
				{ 
					# code...
					$studentUsername[$s] = $schoolId.'student'.$s;
					$studentEmail[$s] = $schoolId.'student'.$s.'@rasmusmath.com';
					$studentPassword[$s] = rand(1000, 9000);

					$email_data['studentUsername'][$s] = $studentUsername[$s];
					$email_data['studentPassword'][$s] = $studentPassword[$s];

					$studentId[$s] = $auth->create_user($studentUsername[$s], $studentPassword[$s], $studentEmail[$s], $group = 2, array(), $active);
					
					Home::saveUsersSchool($studentId[$s], $schoolId);
				}

				for ($t=1; $t <= $teacherCount; $t++) 
				{ 
					

					$taecherUsername[$t] = $schoolId.'teacher'.$t;
					$teacherEmail[$t] = $schoolId.'teacher'.$t.'@rasmusmath.com';
					$teacherPassword[$t] = rand(1000, 9000);

					$email_data['teachersUsername'][$t] = $teacherUsername[$t];
					$email_data['teacherPassword'][$t] = $teacherPassword[$t];


					$teacherId[$t] = $auth->create_user($teacherUsername[$t], $teacherPassword[$t], $teacherEmail[$t], $group = 3, array(), $active);
					
					Home::saveUsersSchool($teacherId[$t], $schoolId);
				}



				$json = array('schoolId' => $schoolId, 'errorBool'=>0);
				echo json_encode($json);



				// Set the from address
				$email->from('noreply@rasmusmath.com', 'rasmusmath.com');
				$email->priority(\Email::P_HIGH);
				// Set the to address
				$email->to($contactEmail, $contactName);

				// Set a subject
				$email->subject(Lang::get('StudentsAndTeachersLoginsDetails'));

				// Set multiple to addresses
				$email_data['name'] = $name;

				$email->html_body(\View::forge('email/studentTeacherTemplate', $email_data));

				try
				{
				    $email->send();
				}
				catch(\EmailValidationFailedException $e)
				{
				    // The validation failed
				    echo $e;
				}
				catch(\EmailSendingFailedException $e)
				{
				    // The driver could not send the email
				    echo $e;
				}

			}
			else
			{
				(string) $schoolName = "";
				(string) $schoolCustomerNumber = "";
				(string) $schoolAddress = "";
				(string) $schoolZip = "";
				(string) $schoolCity = "";
				if ($val->error('name')) 
				{
					$schoolName = $val->error('name')->get_message(false, '','');
				}

				if ($val->error('customerNumber')) 
				{
					$schoolCustomerNumber = $val->error('customerNumber')->get_message(false, '','');
				}

				if ($val->error('address')) 
				{
					$schoolAddress = $val->error('address')->get_message(false, '','');
				}

				if ($val->error('zip')) 
				{
					$schoolZip = $val->error('zip')->get_message(false, '','');
				}

				if ($val->error('city')) 
				{
					$schoolCity = $val->error('city')->get_message(false, '','');
				}

				$json = array('errorBool'=> 1,'schoolName' => $schoolName, 'customerNumber' => $schoolCustomerNumber, 'address'=> $schoolAddress, 'schoolZip'=>$schoolZip, 'schoolCity'=> $schoolCity);
				echo json_encode($json);
			}
		}
		//Home::();
	}

	public function action_confirm()
	{

		$userId = Uri::segment(3);
		$active = Uri::segment(4);

		Home::confirmUser($userId, $active);
		$schoolResult = Home::schoolsUsers($userId);


		$view = View::forge('home/layout');
		$view->head = View::forge('home/head');
		$view->header = View::forge('home/header', $data, false);
		$view->nav = View::forge('home/nav');
		$view->content = View::forge('home/confirm', $data, false);  
		$view->footer = View::forge('home/footer');
			
		return $view;
	}

	public function action_logout()
	{
		$auth = Auth::instance();
		if ($auth->check())
		{
			$auth->logout();
			$url = Uri::generate('home');
		    // credentials ok, go right in          
		    Response::redirect($url);
		}
		else
		{
			$url = Uri::generate('home');
		    // credentials ok, go right in
		           
		    Response::redirect($url);
		}
	}

	public function action_checkAnswers()
	{
		(int) $quizId = 0;
		(int) $optionId = 0;
		(int) $optionValue = 0;
		(int) $problem = 0;
		(int) $userCount = 0;
		(int) $grade = 0;
		(int) $userId = 0;
		(int) $attempt = 0;
		(int) $oldattempt = 0;

		$optionValueArray = Array();
		$optionIdValueArray = Array();

		$quizId = Input::post('quizId');
		$optionId = Input::post('optionId');
		$optionValue = Input::post('optionValue');
		$problem = Input::post('problem');
		$attempt = Input::post('userCount');

		$userId = Input::post('userId');

		$attemptList = Home::checkAttempt($quizId, $problem, $userId)->as_array();

		foreach ($attemptList as $key => $attemptItem) 
		{
			if($key == 0)
			{
				$oldattempt = $attemptItem['Attempt'];
			}
		}

	
			# code...
		$totalAttempt = ($oldattempt + 1);
		if ($optionValue == -1 && $optionId > 0) 
		{
			# code...
			$result = Home::CheckAnswer($optionId)->as_array();

			foreach ($result as $key => $item) 
			{
				if ($item['CorrectAnswer']) 
				{
					$grade = 1;
				}
			}
		}
		else
		{
			

			$optionValueArray = json_decode($optionValue);
			$optionIdValueArray = json_decode($optionId);
			$correctCounter = 0;
			$correctValueCounter = 0;
			$incorrect = 0;

			for ($i=0; $i < sizeof($optionValueArray); $i++) 
			{ 
				$optionResult[$i] = Home::CheckAnswer($optionIdValueArray[$i])->as_array();

				foreach ($optionResult[$i] as $key => $optionItem) 
				{

					if ($optionItem['CorrectAnswer']) 
					{
						# code...
						$correctCounter++;
					}

					if (!$optionItem['CorrectAnswer']) 
					{
						$incorrect++;
					}
					
					if ($optionItem['CorrectAnswer'] == $optionValueArray[$i] ) 
					{
						$correctValueCounter++;
					}	

				}
			}

			if($incorrect == $correctValueCounter && $incorrect == 3)
			{
				$grade = 1;
			}

			if ($correctValueCounter == $correctCounter) 
			{
				$grade = 1;
			}
		}

		Home::SaveGrades($quizId, $problem, $grade, $totalAttempt, $userId);
	}


	public function action_saveQuizAttempt()
	{
		(int) $quizId = 0;
		(int) $userId = 0;
		(int) $userAttempt = 0;
		(int) $oldAttempt = 0;
		(int) $totalAttempt = 0;

		$quizId = Input::post('quizId');
		$userId = Input::post('userId');
		$userAttempt = Input::post('userAttempt');

		$result = Home::CheckQuizAttempt($quizId, $userId)->as_array();

		foreach ($result as $key => $item) 
		{
			if ($key == 0) 
			{
				$oldAttempt = $item['Attempt'];
			}
		}

		echo $oldAttempt;
		$totalAttempt = $oldAttempt + 1;	

		Home::SaveQuizAttemp($userId, $quizId, $totalAttempt);
	}	
}

?>