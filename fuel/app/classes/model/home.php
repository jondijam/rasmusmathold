<?php
namespace Model;
use \DB;
class Home extends \Model 
{
	public static function Notifications($language)
	{
		$list = DB::select()->from('Notification')
		->join('Language','LEFT')->on('Language.LanguageId', '=', 'Notification.LanguageId')
		->where_open()
		->where('Code', '=', $language)
		->and_where('Publish', '=' , true)
		->and_where('Deleted', '=' ,false )
		->where_close()->execute();
		return $list;
	}

	public static function Articles($language)
	{
		$list = DB::select()->from('Article')->join('Language','LEFT')->on('Language.LanguageId', '=', 'Article.LanguageId')
		->where_open()
		->where('Code', '=', $language)
		->and_where('Publish', '=' ,true)
		->and_where('Deleted', '=' ,false )
		->where_close()->execute();

		return $list;
	}

	public static function Languages()
	{
		$list = DB::select()->from('Language')->execute();

		return $list;
	}

	public static function SchoolLevelLinks($language)
	{
		$list = DB::select('LinksId',array('Language.Name', 'Language'), array('Pages.Name','Pages'), 'Title')->from('Links')
		->join('Pages','LEFT')->on('Pages.PagesId','=','Links.PagesId')
		->join('Language','LEFT')->on('Language.LanguageId','=','Links.LanguageId')
		->where_open()
		->where('Code','=',$language)
		->and_where('BranchOrder','=','1')
		->and_where('Links.Deleted','=', 0)
		->and_where('Links.Draft','=', 0)
		->where_close()
		->execute();
		return $list;
	}

	public static function Chapters($linksId, $branchOrder)
	{

		$list = DB::select('LinksId',array('Pages.Name','Pages'), 'Title', 'Code')->from('Links')
		->join('Pages')->on('Pages.PagesId','=','Links.PagesId')
		->join('Language')->on('Language.LanguageId','=','Links.LanguageId')
		->where_open()
		->where('Parent','=',$linksId)
		->and_where('BranchOrder','=', $branchOrder)
		->and_where('Links.Deleted','=', false)
		->and_where('Links.Draft','=', false)
		->where_close()
		->order_by('Links.Orders', 'asc')
		->execute();
		
		return $list;
	}

	public static function Descriptions($language, $pages, $branchOrder)
	{
		$list = DB::select()->from('Description')
		->join('Links')->on('Links.LinksId','=','Description.LinksId')
		->join('Pages')->on('Links.PagesId','=','Pages.PagesId')
		->join('Language')->on('Language.LanguageId','=','Links.LanguageId')
		->where_open()
		->where('Code','=', $language)
		->and_where('Pages.Name','=',$pages)
		->and_where('BranchOrder','=', $branchOrder)
		->and_where('Description.Deleted','=', false)
		->and_where('Links.Draft','=', false)
		->where_close()
		->execute();
		return $list;
	}

	public static function Lesson($language, $lessonPage)
	{
		$list = DB::select()->from('Lesson')->join('Pages','LEFT')->on('Pages.PagesId','=','Lesson.PagesId')
		->join('Language','')->on('Language.LanguageId','=','Lesson.LanguageId')
		->where_open()
		->where('Code',$language)
		->and_where('Pages.Name', $lessonPage)
		->where_close()
		->execute();

		return $list;
	}

	public static function Quiz($language, $quiz)
	{
		$list = DB::select()->from('Quiz')->join('Pages')->on('Pages.PagesId','=','Quiz.PagesId')->join('Language')->on('Language.LanguageId','=','Quiz.LanguageId')
		->where_open()
		->where('Code','=', $language)
		->and_where('Pages.Name','=', $quiz)
		->where_close()
		->execute();
		return $list;
	}

	public static function Questions($quizId)
	{
		$list = DB::select()->from('QuizQuestion')->where('quizId','=', $quizId)->order_by('Problem','asc')->execute();

		return $list;
	}

	public static function Options($quizId, $problem)
	{
		$list = DB::select()->from('QuizOptions')
		->where_open()
		->where('QuizId','=', $quizId)
		->and_where('Problem','=', $problem)
		->where_close()
		->order_by(DB::expr('RAND()'))
		->execute();

		return $list;
	}

	public static function CheckLink($language, $SchoolLevelPages)
	{
		$list = DB::select()->from('Links')->join('Language')->on('Language.LanguageId','=','Links.LanguageId')
		->join('Pages')->on('Pages.PagesId','=','Links.PagesId')
		->where_open()
		->where('Code','=',$language)
		->and_where('Pages.Name','=',$SchoolLevelPages)
		->and_where('Links.Draft','=', 0)
		->and_where('Links.Deleted','=', 0)
		->where_close()
		->execute();
		$num_rows = count($list);

		return $num_rows;
	}	

	public static function CurrencyPriceList($currencyName)
	{
		$list = DB::select()->from('PriceList')
		->join('Currency')->on('Currency.CurrencyId','=','PriceList.CurrencyId')
		->where('CurrencyName','=', $currencyName)
		->execute();   
        return $list;
	}

	public static function Country()
	{
		$list = DB::select()->from('Country')->execute();
		return $list;
	}

	public static function RegisterPayer($email, $name, $customerNumber, $ssn, $address, $zip, $city, $country, $active, $paymentMethod)
	{
		list($insert_id, $rows_affected) = DB::insert('Payer')->set(array(
    	'Name' => $name,
    	'Email' => $email,
    	'SSN' => $ssn,
    	'Address' => $address,
    	'City' => $city,
    	'Zip' => $zip,
    	'CountryId' => $country,
    	'Created' => \Date::forge()->format("%Y-%m-%d %H:%M:%S"),
    	'Updated' => \Date::forge()->format("%Y-%m-%d %H:%M:%S"),
    	'Active' => $active,
		))->execute();

		return $insert_id;
	}

	public static function saveSchool($name, $customerNumber, $address, $zip, $city, $country, $reference, $teacherCount, $studentCount, $payerId, $contactId, $subscriptionsType)
	{
		list($schoolId, $rows_affected) = DB::insert('Schools')->set(array(
    	'Name' => $name,
    	'SSN' => $customerNumber,
    	'Address' => $address,
    	'Zip' => $address,
    	'City' => $city,
    	'CountryId' => $country,
    	'Reference' => $reference,
    	'CustomerNr' => $customerNumber,
    	'PayerId' => $payerId,
    	'StudentsCounts' => $studentCount,
    	'TeachersCounts' => $teacherCount,
    	'SubscriptionType' => $subscriptionsType,
    	'Created' => \Date::forge()->format("%Y-%m-%d %H:%M:%S"),
    	'Updated' => \Date::forge()->format("%Y-%m-%d %H:%M:%S")
		))->execute();


		list($insert_id, $rows_affected) = DB::insert('UsersSchool')->set(array(
    	'UsersId' => $contactId,
    	'SchoolId' => $schoolId,
		))->execute();

		return $schoolId;
	}

	public static function CheckAnswer($optionId)
	{
		$entry = DB::select()->from('QuizOptions')->where('QuizOptionsId','=', $optionId)->execute();

		return $entry;
	}

	public static function saveUsersSchool($userId, $schoolId)
	{
		list($insert_id, $rows_affected) = DB::insert('UsersSchool')->set(array(
    	'UsersId' => $userId,
    	'SchoolId' => $schoolId,
		))->execute();
	}

	public static function SaveGrades($quizId, $problem, $grade, $attempt, $userId)
	{
		DB::insert('UserGrade')->set(array(
    	'QuizId' => $quizId,
    	'Problem' => $problem,
    	'Attempt' => $attempt,
    	'Grade' => $grade,
    	'Created' => \Date::forge()->format("%Y-%m-%d %H:%M:%S"),
    	'UserId' => $userId
		))->execute();
	}

	public static function SaveQuizAttemp($userId, $quizId, $attempt)
	{
		DB::insert('UsersQuizAttempt')->set(array(
    	'QuizId' => $quizId,
    	'UserId' => $userId,
    	'Attempt' => $attempt,
    	'Created' => \Date::forge()->format("%Y-%m-%d %H:%M:%S")
		))->execute();
	}

	public static function CheckAttempt($quizId, $problem, $userId)
	{
		$list = DB::select()->from('UserGrade')
		->where_open()
		->where('QuizId','=', $quizId)
		->and_where('Problem','=',$problem)
		->and_where('UserId','=', $userId)
		->where_close()
		->order_by('Created','DESC')
		->execute();

		return $list;
	}

	public static function CheckGrade($quizId, $userId, $attempt)
	{
		$result = DB::select()->from('UserGrade')
		->where_open()
		->where('QuizId','=', $quizId)
		->and_where('UserId','=', $userId)
		->and_where('Attempt','=', $attempt)
		->where_close()
		->order_by('Problem', 'DESC')
		->execute();

		return $result;

	}

	public static function GetQuizAttemps($userId)
	{
		$result = DB::select()->from('UsersQuizAttempt')->execute();
		return $result;
	}

	public static function CheckQuizAttempt($quizId, $userId)
	{
		$result = DB::select()->from('UsersQuizAttempt')
		->where_open()
		->where('QuizId','=', $quizId)
		->and_where('UserId','=', $userId)
		->where_close()
		->order_by('Created', 'DESC')
		->execute();

		return $result;
	}

	public static function confirmUser($userId, $active)
	{
		$result = DB::update('users')
			    ->set(array('active'  => "0"))
			    ->where('id', '=', $userId)
			    ->execute();
	}

	public static function UsersQuiz($userId)
	{
		$list = DB::select('Attempt', 'UsersQuizAttempt.QuizId', 'Links.Title',array('SubChapter.Title', 'SubChapter'))->from('UsersQuizAttempt')
		->join('Quiz','LEFT')->on('Quiz.QuizId', '=', 'UsersQuizAttempt.QuizId') 
		->join('Links','LEFT')->on('Links.LanguageId', '=', 'Quiz.LanguageId')->on('Links.PagesId','=','Quiz.PagesId')  
		->join(array('Links', 'SubChapter'),'LEFT')->on('SubChapter.LinksId', '=', 'Links.Parent')  
		->where('UsersQuizAttempt.UserId','=',$userId)
		->group_by('UsersQuizAttempt.QuizId')->execute();
		return $list;
	}

	public static function UsersQuizAttempt($quizId)
	{
		$list = DB::select()->from('UsersQuizAttempt')->where('quizId', '=',$quizId)->order_by('Created','DESC')->execute();
		return $list;
	}

	public static function Grades($attempt, $quizId)
	{
		$list = DB::select()->from('UserGrade')->where_open()
		->where('Attempt','=', $attempt)
		->and_where('Grade','=',1)
		->and_where('QuizId','=',$quizId)
		->where_close()
		->execute();
		return $list;
	}

	public static function School($userId)
	{
		$list = DB::select('Schools.SchoolId', 'Name')->from('Schools')
		->join('UsersSchool','LEFT')
		->on('Schools.SchoolId','=','UsersSchool.SchoolId')->where('UsersId','=', $userId)->execute();

		return $list;
	}
}

?>