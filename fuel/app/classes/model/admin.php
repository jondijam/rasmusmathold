<?php
namespace Model;
use \DB;
class Admin extends \Model 
{
    public static function Pages($branchOrder)
    {
        $result = DB::select()->from('Pages')->where('BranchOrder', $branchOrder)->order_by('Name','asc')->execute();
        return $result;
    }

    public static function SavePages($pagesName, $branchOrder, $time)
    {
        list($insert_id, $rows_affected) = DB::insert('Pages')->set(array(
            'Name' => $pagesName,
            'Created' => $time,
            'Updated' => $time,
            'Deleted' => false,
            'BranchOrder' => $branchOrder
            ))->execute();

        return $insert_id;
    }

    public static function UpdatePages($pagesId, $pagesName, $time)
    {
        $result = DB::update('Pages')->set(array(
        'Name'  => $pagesName,
        'Updated' => $time
        ))
        ->where('PagesId', '=', $pagesId)
        ->execute();
    }

    public static function DeletePages($pagesId)
    {
        DB::update('Pages')
        ->value("Deleted", true)
        ->where('PagesId', '=', $pagesId)
        ->execute();

        $result = DB::delete('Pages')->where('PagesId', '=', $pagesId)->execute();
    }

    public static function Links($branchOrder)
    {

        if($branchOrder == 1)
        {
             $result = DB::select('Links.Title', array('Language.Name', 'Language'), 'Pages.Name', 'Links.Orders', 'Code', 'Links.LinksId' )->from('Links')
            ->join('Language','')
            ->on('Links.LanguageId', '=', 'Language.LanguageId')
            ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId')
            ->where_open()
            ->where('BranchOrder', $branchOrder)
            ->and_where('Links.Deleted', false)
            ->where_close()
            ->execute();
            return $result;
        }
        elseif($branchOrder == 2)
        {

            $result = DB::select(array('SchoolLevel.Title', 'SchoolLevel'), 'Links.Title', array('Language.Name','Language'), 'Pages.Name', 'Links.Orders', 'BranchOrder', 'Code','Links.LinksId')->from('Links')
            
            ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId')          
            ->join(array('Links', 'SchoolLevel'),'LEFT')->on('SchoolLevel.LinksId', '=', 'Links.Parent')
            ->join('Language','')
            ->on('SchoolLevel.LanguageId', '=', 'Language.LanguageId')
            ->where_open()
            ->where('BranchOrder', $branchOrder)
            ->and_where('Links.Deleted',false)
            ->where_close()
            ->execute();
            return $result;
        }
        elseif($branchOrder == 3)
        {

            $result = DB::select(array('SchoolLevel.Title', 'SchoolLevel'),array('Chapter.Title', 'Chapter'), 'Links.Title', array('Language.Name','Language'), 'Pages.Name', 'Links.Orders', 'BranchOrder', 'Code','Links.LinksId')->from('Links')
            
            ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId') 
            ->join(array('Links', 'Chapter'),'LEFT')->on('Chapter.LinksId', '=', 'Links.Parent')         
            ->join(array('Links', 'SchoolLevel'),'LEFT')->on('SchoolLevel.LinksId', '=', 'Chapter.Parent')
            ->join('Language','')
            ->on('SchoolLevel.LanguageId', '=', 'Language.LanguageId')
            ->where_open()
            ->where('BranchOrder', $branchOrder)
            ->and_where('Links.Deleted',false)
            ->where_close()
            ->execute();
            return $result;
        }
        elseif($branchOrder == 4)
        {

            $result = DB::select(array('SchoolLevel.Title', 'SchoolLevel'),array('Chapter.Title', 'Chapter'),array('SubChapter.Title', 'SubChapter'), 'Links.Title', array('Language.Name','Language'), 'Pages.Name', 'Links.Orders', 'BranchOrder', 'Code','Links.LinksId')->from('Links')            
            ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId') 
            ->join(array('Links', 'SubChapter'),'LEFT')->on('SubChapter.LinksId', '=', 'Links.Parent')  
            ->join(array('Links', 'Chapter'),'LEFT')->on('Chapter.LinksId', '=', 'SubChapter.Parent')         
            ->join(array('Links', 'SchoolLevel'),'LEFT')->on('SchoolLevel.LinksId', '=', 'Chapter.Parent')
            ->join('Language','')
            ->on('SchoolLevel.LanguageId', '=', 'Language.LanguageId')
            ->where_open()
            ->where('BranchOrder', $branchOrder)
            ->and_where('Links.Deleted',false)
            ->where_close()
            ->execute();
            return $result;
        } 
        elseif($branchOrder == 5)
        {

            $result = DB::select(array('SchoolLevel.Title', 'SchoolLevel'),array('Chapter.Title', 'Chapter'),array('SubChapter.Title', 'SubChapter'), 'Links.Title', array('Language.Name','Language'), 'Pages.Name', 'Links.Orders', 'BranchOrder', 'Code','Links.LinksId')->from('Links')            
            ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId') 
            ->join(array('Links', 'SubChapter'),'LEFT')->on('SubChapter.LinksId', '=', 'Links.Parent')  
            ->join(array('Links', 'Chapter'),'LEFT')->on('Chapter.LinksId', '=', 'SubChapter.Parent')         
            ->join(array('Links', 'SchoolLevel'),'LEFT')->on('SchoolLevel.LinksId', '=', 'Chapter.Parent')
            ->join('Language','')
            ->on('SchoolLevel.LanguageId', '=', 'Language.LanguageId')
            ->where_open()
            ->where('BranchOrder', $branchOrder)
            ->and_where('Links.Deleted', false)
            ->where_close()
            ->execute();
            return $result;
        }       
    }

    public static function SaveLink($title, $LanguageId, $pagesId, $parent, $order, $userId, $time)
    {
        if($LanguageId == 0)
        {
            $links = DB::select()->from('Links')->where('LinksId','=', $parent)->execute();
            foreach($links as $link)
            {
                $LanguageId = $link['LanguageId'];
                list($insert_id, $rows_affected) = DB::insert('Links')->set(array(
                'Title' => $title,
                'LanguageId' => $LanguageId,
                'UserId' => $userId,
                'Created' => $time,
                'Updated' => $time,
                'Parent' => $parent,
                'Orders' => $order,
                'PagesId'=> $pagesId
               
                ))->execute();

            }
        }
        else
        {
            list($insert_id, $rows_affected) = DB::insert('Links')->set(array(
            'Title' => $title,
            'LanguageId' => $LanguageId,
            'UserId' => $userId,
            'Created' => $time,
            'Updated' => $time,
            'Parent' => $parent,
            'Orders' => $order,
            'PagesId'=> $pagesId
           
            ))->execute();
        }        
    }

    public static function UpdateLink($linksId, $column, $title, $time)
    {
        
        if($column == 'Parent')
        {
            $result = DB::select()->from('Links')->where('LinksId','=',$title)->execute()->as_array();

            foreach ($result as $item) 
            {
                $result = DB::update('Links')->set(array(
                $column => $title,
                'LanguageId' => $item['LanguageId'],
                'Updated' => $time
                ))
                ->where('LinksId', '=', $linksId)
                ->execute();   
                
            }
        }
        else
        {
            $result = DB::update('Links')->set(array(
            $column => $title,
            'Updated' => $time
            ))
            ->where('LinksId', '=', $linksId)
            ->execute();   
        }       
    }

    public static function DeleteLinks($linksId)
    {
        DB::update('Links')->value('Deleted', true)->where('LinksId','=', $linksId)->execute();
    }

    public static function LessonLinks($pagesId, $LanguageId)
    {
       $result = DB::select( array('SchoolLevel.Title', 'SchoolLevel'),array('Chapter.Title', 'Chapter'),array('SubChapter.Title', 'SubChapter'), 'Links.Title', array('Language.Name','Language'), 'Pages.Name', 'Links.Orders', 'BranchOrder', 'Code','Links.LinksId')
        ->from('Links')
        ->join('Pages','LEFT')->on('Links.PagesId', '=', 'Pages.PagesId') 
        ->join(array('Links', 'SubChapter'),'LEFT')->on('SubChapter.LinksId', '=', 'Links.Parent')  
        ->join(array('Links', 'Chapter'),'LEFT')->on('Chapter.LinksId', '=', 'SubChapter.Parent')         
        ->join(array('Links', 'SchoolLevel'),'LEFT')->on('SchoolLevel.LinksId', '=', 'Chapter.Parent')
        ->join('Language','')->on('SchoolLevel.LanguageId', '=', 'Language.LanguageId')
        ->where_open()
        ->where('Links.PagesId', $pagesId)
        ->and_where('SchoolLevel.LanguageId', $LanguageId)
        ->where_close()
        ->execute();

        return $result;
    }

    public static function Lessons($branchOrder)
    {
        $result =  DB::select('Lesson.Draft','Lesson.LanguageId','Lesson.PagesId','LessonId','Text', array('Language.Name','Language'), 'Pages.Name', 'BranchOrder', 'Code', 'OldLesson','IFrame')
        ->from('Lesson')
        ->join('Pages','LEFT')->on('Lesson.PagesId', '=', 'Pages.PagesId')
        ->join('Language','')->on('Lesson.LanguageId', '=', 'Language.LanguageId')
        ->where_open()
        ->where('BranchOrder', $branchOrder)
        ->and_where('Lesson.Deleted', false)
        ->where_close()
        ->execute();
        
        return $result;
    }

    public static function UpdateLesson($lessonId, $pagesId, $LanguageId, $oldlesson, $iframe, $draft, $lessonText, $time)
    {
        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('PagesId', '=', $pagesId)
             ->and_where('LanguageId','=', $LanguageId)
             ->where_close()
             ->execute(); 
        echo DB::last_query();

        $result = DB::update('Lesson')
        ->set(array(
            'Text'  => $lessonText,
            'PagesId' => $pagesId,
            'LanguageId' => $LanguageId,
            'OldLesson' => $oldlesson,
            'IFrame' => $iframe,
            'Updated' => $time,
            'Draft' => $draft
        ))
        ->where('LessonId', '=', $lessonId)
        ->execute();
    }

    public static function SaveLesson($pagesId, $LanguageId, $oldlesson, $iframe, $draft, $lessonText, $time)
    {
        $oldlessonBool = false;
        if ($oldlesson == 1) 
        {
            # code...
            $oldlessonBool = true;
        }


        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('PagesId', '=', $pagesId)
             ->and_where('LanguageId','=', $LanguageId)
             ->where_close()
             ->execute(); 
        

        DB::insert('Lesson')->set(
            array(

                'Text' => $lessonText,
                'PagesId' => $pagesId,
                'LanguageId' => $LanguageId,
                'OldLesson' => $oldlessonBool,
                'IFrame' => $iframe,
                'Created' => $time,
                'Updated' => $time,
                'Draft' => $draft
                ))->execute();

    }
    public static function Quiz($branchOrder, $show_per_page, $currentPage)
    {

       $result = DB::select()->from('Quiz')
        ->join('Pages','LEFT')->on('Quiz.PagesId', '=', 'Pages.PagesId')
        ->where_open()
        ->where('BranchOrder', $branchOrder)
        ->and_where('Quiz.Deleted', false)
        ->where_close()
        ->limit($show_per_page)->offset($currentPage)
        ->execute();
        return $result;
    }

    public static function CountQuiz($branchOrder)
    {

       $result = DB::select()->from('Quiz')
        ->join('Pages','LEFT')->on('Quiz.PagesId', '=', 'Pages.PagesId')
        ->where_open()
        ->where('BranchOrder', $branchOrder)
        ->and_where('Quiz.Deleted', false)
        ->where_close()
        ->execute();
        return $result;
    }
    public static function Questions($quizId)
    {
        $result = DB::select()->from('QuizQuestion')
        ->where_open()
        ->where('QuizId','=',$quizId)
        ->where_close()
        ->order_by('Problem','asc')
        ->execute();


        return $result;
    }

    public static function Options($quizId, $questionProblem)
    {

        $result = DB::select()->from('QuizOptions')
        ->where_open()
        ->where('QuizId','=',$quizId)
        ->and_where('Problem','=', $questionProblem)
        ->where_close()
        ->order_by('Problem','asc')
        ->execute();
        return $result;

    }

    public static function UpdateOption($problem, $pageId, $LanguageId, $correctAnswer, $optionText, $time, $userId, $optionId, $quizId)
    {
        
        $result = DB::update('QuizOptions')
        ->set(array(
            'Text' => $optionText,
            'correctAnswer' => $correctAnswer,
            'Problem' => $problem,
            'Updated' => $time,
            'PagesId' => $pageId,
            'LanguageId' => $LanguageId,
        ))
        ->where('QuizOptionsId', '=', $optionId)
        ->execute();
    }

    public static function SaveQuiz($pageId, $LanguageId, $time, $draft,$userId)
    {
        $publish = true;
        if($draft == 1)
        {
            $publish = false;
        }

        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('PagesId', '=', $pageId)
             ->and_where('LanguageId','=', $LanguageId)
             ->where_close()
             ->execute(); 

        list($insert_id, $rows_affected) = DB::insert('Quiz')->set(array(
            'PagesId'=>$pageId,
            'LanguageId'=>$LanguageId,
            'Created'=>$time,
            'Updated'=>$time,
            'UserId'=>$userId,
            'Draft' => $draft
            ))->execute();

        return $insert_id;
    }

    public static function UpdateQuiz($pageId, $LanguageId, $time, $draft, $userId, $quizId)
    {
        
        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('PagesId', '=', $pageId)
             ->and_where('LanguageId','=', $LanguageId)
             ->where_close()
             ->execute(); 

        $result = DB::update('Quiz')
        ->set(array(
            'PagesId' => $pageId,
            'LanguageId' => $LanguageId,
            'Updated' => $time,
            'Draft' => $draft
        ))
        ->where('QuizId', '=', $quizId)
        ->execute();
    }
    
    public static function UpdateQuestion($problem, $pageId, $LanguageId, $question, $time, $userId, $questionId, $quizId)
    {
        $result = DB::update('QuizQuestion')
        ->set(array(
            'Text' => $question,
            'Problem' => $problem,
            'PagesId' => $pageId,
            'LanguageId' => $LanguageId,
            'Updated' => $time
        ))
        ->where('QuizQuestionId', '=', $questionId)
        ->execute();
    }

    public static function SaveQuestion($problem, $pageId, $LanguageId, $question, $time, $userId, $quizId)
    {
        DB::insert('QuizQuestion')->set(
            array(
                'Text' => $question,
                'Problem' => $problem,
                'PagesId' => $pageId,
                'LanguageId' => $LanguageId,
                'QuizId' => $quizId,
                'Created' => $time,
                'Updated' => $time,
                'UserId' => $userId
                ))->execute();
    }

    public static function SaveOption($problem, $pageId, $LanguageId, $correctAnswer, $optionText, $time, $userId, $quizId)
    {
        $correct = false;

        if ($correctAnswer == 1) 
        {
            $correct = true;
        }

        DB::insert('QuizOptions')->set(
            array(
                'Text' => $optionText,
                'correctAnswer' => $correct,
                'Problem' => $problem,
                'Created' => $time,
                'Updated' => $time,
                'PagesId' => $pageId,
                'LanguageId' => $LanguageId,
                'QuizId' => $quizId
                ))->execute();
    }

    public static function DeleteQuiz($quizId)
    {
        DB::delete('Quiz')->where('QuizId', '=', $quizId)->execute();
        DB::delete('QuizQuestion')->where('QuizId', '=', $quizId)->execute();
        DB::delete('QuizOptions')->where('QuizId', '=', $quizId)->execute();
        DB::delete('UserGrade')->where('QuizId', '=', $quizId)->execute();
        DB::delete('UsersQuizAttempt')->where('QuizId', '=', $quizId)->execute();
    }

    public static function Descriptions($branchOrder)
    {
       $result = DB::select('Links.Draft' ,array('Language.Name', 'LanguageName'),'Title','Description','DescriptionId', 'Links.LinksId')
        ->from('Description')
        ->Join('Links','LEFT')
        ->on('Links.LinksId', '=','Description.LinksId')
        ->join('Pages','LEFT')
        ->on('Pages.PagesId', '=', 'Links.PagesId')
        ->join('Language','LEFT')
        ->on('Language.LanguageId','=','Links.LanguageId')
        ->where_open(
)        ->where('BranchOrder', '=', $branchOrder)
        ->and_where('Description.Deleted','=',false)
        ->where_close()
        ->execute();

        return $result;
    }
    
    public static function SaveDescription($linksId, $description, $time, $userId, $draft)
    {

        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('LinksId', '=', $linksId)
             ->where_close()
             ->execute(); 


        DB::insert('Description')->set(
            array(
                'Description' => $description,
                'LinksId' => $linksId,
                'Created' => $time,
                'Updated' => $time,
                'UsersId' => $userId
                ))->execute();

    }

    public static function UpdateDescription($linksId, $descriptionId, $description, $time, $draft)
    {
        DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('LinksId', '=', $linksId)
             ->where_close()
             ->execute(); 

        $result = DB::update('Description')
        ->set(array(
            'Description'  => $description,
            'LinksId' => $linksId,
            'Updated' => $time
        ))
        ->where('DescriptionId', '=', $descriptionId)
        ->execute();
    }

    public static function DeleteDescription($descriptionId)
    {
        $result = DB::select()->from('Description')->where('DescriptionId','=', $descriptionId)->execute();

        $on_key = $result->as_array('DescriptionId');
        foreach($on_key as $id => $item)
        {
             DB::update('Links')->value("Draft", $draft)
             ->where_open()
             ->where('LinksId', '=', $item['LinksId'])
             ->where_close()
             ->execute(); 
        }

        DB::update('Description')->value('Deleted', true)->where('DescriptionId','=',$descriptionId)->execute();
    }

    public static function Languages()
    {
    	$result = DB::select()->from('Language')->order_by('Name','asc')->execute();
        return $result;
    }

    public static function Notifications($LanguageId)
    {
    	$result = DB::select()->from('Notification')->join('Language','')->on('Language.LanguageId', '=', 'Notification.LanguageId')->where('Deleted', 0)->execute();

    	return $result;
    }

    public static function SaveNotification($title, $LanguageId, $draft, $messages, $time, $userId)
    {
        $publish = true;
        if($draft == 1)
        {
            $publish = false;
        }

        list($insert_id, $rows_affected) = DB::insert('Notification')->set(array(
            'Title' => $title,
            'Text' => $messages,
            'LanguageId' => $LanguageId,
            'Publish' => $publish,
            'Created' => $time,
            'Updated' => $time,
            'Deleted' => false,
            'UserId' => $userId
            ))->execute();

        return $insert_id;
    }

    public static function DeleteNotification($notificationId)
    {
        DB::update('Notification')
        ->value("Deleted", true)
        ->where('NotificationId', '=', $notificationId)
        ->execute();
    }

    public static function UpdateNotification($notificationId, $title, $LanguageId, $draft, $message, $time)
    {
        $publish = true;
        if($draft == 1)
        {
            $publish = false;
        }

        $result = DB::update('Notification')->set(array(
        'Title'  => $title,
        'Text' => $message,
        'LanguageId' => $LanguageId,
        'Publish' => $publish,
        'Updated' => $time
    ))
    ->where('NotificationId', '=', $notificationId)
    ->execute();
    }

    public static function SaveArticle($title, $text, $LanguageId, $draft, $time, $userId)
    {
        $publish = true;
        if($draft == 1)
        {
            $publish = false;
        }

        list($insert_id, $rows_affected) = DB::insert('Article')->set(array(
            'Title' => $title,
            'Text' => $text,
            'LanguageId' => $LanguageId,
            'Publish' => $publish,
            'Created' => $time,
            'Updated' => $time,
            'Deleted' => false,
            'UserId' => $userId
            ))->execute();

        return $insert_id;
    }

    public static function UpdateArticle($articleId, $title, $text, $LanguageId, $draft, $time)
    {
        $publish = true;
        if($draft == 1)
        {
            $publish = false;
        }

        $result = DB::update('Article')->set(array(
        'Title'  => $title,
        'Text' => $text,
        'LanguageId' => $LanguageId,
        'Publish' => $publish,
        'Updated' => $time
         ))->where('ArticleId', '=', $articleId)->execute();

    }

    public static function Articles()
    {
        $result = DB::select()->from('Article')->join('Language','')->on('Language.LanguageId', '=', 'Article.LanguageId')->where('Deleted', 0)->execute();

        return $result;
    }

    public static function DeleteArticle($ArticleId)
    {
        DB::update('Article')
        ->value("Deleted", true)
        ->where('ArticleId', '=', $ArticleId)
        ->execute();
    }


    public static function Users($group)
    {
        $list = DB::select()->from('users')->where('group','=',$group)->execute();
        return $list;
    }

    public static function Currency()
    {
        $list = DB::select()->from('Currency')->execute();

        return $list;
    }

    public static function SaveCurrency($currencyCode, $currencyName, $time)
    {
        list($insert_id, $rows_affected) = DB::insert('Currency')->set(array(
            'CurrencyCode' => $currencyCode,
            'CurrencyName' => $currencyName,
            'Created' => $time,
            'Updated' => $time
            ))->execute();
    }


    public static function PriceList()
    {
       $list = DB::select()->from('PriceList')->join('Currency')->on('Currency.CurrencyId','=','PriceList.CurrencyId')->execute();   
        return $list;
    }   

    public static function savePriceList($priceperstudentpermonth, $priceperschoolpermonth, $priceperteacherpermonth, $priceperstudentperyear, $priceperschoolperyear, $priceperteacherperyear, $currencyId, $time)
    {
        list($insert_id, $rows_affected) = DB::insert('PriceList')->set(array(
            'CurrencyId' => $currencyId,
            'SchoolPriceMonth' => $priceperschoolpermonth,
            'StudentPriceMonth' => $priceperstudentpermonth,
            'TeacherPriceMonth' => $priceperteacherpermonth,
            'SchoolPriceYear' => $priceperschoolperyear,
            'StudentPriceYear' => $priceperstudentperyear, 
            'TeacherPriceYear' => $priceperteacherperyear,
            'Created' => $time,
            'Updated' => $time
            ))->execute();
    }
}
?>