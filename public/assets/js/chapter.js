$('#chapterPages').dataTable().makeEditable({
	sUpdateURL: function(value, settings)
	{
		//$(this).html();	
        var pagesId = $(this).parent().attr('data-id');
        var oldPagesName = $(this).attr('data-oldpagesname');
        var columnName = settings.name;
        var pagesName = value;
        var post = "";    
        var error = "";

        $.ajax({
          url:  savePagesUrl,
          type: "POST",
          data: {'pagesId':pagesId, 'pagesName':pagesName},
          async:false,
          success: function(data)
          {
            error = data.length;
            post = data;

          }
       });

        if(error > 2)
        {
            return post;
        }
        else
        {
           return(value); 
        }     
   	} ,
	"aoColumns": [
	null,
	{
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'pagesName',
        submit:'Save changes',
        width:'220px',
        id: 1
    },
    null
	],
    fnOnDeleting: function(tr, id)
    {   

        if(confirm('you are deleting'))
        {
            $("#"+id+"").hide();

            $.post(deletePagesUrl,{pagesId:id});
            return false;
        }
        else
        {
            return false;
        }
        
       
    }
});

$('#chapterLinks').dataTable().makeEditable({
  sUpdateURL: function(value, settings)
  {
        var linksId = $(this).parent().attr('data-id');
        var columnName = settings.name;
        var value = value;

        $.ajax({
          url:  saveLinksUrl,
          type: "POST",
          data: {'linksId':linksId, 'columnName':columnName, title:value},
          async:false,
          success: function(data)
          {
            error = data.length;
            post = data;

          }
       });

        if(error > 2)
        {
            return post;
        }
        else
        {
           return(value); 
        }
        //data: {'pagesId':pagesId, 'pagesName':pagesName},
  },
  "aoColumns": [
    null,
    {
        //language
        type: 'select',
        name: "Parent",
        onblur: 'cancel',
        submit: 'Ok',
        loadurl: 'http://rasmus.local/admin/schoolLevel/select',
        loadtype: 'POST',
        event: 'click'
    },
    { 
        //title
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'Title',
        submit:'Save changes',
        width:'90%'
    },
    {
        //Pages
        type: 'select',
        onblur: 'cancel',
        name: 'PagesId',
        submit: 'Ok',
        loadurl: '/admin/pages/select/2',
        loadtype: 'GET',
        event: 'click'
    },
    {
        //Order
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'Orders',
        submit:'Save changes',
        width:'90%'
    }

  ],
  fnOnDeleting: function(tr, id)
    {   

        if(confirm('you are deleting'))
        {
            
            values = id.split('s');
            linksId = values[1]


            $("#"+id+"").hide();

            $.post(deleteLinksUrl,{linksId:linksId}, function(data){

                console.log(data);
            });
            return false;
        }
        else
        {
            return false;
        }
        
       
    }
});


 CKEDITOR.replace( 'description',{

    filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

 });