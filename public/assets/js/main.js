var notificationId = 0;
var title = "";
var draft = 0;
var descriptionId = 0;
var description = "";
var languages = 0;
var languageId = 0;
var messages = "";
var articleId = "";
var parent = 0;
var order = 0;
var pages = 0;
var draftBool = false;
var linksId = 0;
var quizId = 0;
var correctBool = new Array();
var optionsArray = new Array();
var correctAnswer = new Array(); 
var question = new Array();
var optionText = new Array();
var problemsArray = new Array();
var questionId = new Array();
var optionId = new Array();
var problemsCount = 0;
var optionsCount = new Array();
var translate = 0;
var newQuizId = 0;
var nquizId = 0;

//dataTable
$('.saveLinks').click(function(e)
{
	e.preventDefault();
	parent = $('.parent').val();
	languages = $('.languages').val();
	order = $('.order').val();
	title = $('.title').val();
	pages = $('.pages').val();
	branchOrder = $(this).attr('data-branchOrder');
	
	if(typeof parent == 'undefined')
	{
		parent = 0;
	}
	
	$.post(saveLinksUrl,{pagesId: pages, languageId: languages, parent: parent, order:order, title:title }, function(data)
	{ 



		if (data.length === 2 && branchOrder == 1) 
		{
			window.location = schoolLevelUrl;
		}
		else if (data.length === 2 && branchOrder == 2) 
		{
			window.location =  chapterUrl;
		}
		else if (data.length === 2 && branchOrder == 3) 
		{
			window.location =  subChapterUrl;
		}
		else if (data.length === 2 && branchOrder == 4) 
		{
			window.location = lessonUrl;
		}
		else if (data.length === 2 && branchOrder == 5) 
		{
			window.location = quizUrl;
		}
		else
		{
		
			alert(data);
		}
		
	});
	
});

//savePages

$('.savePages').click(function(e){
	e.preventDefault();
	var branchOrder = $(this).attr('data-branchOrder');
	var pagesName = $('.pagesName').val();
	$.post(savePagesUrl, {pagesName:pagesName, branchOrder:branchOrder}, function(data){

		if (data.length === 2 && branchOrder == 1) 
		{
			window.location = schoolLevelUrl;
		}
		else if (data.length === 2 && branchOrder == 2) 
		{
			window.location =  chapterUrl;
		}
		else if (data.length === 2 && branchOrder == 3) 
		{
			window.location =  subChapterUrl;
		}
		else if (data.length === 2 && branchOrder == 4) 
		{
			window.location = lessonUrl;
		}
		else if (data.length === 2 && branchOrder == 5) 
		{
			window.location = quizUrl;
		}
		else
		{
			alert(data);
		}

	})
});


$('.lessonOpen').click(function()
 {
 	$(this).next('.lessonClosed').removeClass('hidden');
 	lessonId = $(this).attr('data-id');
 	$('#lessonGadgetblock'+lessonId+'').slideDown();
 	$(this).addClass('hidden');


 
 });

 $('.lessonClosed').click(function()
 {
 	descriptionId = $(this).attr('data-id'); 	
 	$('#lessonGadgetblock'+lessonId+'').slideUp();
 	$('.lessonOpen').removeClass('hidden');
 	$(this).addClass('hidden');
 });


$('.quizOpen').live('click', function()
 {
 	$(this).next('.quizClosed').removeClass('hidden');
 	quizId = $(this).attr('data-id');
 	$('#quizGadgetblock'+quizId+'').slideDown();
 	$(this).addClass('hidden'); 

 	loadEditors(quizId);


 });

 $('.quizClosed').live('click', function()
 {
 	quizId = $(this).attr('data-id'); 	
 	$('#quizGadgetblock'+quizId+'').slideUp();
 	$('.quizOpen').removeClass('hidden');
 	$(this).addClass('hidden');

 	destroyEditors();
 });



 $('.descriptionOpen').click(function()
 {
 	$(this).next('.descriptionClosed').show();
 	descriptionId = $(this).attr('data-id');
 	$('#descriptionGadgetblock'+descriptionId+'').slideDown();
 	$(this).hide();


 
 });

 $('.descriptionClosed').click(function()
 {
 	descriptionId = $(this).attr('data-id'); 	
 	$('#descriptionGadgetblock'+descriptionId+'').slideUp();
 	$('.descriptionOpen').show();
 	$(this).hide();
 });




 $('.articleOpen').click(function()
 {
 	$('.articleClosed').show();
 	articleId = $(this).attr('data-id');
 	$('#articleGadgetblock'+articleId+'').slideDown();

 	$(this).hide();


 
 });

 $('.articleClosed').click(function()
 {
 	articleId = $(this).attr('data-id'); 	
 	$('#articleGadgetblock'+articleId+'').slideUp();
 	$(this).hide();
 	$('.articleOpen').show();
 });


 $('.open').click(function()
 {
 	$('.closed').show();
 	notificationId = $(this).attr('data-id');
 	$('#gadgetblock'+notificationId+'').slideDown();
 	$(this).hide();
 });

 $('.closed').click(function()
 {
 	notificationId = $(this).attr('data-id'); 	
 	$('#gadgetblock'+notificationId+'').slideUp();
 	$(this).hide();
 	$('.open').show();
 });

 $('.deleteArticle').click(function(){
 	articleId = $(this).attr('data-id');
 	$.post(deleteArticleUrl,{articleId:articleId}, function(){
 		$('#articleGadget'+articleId+'').hide();
 	});

 });

 $('.saveArticle').click(function(e){
 	e.preventDefault();
 	articleId = $(this).attr('data-itemid');
 	title = $('#articleTitle'+articleId+'').val();
 	languages = $('#articleLanguages'+articleId).val();
 	draftBool = $('#articleDraft'+articleId).is(':checked');

 	if (draftBool) 
 	{
 		draft = 1;
 	};
	
 	messages = CKEDITOR.instances['article'+articleId+''].getData(); 	

 	$.post(updateArticleUrl, {articleId: articleId, articleTitle:title, languages:languages, articleDraft:draft, article:messages},function(){});

 });

 $('.deleteNotification').click(function(){
 	notificationId = $(this).attr('data-id');
 	$.post(deleteNotificationUrl,{notificationId:notificationId}, function(){
 		$('#gadget'+notificationId+'').hide();
 	});

 });

 $('.saveNotification').click(function(e){
 	e.preventDefault();
 	notificationId = $(this).attr('data-itemId');
 	title = $('#notificationTitle'+notificationId+'').val();
 	languages = $('#languages'+notificationId).val();
 	
 	draftBool = $('#draft'+notificationId).is(':checked');

 	if (draftBool) 
 	{
 		draft = 1;
 	};
 	
 	messages = CKEDITOR.instances['notification'+notificationId+''].getData();
 	$.post(updateNotificationUrl, {notificationId: notificationId, title:title, languages:languages, draft:draft, notification:messages},function(){});

 });

$('.saveDescription').click(function(e)
{
	e.preventDefault();
	linksId = $('.linksId').val();
	description = CKEDITOR.instances['description'].getData();
	var branchOrder = $(this).attr('data-branchOrder');

	draftBool = $('.draft').is(':checked');

	if(draftBool) 
 	{
 		draft = 1;
 	}


	$.post(savePagesDescription, {linksId:linksId, description:description, draft:draft}, function(data){
		if (data.length == 2 && branchOrder == 1) 
		{
			window.location = schoolLevelUrl;
		}
		else if (data.length == 2 && branchOrder == 2) 
		{
			window.location = chapterUrl;
		}
		else if (data.length == 2 && branchOrder == 3) 
		{
			window.location = subChapterUrl;
		}
		else
		{
			alert(data);
		}
	});
});


$('.deleteDescription').click(function(e){
	e.preventDefault();


	descriptionId = $(this).attr('data-id');

	$.post(deleteDescriptionUrl, {descriptionId:descriptionId}, function(data){
		
	});

	$('#descriptionGadget'+descriptionId+'').hide();
});


$('.updateDescription').click(function(e){
	e.preventDefault();

	descriptionId = $(this).attr('data-id');
	description = CKEDITOR.instances['description'+descriptionId+''].getData();
	linksId = $('.linksId'+descriptionId+'').val();

	draftBool = $('.draft'+descriptionId).is(':checked');

	if(draftBool) 
 	{
 		draft = 1;
 	}

 	$.post(savePagesDescription, {linksId:linksId, description:description, draft:draft, descriptionId:descriptionId}, function(data){
 		if (data.length > 2) 
 		{
 			alert(data);
 		}
 	});
});

var editorID = "";
var dst = "";
var CKeditors = {};
var html = "";

function loadEditors(quizId) 
{
	$('.loader').removeClass('hidden');
	console.log(quizId);
    var editors = $("textarea.editor"+quizId+"");

    if (editors.length)
    {
    	console.log(editors.length);
        editors.each(function() 
        {
        	counter++;
            editorID = $(this).attr("id");
           	
           	if(CKeditors[editorID]){
                CKeditors[editorID].destroy();
                CKeditors[editorID] = null;
            }

           	dst = editorID;

            if( $(this).val() )
            {
                html = $(this).val();
            }

           	CKeditors[editorID] = CKEDITOR.replace(dst, {});
           
           
            //CKEDITOR.appendTo(dst, null, html);
           

        });
        $("textarea.ckeditor").hide();

        CKEDITOR.on('instanceReady', function(e) 
        {
        	$('.loader').addClass('hidden');
			
		});
    }
}

function destroyEditors()
{
	for(x in CKeditors)
	{

    	if (CKEDITOR.instances[x])
    	{
    		CKEDITOR.instances[x].destroy(); 
    	}
    }
}