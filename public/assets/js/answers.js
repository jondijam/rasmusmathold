var quizId = 0;
var problems = 0;
var options = new Array();
var optionValue = new Array();
var answerCounter = new Array();
var value = new Array();
var userCount = 0;
var jsonvalueArray = new Array();
var jsonoptionValue = new Array();

$('.sendAnswers').click(function(e){
	e.preventDefault();
	quizId = $(this).attr('data-quizId');
	userCount++;
	problems = $('.problems').size();

	$.post(saveQuizAttempt, {quizId:quizId, userAttempt:userCount, userId:userId});
	for (var i = 1; i <= problems; i++)
	{
		options[i] = $('.option'+i+'').size();
		answerCounter[i] = $('.option'+i).attr('data-answercounter');

		if(answerCounter[i] != 1)
		{
			optionValue[i] = new Array();
			value[i] = new Array();

			for (var op = 1; op <= options[i]; op++)
			{
				value[i][op] = $('.correct'+i+''+op+'').val();

				if($('.correct'+i+''+op+'').is(':checked'))
				{
					optionValue[i][op] = 1;
				}
				else
				{
					optionValue[i][op] = 0;
				}
			}

			jsonvalueArray[i] = JSON.stringify(value[i]);
			jsonoptionValue[i] = JSON.stringify(optionValue[i]);
		}
		else
		{
			jsonvalueArray[i] = 0;
			jsonvalueArray[i] = $('.correct'+i+':checked').val();
			jsonoptionValue[i] = -1;
		}

		$.post(saveAnswer, {optionId: jsonvalueArray[i], optionValue:jsonoptionValue[i], quizId:quizId, userId:userId, userCount:userCount, problem:i},function(data){
			console.log('works');
		});	
	}
});