$('#lessonPages').dataTable().makeEditable({
	sUpdateURL: function(value, settings)
	{
		//$(this).html();	
        var pagesId = $(this).parent().attr('data-id');
        var oldPagesName = $(this).attr('data-oldpagesname');
        var columnName = settings.name;
        var pagesName = value;
        var post = "";    
        var error = "";

        $.ajax({
          url:  savePagesUrl,
          type: "POST",
          data: {'pagesId':pagesId, 'pagesName':pagesName},
          async:false,
          success: function(data)
          {
            error = data.length;
            post = data;

          }
       });

        if(error > 2)
        {
            return post;
        }
        else
        {
           return(value); 
        }     
   	} ,
	"aoColumns": [
	null,
	{
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'pagesName',
        submit:'Save changes',
        width:'220px',
        id: 1
    },
    null
	],
    fnOnDeleting: function(tr, id)
    {   

        if(confirm('you are deleting'))
        {
            $("#"+id+"").hide();

            $.post(deletePagesUrl,{pagesId:id});
            return false;
        }
        else
        {
            return false;
        }
        
       
    }
});


$('#lessonLinks').dataTable().makeEditable({
  sUpdateURL: function(value, settings)
  {
        var linksId = $(this).parent().attr('data-id');
        var columnName = settings.name;
        var value = value;

        $.ajax({
          url:  saveLinksUrl,
          type: "POST",
          data: {'linksId':linksId, 'columnName':columnName, title:value},
          async:false,
          success: function(data)
          {
            error = data.length;
            post = data;

          }
       });

        if(error > 2)
        {
            return post;
        }
        else
        {
           return(value); 
        }
        //data: {'pagesId':pagesId, 'pagesName':pagesName},
  },
  "aoColumns": [
    null,
    {
        //language
        type: 'select',
        name: "Parent",
        onblur: 'cancel',
        submit: 'Ok',
        loadurl: '/admin/subchapter/select',
        loadtype: 'POST',
        event: 'click'
    },
    { 
        //title
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'Title',
        submit:'Save changes',
        width:'90%'
    },
    {
        //Pages
        type: 'select',
        onblur: 'cancel',
        name: 'PagesId',
        submit: 'Ok',
        loadurl: '/admin/pages/select/4',
        loadtype: 'GET',
        event: 'click'
    },
    {
        //Order
        indicator: 'Saving platforms...',
        tooltip: 'Click to edit platforms',
        type: 'text',
        name: 'Orders',
        submit:'Save changes',
        width:'90%'
    }

  ],
  fnOnDeleting: function(tr, id)
    {   

        if(confirm('you are deleting'))
        {
            
            values = id.split('s');
            linksId = values[1]


            $("#"+id+"").hide();

            $.post(deleteLinksUrl,{linksId:linksId}, function(data){

                console.log(data);
            });
            return false;
        }
        else
        {
            return false;
        }
        
       
    }
});

 CKEDITOR.replace( 'lesson',{

    filebrowserBrowseUrl : '/assets/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : '/assets/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : '/assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

 });

var iframeUrl = ""; 
var oldlessonCheck = false;
var oldLesson = 0;
var lesson = "";
var lessonId = 0;

$('.oldlessonCheck').click(function(e){

    oldlessonCheck =  $('.oldlessoncheckbox').is(':checked');

    if (oldlessonCheck) 
    {
  
       $('.oldlesson').removeClass('hidden');
    }
    else
    {
        $('.oldlesson').addClass('hidden');
    }

 });

  $('.iframeCheck').click(function(e){

    lessonId = $(this).attr('data-id');
    oldlessonCheck =  $('.iframeCheck'+lessonId+'').is(':checked');

    if (oldlessonCheck) 
    {
  
       $('.oldlesson'+lessonId +'').removeClass('hidden');
    }
    else
    {
        $('.oldlesson'+lessonId+'').addClass('hidden');
    }

 });

$('.savelesson').click(function(e)
{
    e.preventDefault();
    oldlessonCheck =  $('.oldlessoncheckbox').is(':checked');

    if (oldlessonCheck) 
    {
        oldLesson = 1;
    };

    draftBool = $('.draft').is(':checked'); 
    
    if (draftBool) 
    {
        draft = 1;
    };

    iframeUrl = $('.iframeUrl').val(); 
    pages = $('.pagesId').val();
    languages = $('.languages').val();
    lesson = CKEDITOR.instances['lesson'].getData();

   $.post(saveLessonUrl, {oldLesson: oldLesson, draft:draft, languageId: languages, iframeUrl: iframeUrl, pagesId: pages, lessonText: lesson}, function(data){
        window.location = lessonUrl;
    });
});

$('.updatelesson').click(function(e){
    e.preventDefault();
    lessonId = $(this).attr('data-id');

    oldlessonCheck =  $('.iframeCheck'+lessonId+'').is(':checked');
    draftBool = $('.draft'+lessonId+'').is(':checked'); 

    if (draftBool) 
    {
        draft = 1;
    }
    else
    {
        draft = 0;
    }

    if (oldlessonCheck) 
    {
        oldLesson = 1;
    };

    iframeUrl = $('.iframeUrl'+lessonId+'').val(); 
    pages = $('.pagesId'+lessonId+'').val();
    languages = $('.languages'+lessonId+'').val();
    lesson = CKEDITOR.instances['lesson'+lessonId+''].getData();

    $.post(saveLessonUrl, {oldLesson: oldLesson, draft:draft, languageId: languages, iframeUrl: iframeUrl, pagesId: pages, lessonText: lesson, lessonId: lessonId}, function(data){
 
    });
});