var problemsCount = 0;
var problems = 0;
var counter = 0;
var subcounter = 0; 
var questionId = new Array();
var optionId = new Array();
var show_per_page = 10;  
var number_of_items = $('.paginationContent').attr('data-quizCount');  
var number_of_pages = Math.ceil(number_of_items/show_per_page); 
var navigation_html = '<li><a class="previous_link" href="javascript:previous();">Prev</a></li>';  
var current_link = 0;
var optionCount = 0;

//jquery
$(document).ready(function()
{
  loadnewEditors();

  $('.paginationContent').attr('data-currentPage', '0');
  $('.paginationContent').attr('data-showPerPage', ''+show_per_page+'');

  while(number_of_pages > current_link)
  {
    navigation_html += '<li><a class="page_link" href="javascript:go_to_page(' + current_link +')" longdesc="' + current_link +'">'+ (current_link + 1) +'</a></li>';  
    current_link++; 
  }

  navigation_html += '<li><a class="next_link" href="javascript:next();">Next</a></li>';

  $('#page_navigation').html(navigation_html);  
  
  $.post('/admin/quiz/loading',{show_per_page:show_per_page, currenPage:current_link}, function(data){
    $('.paginationContent').html(data);
  });

  $('.paginationContent').children().slice(0, show_per_page).css('display', 'block');

  $('.savequiz').click(function(e)
  {
    e.preventDefault();
    $('.loader').removeClass('hidden');

    problems = $('.problemsCount').val();
    languageId = $('.languages').val();
    pages = $('.pagesId').val();
    draftBool = $('.draft0').is(':checked');

    if (draftBool) 
    {
      draft = 1;
    };
      
    $.ajax({
      url:  saveQuizUrl,
      type: "POST",
      data: {'draft':draft,'pageId':pages, 'languageId':languageId},
      async:false,
      success: function(data)
      {
        quizId = data.replace(/(\r\n|\n|\r)/gm,"");
      }
    });

    counter = 0;
    subcounter = 0;

    for (var p = 1; p <= problems; p++) 
    {
          
      correctBool[p] = new Array();
      correctAnswer[p] = new Array();
      optionText[p] = new Array();
      question[p] = CKEDITOR.instances['questionText0'+p+''].getData(); 
      optionsArray[p] = $('.option0'+p+'').size();
        
      $.ajax({
            
      url:  saveQuestionUrl,
      type: "POST",
      data: {'pageId': pages, 'languageId': languageId, 'problem': p, 'question':question[p], 'questionId':0, 'quizId':quizId},
      async:false,
      success: function(data)
      {
        counter++;
        subcounter = 0;

        for (var op = 1; op <= optionsArray[p]; op++) 
        {
          correctBool[p][op] = $('.correctBool0'+p+''+op+'').is(':checked');
          if (correctBool[p][op]) 
          {
            correctAnswer[p][op] = 1;
          }
          else
          {
            correctAnswer[p][op] = 0;
          }   
          
          optionText[p][op] = CKEDITOR.instances['optionText0'+p+''+op+''].getData(); 
                 
          $.ajax(
          {
            url:  saveOptionUrl,
            type: "POST",
            data: {'optionText': optionText[p][op], 'correctAnswer': correctAnswer[p][op], 'problem':p, 'pageId':pages, 'languageId':languageId, 'optionId':0, 'quizId':quizId},
            async:false,
            success: function(data)
            {
              subcounter++;

              if(counter == problems && subcounter == optionsArray[p])
              {
                  $.post('/admin/quiz/loading',{show_per_page:5, currentPage:0}, function(data){
                  $('.paginationContent').html(data);
                      $('.loader').addClass('hidden');           
                  }); 
              }

            }
            });
          }
        }
      });
    }  
  });
  $('.updateQuiz').live('click',function(e){

      e.preventDefault();

      $('.loader').removeClass('hidden');
      quizId = $(this).attr('data-id');
      problems = $('.problems'+quizId+'').size();
      languageId = $('.languages'+quizId+'').val();
      pages = $('.pagesId'+quizId+'').val();

      translate = $('.translate'+quizId+'').is(':checked');
      if (!translate) 
      {
        newQuizId = quizId;
      }

      draftBool = $('.draft'+quizId+'').is(':checked');
      
      if (draftBool)
      {
        draft = 1;
      }; 

      $.ajax({
            url:  saveQuizUrl,
            type: "POST",
            data: {'draft':draft,'pageId':pages, 'languageId':languageId, 'quizId':newQuizId},
            async:false,
            success: function(data)
            {
               if(newQuizId == 0)
               {
                  nquizId = data.replace(/(\r\n|\n|\r)/gm,"");
               }
            }
      });

      counter = 0;
      subcounter = 0; 

      for (var p = 1; p <= problems; p++)
      {
        correctBool[p] = new Array();
        correctAnswer[p] = new Array();
        optionText[p] = new Array();
        optionId[p] = new Array();

        question[p] = CKEDITOR.instances['questionText'+quizId+''+p+''].getData();

        if (translate)
        {
          questionId[p] = 0; 

        }
        else
        {
          questionId[p] = $('.question'+quizId+''+p+'').attr('data-questionId');
          nquizId = quizId;
        }

        $.ajax({
            url:  saveQuestionUrl,
            type: "POST",
            data: {'pageId': pages, 'languageId': languageId, 'problem': p, 'question':question[p], 'questionId':questionId[p], 'quizId':nquizId},
            async:false,
            success: function(data)
            {
              counter++;
              subcounter = 0;

              optionsArray[p] = $('.option'+quizId+''+p+'').size(); 
              
              for (var op = 1; op <= optionsArray[p]; op++)
              {
                correctBool[p][op] = $('.correctBool'+quizId+''+p+''+op+'').is(':checked');
          
                if (correctBool[p][op]) 
                {
                  correctAnswer[p][op] = 1;
                }
                else
                {
                  correctAnswer[p][op] = 0;
                }
                
                optionText[p][op] =  CKEDITOR.instances['optionText'+quizId+''+p+''+op+''].getData();    

                if (translate)
                {
                  optionId[p][op] = 0;
                }
                else
                {
                  optionId[p][op] = $('#option'+quizId+''+p+''+op+'').attr('data-optionId');
                  nquizId = quizId;
                }

                $.ajax({
                          url:  saveOptionUrl,
                          type: "POST",
                          data: {'optionText': optionText[p][op], 'correctAnswer': correctAnswer[p][op], 'problem':p, 'pageId':pages, 'languageId':languageId, 'optionId':optionId[p][op], 'quizId':nquizId},
                          async:false,
                          success: function(data)
                          {
                            subcounter++;
                            if(counter == problems && subcounter == optionsArray[p])
                            {
                                
                                $.post('/admin/quiz/loading',{show_per_page:5, currentPage:0}, function(data){
                                        $('.paginationContent').html(data);
                                         $('.loader').addClass('hidden');
                                }); 
                            }

                          }
                });
              }
            }
          });
      }   
  });

  $('.deleteQuiz').live('click', function(e){
    e.preventDefault();

    quizId = $(this).attr('data-id');

    var r = confirm(confirmText);

    if (r==true)
    {

      $.post(deleteQuizUrl, {quizId:quizId}, function(post){

        $('#gadget'+quizId).hide();
      });

    }
    else
    {

    }    
  });

  $('.problemsCount').change(function(){
    problemsCount = $(this).val();

    $.post(quizProblemsUrl, {problemsCount:problemsCount}, function(data)
    {
      $('.quiz').html(data);
    })
  });

});

//tables
$('#quizPages').dataTable().makeEditable({
    sUpdateURL: function(value, settings)
    {
      //$(this).html(); 
          var pagesId = $(this).parent().attr('data-id');
          var oldPagesName = $(this).attr('data-oldpagesname');
          var columnName = settings.name;
          var pagesName = value;
          var post = "";    
          var error = "";

          $.ajax({
            url:  savePagesUrl,
            type: "POST",
            data: {'pagesId':pagesId, 'pagesName':pagesName},
            async:false,
            success: function(data)
            {
              error = data.length;
              post = data;

            }
         });

          if(error > 2)
          {
              return post;
          }
          else
          {
             return(value); 
          }     
      } ,
    "aoColumns": [
    null,
    {
          indicator: 'Saving platforms...',
          tooltip: 'Click to edit platforms',
          type: 'text',
          name: 'pagesName',
          submit:'Save changes',
          width:'220px',
          id: 1
      },
      null
    ],
      fnOnDeleting: function(tr, id)
      {   

          if(confirm('you are deleting'))
          {
              $("#"+id+"").hide();

              $.post(deletePagesUrl,{pagesId:id});
              return false;
          }
          else
          {
              return false;
          }
          
         
      }
  });

$('#quizLinks').dataTable().makeEditable({
    sUpdateURL: function(value, settings)
    {
          var linksId = $(this).parent().attr('data-id');
          var columnName = settings.name;
          var value = value;

          $.ajax({
            url:  saveLinksUrl,
            type: "POST",
            data: {'linksId':linksId, 'columnName':columnName, title:value},
            async:false,
            success: function(data)
            {
              error = data.length;
              post = data;

            }
         });

          if(error > 2)
          {
              return post;
          }
          else
          {
             return(value); 
          }
          //data: {'pagesId':pagesId, 'pagesName':pagesName},
    },
    "aoColumns": [
      null,
      {
          //language
          type: 'select',
          name: "Parent",
          onblur: 'cancel',
          submit: 'Ok',
          loadurl: '/admin/subchapter/select',
          loadtype: 'POST',
          event: 'click'
      },
      { 
          //title
          indicator: 'Saving platforms...',
          tooltip: 'Click to edit platforms',
          type: 'text',
          name: 'Title',
          submit:'Save changes',
          width:'90%'
      },
      {
          //Pages
          type: 'select',
          onblur: 'cancel',
          name: 'PagesId',
          submit: 'Ok',
          loadurl: '/admin/pages/select/5',
          loadtype: 'GET',
          event: 'click'
      },
      {
          //Order
          indicator: 'Saving platforms...',
          tooltip: 'Click to edit platforms',
          type: 'text',
          name: 'Orders',
          submit:'Save changes',
          width:'90%'
      }

    ],
    fnOnDeleting: function(tr, id)
      {   

          if(confirm('you are deleting'))
          {
              
              values = id.split('s');
              linksId = values[1]


              $("#"+id+"").hide();

              $.post(deleteLinksUrl,{linksId:linksId}, function(data){

              });
              return false;
          }
          else
          {
              return false;
          }
          
         
      }});

function loadnewEditors()
{
    var editors = $("textarea.neweditor");
    if (editors.length) {
        editors.each(function() 
        {
          counter++;
            editorID = $(this).attr("id");
            dst = editorID;

            if( $(this).val() )
            {
                html = $(this).val();
            }            
            CKEDITOR.replace(dst);
        });
        $("textarea.neweditor").hide();

        CKEDITOR.on('instanceReady', function(e) 
        {
          $('.loader').addClass('hidden');
      
    });
    }
}

function addOption(quizId, problem)
{
  optionCount = $('.option'+quizId+''+problem+'').size();
  optionCount++;
  $.post(quizOptionsUrl, {optionsCount:optionCount, problem:problem, quizId:quizId}, function(data){
    $('.options'+quizId+''+problem+'').append(data);
  });
}

function addProblem(quizId)
{
    problemsCount = $('.problems'+quizId).size();
}

function RemoveProblem(quizId)
{   
}

function removeOption(quizId, problem)
{
  if (optionCount > 1) 
  {
    optionCount--;
        $(".options"+quizId+""+problem+" .option:last-child").remove(); 
  };
}

function loadnewEditors() 
{
    var editors = $("textarea.neweditor");
    if (editors.length) {
        editors.each(function() 
        {
          counter++;
          editorID = $(this).attr("id");
           dst = editorID;

          if( $(this).val() )
          {
            html = $(this).val();
          }

          CKEDITOR.replace(dst);
        });
        $("textarea.neweditor").hide();

        CKEDITOR.on('instanceReady', function(e) 
        {
          $('.loader').addClass('hidden');
      
    });
    }
}

function previous(){  
  
    new_page = parseInt( $('.paginationContent').attr('data-currentPage')) - 10;  

    //if there is an item before the current active link run the function  
    if($('.active_page').prev('.page_link').length==true){  
        go_to_page(new_page);  
    } 
    $('.loader').removeClass('hidden'); 
    go_to_page(new_page); 
    
  
}  
  
function next(){  
    
    new_page = parseInt($('.paginationContent').attr('data-currentPage')) + 10;  
    //if there is an item after the current active link run the function  
    if($('.active_page').next('.page_link').length==true){  
        go_to_page(new_page);  
    } 
    $('.loader').removeClass('hidden');

    go_to_page(new_page);  

    

  
}  
function go_to_page(page_num)
{  
    //get the number of items shown per page  

    var show_per_page = parseInt($('.paginationContent').attr('data-showPerPage', show_per_page));  
    
    //get the element number where to start the slice from  
    start_from = page_num * show_per_page;  
  
    //get the element number where to end the slice  
    end_on = start_from + show_per_page;  
    
    //hide all children elements of content div, get specific items and show them  
    $.post('/admin/quiz/loading',{show_per_page:show_per_page, currentPage:page_num}, function(data){
      $('.paginationContent').html(data);
      $('.loader').addClass('hidden');
   
    }); 
  
    /*get the page link that has longdesc attribute of the current page and add active_page class to it 
    and remove that class from previously active page link*/  
    $('.page_link[longdesc=' + page_num +']').addClass('active_page').siblings('.active_page').removeClass('active_page');  
  
    //update the current page input field  
    $('.paginationContent').attr('data-currentPage', ''+page_num+'');
}  


//functions