-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2013 at 11:44 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rasmusmath`
--

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `CurrencyId` int(11) NOT NULL AUTO_INCREMENT,
  `CurrencyCode` varchar(255) NOT NULL,
  `CurrencyName` varchar(155) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`CurrencyId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`CurrencyId`, `CurrencyCode`, `CurrencyName`, `Created`, `Updated`) VALUES
(1, '€', 'euro', '2013-03-01 13:01:30', '2013-03-01 13:01:30'),
(2, 'ISK', 'isk', '2013-03-01 13:07:05', '2013-03-01 13:07:05'),
(3, '£', 'pound', '2013-03-01 13:10:15', '2013-03-01 13:10:15'),
(4, '$', 'dollar', '2013-03-01 13:10:35', '2013-03-01 13:10:35'),
(5, 'DKK', 'dkk', '2013-03-01 13:10:49', '2013-03-01 13:10:49'),
(6, 'NOK', 'nok', '2013-03-01 13:11:08', '2013-03-01 13:11:08'),
(7, 'SEK', 'sek', '2013-03-01 13:11:24', '2013-03-01 13:11:24');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
