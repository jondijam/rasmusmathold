-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 06, 2013 at 09:59 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rasmusmath`
--

-- --------------------------------------------------------

--
-- Table structure for table `payer`
--

CREATE TABLE IF NOT EXISTS `payer` (
  `PayerId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(127) NOT NULL,
  `Email` varchar(127) NOT NULL,
  `SSN` varchar(127) NOT NULL,
  `Address` varchar(127) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Zip` varchar(125) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `Active` varchar(255) NOT NULL,
  `PaymentMethod` int(11) NOT NULL DEFAULT '0',
  `CurrencyId` int(11) NOT NULL,
  PRIMARY KEY (`PayerId`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `SSN` (`SSN`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
