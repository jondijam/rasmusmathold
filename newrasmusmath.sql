-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2013 at 03:05 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rasmusmath`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `ArticleId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(127) NOT NULL,
  `Text` varchar(600) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `Publish` tinyint(1) NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`ArticleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`ArticleId`, `Title`, `Text`, `LanguageId`, `Publish`, `Created`, `Updated`, `Deleted`, `UserId`) VALUES
(1, 'A wonderful serenity ', '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, sans-serif; font-size: 10px;">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>\r\n\r\n<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, sans-serif; font-size: 10px;">I am so happy, my dear friend, so absorbed ', 11, 0, '2002-03-13 13:27:30', '2002-03-13 13:27:30', 1, 1),
(2, 'O my friend ', '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, sans-serif; font-size: 10px;">O my friend -- but it is too much for my strength -- I sink under the weight of the splendour of these visions!</p>\r\n\r\n<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, sans-serif; font-size: 10px;">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>\r\n', 10, 1, '2002-03-13 15:23:12', '2002-03-13 15:23:12', 1, 1),
(3, 'title', '<p><span style="font-family: ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 17px;">issuu.com</span></p>\n', 10, 0, '2002-03-13 17:24:36', '2002-03-13 17:54:48', 1, 1),
(4, '', '', 12, 1, '2002-03-13 17:29:31', '2002-03-13 17:29:31', 1, 1),
(5, 'New guy', '<p>The newest guy in the word is me.</p>\n', 10, 0, '2002-08-13 09:58:19', '2002-08-13 09:58:56', 1, 1),
(6, 'aaaa', '<p>aaaa</p>\r\n', 10, 0, '2002-08-13 11:07:20', '2002-08-13 11:07:20', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `ContactId` int(11) NOT NULL AUTO_INCREMENT,
  `SchoolsId` int(11) NOT NULL,
  `FirstName` varchar(127) NOT NULL,
  `LastName` varchar(127) NOT NULL,
  `Position` varchar(127) NOT NULL,
  `PhoneNumber` varchar(127) NOT NULL,
  `Email` varchar(127) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UsersId` int(11) NOT NULL,
  PRIMARY KEY (`ContactId`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `description`
--

CREATE TABLE IF NOT EXISTS `description` (
  `DescriptionId` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text NOT NULL,
  `LinksId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UsersId` int(11) NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`DescriptionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `description`
--

INSERT INTO `description` (`DescriptionId`, `Description`, `LinksId`, `Created`, `Updated`, `UsersId`, `Deleted`) VALUES
(1, '<p>L&yacute;singin er &aelig;&eth;islegt</p>\n', 1, '2002-08-13 11:10:57', '2002-09-13 12:09:07', 1, 1),
(2, '<p>Almenn brot</p>\n', 7, '2002-11-13 17:27:44', '0000-00-00 00:00:00', 1, 0),
(3, '<p>aaaa a</p>\n', 9, '2002-11-13 17:28:15', '2002-11-13 17:36:20', 1, 0),
(4, '<p>Pr&oacute;sentur og vextir</p>\n', 8, '2002-11-13 17:29:18', '0000-00-00 00:00:00', 1, 0),
(5, '<p>Yngsta stig fjallar um stig</p>\n', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(6, '<p>Mi&eth;stig er Mi&eth;stig</p>\n', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(7, '<p>Unglingastig er stig</p>\n', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(8, '<p>Choose the subject you wish to work on. Every subject is divided into sub-levels. First there is a lesson with explanations and worked examples. When you are ready&nbsp; you take a test. It your test results aren&#39;t good enough then go through the lesson again&nbsp; and then redo the test.&nbsp;<br />\nWhen you have finished all the tests on one subject you can choose another from the list above.<br />\nby clicking with the left mouse button.&nbsp;</p>\n', 88, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(9, '<p>You have finished the primary school, and you need to learn more. Before you start reading the chapters. I want to be sure that you have enough knowledge to be here.&nbsp;<span style="line-height: 1.6em;">So I will ask you some question. If you can answer yes to them all. Then you are on the right place. Good luck and enjoy the math.&nbsp;</span></p>\n', 89, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(10, '<p>Hnitakerfi&eth; er hanna&eth; ..&nbsp;</p>\n', 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(11, '<p>Almenn brot grunnu</p>\n', 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(12, '<p>Hver kannast vi&eth; x = 1 og y = 2 &thorn;&aacute; er x + y = 3? &THORN;etta er d&aelig;mi Algebru. &iacute; &thorn;essum kafla &aelig;tlum vi&eth; a&eth; kenna ykkur grunn atri&eth;i i algebru og hva&eth; ber a&eth; hafa &iacute; huga &thorn;egar &thorn;arf a&eth; reikna algebru d&aelig;mi.&nbsp;</p>\n\n<p>&nbsp;</p>\n', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(13, '<p>J&ouml;fnur er mj&ouml;g kunnuglegt or&eth; en samt er ma&eth;ur ekki viss hva&eth; j&ouml;fnur eru. &Iacute; &thorn;essum kafla &aelig;tlum vi&eth; a&eth; fjalla j&ouml;fnur og &oacute;j&ouml;fnur. Sem d&aelig;mi getum vi&eth; nefnt&nbsp;<span style="line-height: 1.6em;">X - 3&nbsp;+ 3&nbsp;= 10 + 3. Hva&eth; skyldi x n&uacute; vera? Vi&eth; vitum a&eth; jafntog merki segir okkur a&eth; &thorn;a&eth; sem er fyrir framan &thorn;arf a&eth; vera jafn st&oacute;rt og er fyrir aftan.&nbsp;</span><span style="line-height: 1.6em;">&nbsp;&nbsp;</span></p>\n', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(14, '<p>&THORN;a&eth; eru horn &uacute;t um allt. Ef vi&eth; horfum &aacute; mynd og sp&aacute;um &iacute; grunn formin &thorn;&aacute; sj&aacute;um vi&eth; horn &thorn;egar vi&eth; horfum &aacute; kassa. &Iacute; &thorn;essum kafla &aelig;tlum vi&eth; a&eth; kynna nokkur l&ouml;gm&aacute;l &thorn;egar kemur a&eth; hornum.&nbsp;</p>\n', 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(15, '<p>&Iacute; &thorn;essum kafla langar okkur a&eth; kynna ykkur fyrir hva&eth; horn er og &thorn;au l&ouml;gm&aacute;l sem &thorn;ar eru notu&eth;.&nbsp;</p>\n', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(16, '<p>R&uacute;m og flatarm&aacute;l er eitthva&eth; sem allir &thorn;urfa a&eth; l&aelig;ra. &THORN;egar vi&eth; erum a&eth; reikna &uacute;t hva&eth; h&uacute;si&eth; okkar er st&oacute;rt &thorn;&aacute; erum vi&eth; a&eth; tala um flatarm&aacute;l. Hva&eth; er flatarm&aacute;l spyrjar margir? &Iacute; &thorn;essum kafla &aelig;tlum vi&eth; a&eth; fjalla um flatarm&aacute;l og allt sem tengist &thorn;v&iacute;.&nbsp;</p>\n', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(17, '<p>Hefur&eth;u unni&eth; &iacute; lott&oacute;i? Hversu margar l&iacute;kur er &aacute; &thorn;v&iacute; a&eth; &thorn;&uacute; vinnur &iacute; lott&oacute;i? &Iacute; &thorn;essum kafla langar okkur a&eth; kynna l&iacute;kindareikning.</p>\n', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(18, '<p>&THORN;a&eth; skiptir m&aacute;li &iacute; hvernig r&ouml;&eth; ma&eth;ur reiknar d&aelig;mi. Til d&aelig;mis &thorn;&aacute; hefur <strong>margf&ouml;ldun og deiling</strong> forgang &aacute; pl&uacute;s.&nbsp;</p>\n\n<ol>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»3«/mn»«mo»§#160;«/mo»«mo»+«/mo»«mo»§#160;«/mo»«mn»3«/mn»«mo»§#160;«/mo»«mo»§#183;«/mo»«mo»§#160;«/mo»«mn»2«/mn»«mo»§#160;«/mo»«mo»=«/mo»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=6e8d75c767a362e9ed8d845c4a319b1a.png" /></li>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»3«/mn»«mo»+«/mo»«mn»6«/mn»«mo»§#160;«/mo»«mo»=«/mo»«mo»§#160;«/mo»«mo»?«/mo»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=d332aa755c0dfa3627f0005a3682ee28.png" /></li>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»3«/mn»«mo»§#160;«/mo»«mo»+«/mo»«mo»§#160;«/mo»«mn»6«/mn»«mo»§#160;«/mo»«mo»=«/mo»«mo»§#8201;«/mo»«mn»9«/mn»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=58f5d7310caea773cd1daae24a601c9e.png" /></li>\n</ol>\n', 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(19, '<p>Stundum &thorn;arft &thorn;&uacute; a&eth; skilja t&ouml;lur sem eru minni en 0. T.d. &thorn;egar &thorn;&uacute; sko&eth;ar hitam&aelig;li a&eth; vetri til og m&aelig;lirinn s&yacute;nir - t&ouml;lu e&eth;a frost.&nbsp;</p>\n', 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(20, '<p><img alt="Kvarði" src="http://rasmus.is/Is/T/m/st22k1m03.gif" style="width: 345px; height: 44px;" /></p>\n\n<p>Er&nbsp;<strong>25&nbsp;</strong>n&aelig;r &thorn;v&iacute; a&eth; vera <strong>20</strong> en <strong>30</strong> ?</p>\n\n<p>Sko&eth;a&eth;u <strong>kynningu 1</strong> og taktu s&iacute;&eth;an pr&oacute;fi&eth;.</p>\n', 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(21, '<p>Vi&eth; hugsum okkur t&ouml;lu og setjum hana inn &iacute; nokkurs konar algebruv&eacute;l, Veslings talan lendir &iacute; alls kyns hremmingum og a&eth;ger&eth;um. A&eth; lokum ver&eth;ur eftir a&eth;eins talan 1, hver &aelig;tli upphaflega talan hafi veri&eth; ?</p>\n\n<p>Sko&eth;a&eth;u kynninguna til &thorn;ess a&eth; komast a&eth; svarinu.&nbsp;</p>\n', 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(22, '<p>&THORN;egar vi&eth; erum a&eth; tala um pr&oacute;sentur &thorn;&aacute; erum vi&eth; oftast a&eth; tala um hluta &uacute;r einhverri heild. Td er 50% helmingurinn af 100%.&nbsp;</p>\n', 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(23, '<p>Vi&eth; m&aelig;lum me&eth; &yacute;msum m&aelig;lit&aelig;kjum, svo sem klukku, vog, m&aacute;lbandi o.s.frv.&nbsp;H&eacute;r reynum vi&eth; a&eth; skilja &thorn;&aelig;r m&aelig;lieiningar sem mest eru nota&eth;ar.</p>\n\n<h2>Metrakerfi&eth;</h2>\n\n<p>&THORN;&uacute; &thorn;ekkir eflaust algengustu einingar &iacute; metrakerfinu eins og k&iacute;l&oacute;meter&nbsp;<strong>(km)</strong>, meter&nbsp;<strong>(m)</strong>&nbsp;og sentimeter&nbsp;<strong>(cm)</strong>.&nbsp; Vi&eth; skulum ra&eth;a &thorn;eim upp &iacute; talnah&uacute;s til a&eth; sj&aacute; sambandi&eth; &aacute; milli eininganna.</p>\n\n<h2>Talnah&uacute;si&eth;</h2>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm04k1p1.gif" style="width: 449px; height: 178px;" /></p>\n\n<p>Ef vi&eth; hugsum um st&aelig;r&eth;ina 1 meter og setjum hana inn &iacute; talnah&uacute;si&eth; undir vi&eth;eigandi s&aelig;ti og fyllum &ouml;nnur s&aelig;ti me&eth; 0 &thorn;&aacute; s&eacute;st a&eth; 1 m = 1000 millimetrar. Sko&eth;a&eth;u bl&aacute;a sv&aelig;&eth;i&eth; &aacute; myndinni. &Aacute; sama h&aacute;tt s&eacute;st a&eth; 1 m = 10 dm og 1m&nbsp; = 100 cm. &THORN;a&eth; munar alltaf um 10 &aacute; milli s&aelig;ta eins og &iacute; tugakerfinu.</p>\n\n<ul>\n	<li>1m = 10dm</li>\n	<li>1dm = 10cm</li>\n	<li>1cm = 10 mm</li>\n</ul>\n\n<h2>T&ouml;kum nokkur d&aelig;mi.&nbsp;</h2>\n\n<h3>D&aelig;mi 1</h3>\n\n<p>Hva&eth; eru 6,5 k&iacute;l&oacute;metrar margir sentimetrar ? Vi&eth; setjum 6,5 inn &iacute; talnah&uacute;si&eth; 6 undir s&aelig;ti&eth; km og 5 &iacute; n&aelig;sta s&aelig;ti vi&eth; hli&eth;ina.</p>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm04k1p2.gif" style="width: 449px; height: 73px;" /></p>\n\n<p>S&iacute;&eth;an fyllum vi&eth; me&eth; 0 &iacute; au&eth;u s&aelig;tin ni&eth;ur a&eth; cm. &THORN;&aacute; er au&eth;velt a&eth; lesa t&ouml;luna sem 6,5 km standa fyrir , vi&eth; sj&aacute;um a&eth; 6,5 km = 650000 cm.</p>\n\n<h3>D&aelig;mi 2</h3>\n\n<p>Hva&eth; eru 250 m margir k&iacute;l&oacute;metrar ? Vi&eth; setjum 250 metra inn &iacute; talnah&uacute;si&eth; og lesum af.</p>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm04k1p3.gif" style="width: 449px; height: 103px;" /></p>\n\n<p>Fyllum me&eth; 0 upp a&eth; km. s&aelig;tinu og setjum kommuna &aacute; eftir km. &thorn;ar sem 250 m. eru minna en 1 km.&nbsp; Lesum svari&eth; 250 m = 0,250 km.&nbsp;</p>\n', 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(24, '<p>Til &thorn;ess a&eth; finna flatarm&aacute;l &thorn;urfum vi&eth; oftast n&aelig;r a&eth; reikna &uacute;t fr&aacute; tveimur e&eth;a fleiri m&aelig;lingum. &THORN;&aelig;gilegast er a&eth; vinna me&eth; r&eacute;tthyrninga sem eru fletir me&eth; fj&oacute;rum hornum og fj&oacute;rum hli&eth;um og &ouml;ll hornin r&eacute;tt.</p>\n\n<h2>Flatarm&aacute;l r&eacute;tthyrnings.</h2>\n\n<p>Fl&ouml;tur&nbsp; sem er r&eacute;tthyrndur og 1cm &aacute; alla kanta hefur flatarm&aacute;li&eth; einn fersentimetri.<br />\nSkrifa&eth;: <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«mo»§#160;«/mo»«msup»«mi»cm«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=831ac1a2690785d1a0d2aecf845adfed.png" />&nbsp;</p>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm05k01m01.gif" style="width: 108px; height: 76px; float: left; margin-left: 10px; margin-right: 10px;" /></p>\n\n<p>&THORN;essi litli bl&aacute;i fl&ouml;tur er 1 cm. &aacute; hvern kant, hann er &thorn;v&iacute; <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«mi»c«/mi»«msup»«mi»m«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=c432f6a981f20cb76bdfa1c6ffb815d3.png" />.&nbsp;&Aacute; sama h&aacute;tt yr&eth;i fl&ouml;tur&nbsp; sem v&aelig;ri r&eacute;tthyrndur og 1 m. &aacute; alla kanta me&eth; flatarm&aacute;li&eth; einn fermetri, skrifa&eth;: <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«msup»«mi»m«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=b986de16210ef139167cab965f022a6f.png" />.&nbsp;&THORN;&aelig;r einingar &iacute; flatarm&aacute;li sem eru mest nota&eth;ar eru <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«mi»c«/mi»«msup»«mi»m«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=c432f6a981f20cb76bdfa1c6ffb815d3.png" />&nbsp;og <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«msup»«mi»m«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=b986de16210ef139167cab965f022a6f.png" />.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h2>A&eth; reikna flatarm&aacute;l.</h2>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm05k01m02.gif" style="width: 240px; height: 115px; float: left; margin-right: 10px;" />Ef hver ferningur &aacute; &thorn;essari mynd v&aelig;ri <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»1«/mn»«mo»§#160;«/mo»«msup»«mi»cm«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=831ac1a2690785d1a0d2aecf845adfed.png" />&nbsp;. &THORN;a&eth; er h&aelig;gt a&eth; telja &thorn;&aacute; og vi&eth; sj&aacute;um a&eth; &ouml;ll myndin af bl&aacute;u fl&ouml;tunum er <img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»10«/mn»«mo»§#160;«/mo»«msup»«mi»cm«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=1568b9edb6d8bde19dc957f6d870e99b.png" />.</p>\n\n<p>&Ouml;nnur lei&eth; v&aelig;ri a&eth; margfalda lengd me&eth; breidd. Flatarm&aacute;li&eth; = &nbsp;<img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»5 cm«/mn»«mo»§#160;«/mo»«mo»§#183;«/mo»«mo»§#160;«/mo»«mo»§#160;«/mo»«mn»2 cm«/mn»«mo»§#160;«/mo»«mo»=«/mo»«mn»10«/mn»«msup»«mi»cm«/mi»«mn»2«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=aaa6a66a115dba5478fe751346360747.png" /></p>\n\n<p>&nbsp;</p>\n\n<h2>&nbsp;</h2>\n\n<h2>A&eth; reikna umm&aacute;l.</h2>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm05k01m01.gif" style="width: 108px; height: 76px; float: left; margin-right: 10px;" />Ef vi&eth; leggjum saman allar hli&eth;ar &aacute; einhverjum fleti &thorn;&aacute; f&aacute;um vi&eth; &uacute;t hva&eth; umm&aacute;li&eth; er miki&eth;. D&aelig;mi litli bl&aacute;i fl&ouml;turinn er 1cm &aacute; hvern kant ,hann er &thorn;v&iacute; me&eth; umm&aacute;li&eth;:</p>\n\n<p>U = 1cm + 1cm + 1cm +1cm = 4 cm.</p>\n\n<p>&nbsp;</p>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/stm05k01m02.gif" style="width: 240px; height: 115px; float: left; margin-right: 10px;" />&Aacute; &thorn;essum st&oacute;ra bl&aacute;a yr&eth;i umm&aacute;li&eth; &aacute; sama h&aacute;tt:&nbsp;</p>\n\n<p>U = 2cm + 5cm + 2cm + 5cm = 14 cm.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;">Athugi&eth; a&eth; lengdarm&aelig;lingar skila s&eacute;r ekki alltaf r&eacute;ttar &aacute; skj&aacute;myndum e&eth;a &iacute; &uacute;tprentun &thorn;annig a&eth; ekki er r&eacute;tt a&eth; treysta fullkomlega &aacute; a&eth; ofangreindar myndir standist &thorn;au m&aacute;l sem eru gefin upp &aacute; &thorn;eim.</div>\n', 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(25, '<p>Teningur hefur allar hli&eth;ar jafn langar, &ouml;ll horn eins og alla fleti jafn st&oacute;ra.&nbsp; Pl&aacute;ssi&eth; sem teningurinn tekur kallast r&uacute;mm&aacute;l.&nbsp;</p>\n\n<p><span style="line-height: 1.6em;"><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/m/stm06m02.gif" style="width: 63px; height: 152px;" />​Fimm teningar taka 5 sinnum meira pl&aacute;ss en 1 teningur.</span></p>\n\n<p><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/m/stm06m03.gif" style="width: 94px; height: 134px;" />&nbsp;Tveir staflar saman me&eth; 4 teningum &iacute; hvorum stafla:&nbsp; 2&nbsp;&times; 4 = 8 teningar.</p>\n\n<p><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/m/stm06m04.gif" style="width: 95px; height: 91px;" />Fj&oacute;rir staflar saman.&nbsp; Tveir teningar &iacute; hverjum stafla:&nbsp; 2&nbsp;&times; 2 &times; 2 = 8 teningar</p>\n\n<h3>D&aelig;mi</h3>\n\n<p><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/m/stm06m05.gif" style="width: 192px; height: 114px;" />&nbsp;cm = sentimetrar, m = metrar, R = R&uacute;mm&aacute;l&nbsp;</p>\n\n<p><span style="line-height: 1.6em;">R = 6m&nbsp;&times; 3m &times; 2m = </span><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»36«/mn»«msup»«mi»m«/mi»«mn»3«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=6a78a6b2acee23354c4846ade50004ea.png" style="line-height: 1.6em;" /><span style="line-height: 1.6em;">, =&gt; lesi&eth; r&uacute;mmetrar.&nbsp;</span><br />\n<span style="line-height: 1.6em;">&THORN;essi kassi er bygg&eth;ur &uacute;r 36 kubbum &thorn;ar sem hver kubbur er 1 meter &aacute; hvern kant</span></p>\n\n<h3><span style="line-height: 1.6em;">D&aelig;mi</span></h3>\n\n<p><span style="line-height: 1.6em;"><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/m/stm06m07.gif" style="width: 216px; height: 160px;" />Til a&eth; reikna r&uacute;mm&aacute;l &aacute; svona byggingum getur veri&eth; gott a&eth; skipta &thorn;eim &iacute; kassa.</span></p>\n\n<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;">\n	<tbody>\n		<tr>\n			<td style="text-align: center; vertical-align: middle;">R&uacute;mm&aacute;l&nbsp;gula&nbsp;kassans.</td>\n			<td style="text-align: center; vertical-align: middle;">R = 4m&nbsp;&times; 2m &times; 2m =&nbsp;<img align="middle" class="Wirisformula" data-mathml="«math style=¨font-size:12px¨ xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»16«/mn»«msup»«mi mathvariant=¨normal¨»m«/mi»«mn»3«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=6ddb77e39988a3cf7129e6fe7bc251df.png" /></td>\n		</tr>\n		<tr>\n			<td style="text-align: center; vertical-align: middle;">R&uacute;mm&aacute;l&nbsp;gr&aelig;na&nbsp;kassans.</td>\n			<td style="text-align: center; vertical-align: middle;">R = 5m&nbsp;&times; 3m &times; 4m =&nbsp;<img align="middle" class="Wirisformula" data-mathml="«math style=¨font-size:12px¨ xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»60«/mn»«msup»«mi mathvariant=¨normal¨»m«/mi»«mn»3«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=8078539d79b4723e248f09e049ed7fde.png" /></td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;&Ouml;ll byggingin =&nbsp;gr&aelig;ni&nbsp;+&nbsp;guli&nbsp;=&nbsp;<img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«msup»«mn»60m«/mn»«mn»3«/mn»«/msup»«msup»«mo»+ 16m«/mo»«mn»3«/mn»«/msup»«msup»«mo»= 76m«/mo»«mn»3«/mn»«/msup»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=a4cd44a5a11d180f9c95bab39c7654e0.png" /></p>\n\n<p>&nbsp;</p>\n', 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(26, '<p>Stundum er betra a&eth; skilja t&ouml;lur ef &thorn;&aelig;r er settar fram sem myndrit. &iacute; T&ouml;lfr&aelig;&eth;i notum vi&eth; myndrit til &thorn;ess a&eth; t&uacute;lka t&ouml;lur. &Iacute; &thorn;essum &thorn;&aelig;tti er einnig v&iacute;sa&eth; til T&ouml;lvul&aelig;sivefsins&nbsp;&aacute; Rasmus.is &thorn;ar getur &thorn;&uacute; gert &yacute;msar tilraunir me&eth; t&ouml;lur og myndir me&eth; t&ouml;flureikninum&nbsp;Excel.</p>\n\n<h2>T&iacute;&eth;nit&ouml;flur - s&uacute;lurit - l&iacute;nurit</h2>\n\n<p>T&iacute;u unglingar voru spur&eth;ir hve oft &thorn;eir f&oacute;ru &iacute; b&iacute;&oacute; (kvikmyndah&uacute;s) s&iacute;&eth;asta m&aacute;nu&eth;.</p>\n\n<table border="0" cellpadding="1" cellspacing="1" class="table" style="width: 500px;">\n	<thead>\n		<tr>\n			<th scope="col">Nafn:</th>\n			<th scope="col">Fj&ouml;ldi b&iacute;&oacute;fer&eth;a</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>Bj&ouml;rn</td>\n			<td>1</td>\n		</tr>\n		<tr>\n			<td>Anna</td>\n			<td>0</td>\n		</tr>\n		<tr>\n			<td>Siggi</td>\n			<td>0</td>\n		</tr>\n		<tr>\n			<td>St&iacute;na</td>\n			<td>2</td>\n		</tr>\n		<tr>\n			<td>&Oacute;li</td>\n			<td>1</td>\n		</tr>\n		<tr>\n			<td>Magn&uacute;s</td>\n			<td>2</td>\n		</tr>\n		<tr>\n			<td>Gr&eacute;ta</td>\n			<td>1</td>\n		</tr>\n		<tr>\n			<td>R&uacute;na</td>\n			<td>4</td>\n		</tr>\n		<tr>\n			<td>Palli</td>\n			<td>0</td>\n		</tr>\n		<tr>\n			<td>Kalli</td>\n			<td>1</td>\n		</tr>\n	</tbody>\n</table>\n\n<p><strong>T&iacute;&eth;nitafla</strong>&nbsp;vi&eth; setjum ni&eth;urst&ouml;&eth;urnar upp &iacute;&nbsp;<strong>t&iacute;&eth;nit&ouml;flu</strong>&nbsp;og teljum hve m&ouml;rg tilfelli reyndust fara aldrei &iacute; b&iacute;&oacute;, einu sinni &iacute; b&iacute;&oacute; o.s.frv.&nbsp;</p>\n\n<table border="0" cellpadding="1" cellspacing="1" class="table" style="width: 500px;">\n	<thead>\n		<tr>\n			<th scope="col">Fj&ouml;ldi b&iacute;&oacute;fer&eth;a</th>\n			<th scope="col">Fj&ouml;ldi tilfella (barna)</th>\n			<th scope="col">&nbsp;</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td>0</td>\n			<td>3</td>\n			<td>&THORN;r&iacute;r f&oacute;ru aldrei &iacute; b&iacute;&oacute; &iacute; m&aacute;nu&eth;inum.</td>\n		</tr>\n		<tr>\n			<td>1</td>\n			<td>4</td>\n			<td>Fj&oacute;rir f&oacute;ru einu sinni &iacute; b&iacute;&oacute; &iacute; m&aacute;nu&eth;inum.</td>\n		</tr>\n		<tr>\n			<td>2</td>\n			<td>2</td>\n			<td>Tveir f&oacute;ru tvisvar &iacute; b&iacute;&oacute; &iacute; m&aacute;nu&eth;inum.</td>\n		</tr>\n		<tr>\n			<td>3</td>\n			<td>0</td>\n			<td>Enginn f&oacute;r &thorn;risvar sinnum &iacute; b&iacute;&oacute; &iacute; m&aacute;nu&eth;inum.</td>\n		</tr>\n		<tr>\n			<td>4</td>\n			<td>1</td>\n			<td>Einn f&oacute;r fj&oacute;rum sinnum &iacute; b&iacute;&oacute; &iacute; m&aacute;nu&eth;inum.</td>\n		</tr>\n	</tbody>\n</table>\n\n<p><strong>T&iacute;&eth;ni</strong>&nbsp;merkir hve oft atri&eth;i (tilfelli) koma fyrir. &Iacute; t&ouml;lfr&aelig;&eth;i v&aelig;ri tala&eth; um tilfelli&eth; &quot;a&eth; fara einu sinni &aacute; m&aacute;nu&eth;i &iacute; b&iacute;&oacute;&quot; hef&eth;i t&iacute;&eth;nina 4.</p>\n\n<p>Hver v&aelig;ri t&iacute;&eth;nin fyrir tilfelli&eth;: &quot;Fer &iacute; b&iacute;&oacute; fj&oacute;rum sinnum &iacute; m&aacute;nu&eth;i&quot; ?<br />\n<strong>Svar:</strong> T&iacute;&eth;nin v&aelig;ri 1</p>\n\n<h3>S&uacute;lurit</h3>\n\n<p>S&uacute;lurit&nbsp;oft er au&eth;veldara og sk&yacute;rara a&eth; lesa uppl&yacute;singar &uacute;r myndritum.&nbsp;<span style="line-height: 1.6em;">Setjum ni&eth;urst&ouml;&eth;urnar fram me&eth; s&uacute;luriti.</span></p>\n\n<p>&nbsp;<img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/U/st21k01_01.gif" style="width: 494px; height: 302px;" /></p>\n\n<h3>L&iacute;nurit</h3>\n\n<p>Anna f&oacute;r til Kanar&iacute;eyja &iacute; 7 daga fr&iacute; og m&aelig;ldu h&uacute;n lofthitann &aacute; hverjum degi alltaf &aacute; sama t&iacute;ma kl: 16:00.&nbsp;<span style="line-height: 1.6em;">M&aelig;lingar hennar litu &thorn;annig &uacute;t.</span></p>\n\n<p><span style="line-height: 1.6em;"><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/U/st21k01_02.gif" style="width: 484px; height: 292px;" /></span></p>\n\n<p><span style="line-height: 1.6em;">​&Aacute; sj&ouml;unda degi var heitast e&eth;a 35&deg;C.&nbsp;&nbsp;&Aacute; fimmta deginum var kaldast e&eth;a 15&deg;C.</span></p>\n\n<p>&nbsp;</p>\n', 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(27, '<p>Vi&eth; reiknum l&iacute;kur b&aelig;&eth;i &iacute; spilum og &iacute; &yacute;msum st&aelig;r&eth;fr&aelig;&eth;ilegum athugunum.</p>\n\n<p><span style="line-height: 1.6em;">L&iacute;kur eru miki&eth; nota&eth;ar &iacute; daglegu l&iacute;fi.&nbsp; Til d&aelig;mis &iacute; sambandi vi&eth; umfer&eth;, sj&uacute;kd&oacute;ma og happdr&aelig;tti.&nbsp; &THORN;a&eth; m&aacute; finna l&iacute;kur &aacute; tvo vegu.&nbsp; Fr&aelig;&eth;ilegar l&iacute;kur eru fundnar me&eth; hreinum &uacute;treikningum, en raunverulegar l&iacute;kur eru fundnar me&eth; tilraunum.</span></p>\n\n<p><span style="line-height: 1.6em;">Vi&eth; sko&eth;um n&uacute; atri&eth;i sem fengist er vi&eth; &iacute; sk&oacute;lanum.</span></p>\n\n<h3><span style="line-height: 1.6em;">D&aelig;mi1&nbsp;</span></h3>\n\n<p>Hendum upp 10 kr. peningi.&nbsp; L&iacute;kurnar &aacute; a&eth; fiskarnir komi upp er einn af tveim m&ouml;gulegum.&nbsp; Svara m&aacute; &iacute;&nbsp;or&eth;um,&nbsp;brotum,&nbsp;tugabrotum&nbsp;e&eth;a&nbsp;pr&oacute;sentum:</p>\n\n<p><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/U/st12k01m01.gif" style="width: 433px; height: 72px;" /></p>\n\n<h3>D&aelig;mi 2</h3>\n\n<p>L&iacute;kur &aacute; a&eth; f&aacute; hjarta ef dregi&eth; er spil &uacute;r spilastokk.</p>\n\n<p><img alt="" src="http://rasmus.is/Is/T/U/st12k01m02.gif" style="width: 227px; height: 172px;" /></p>\n', 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(28, '<p>Vi&eth; m&aelig;lum t&iacute;mann oftast me&eth; klukkum ef vi&eth; hugsum um styttri t&iacute;mabil. &THORN;&aacute; t&ouml;lum vi&eth; um klukkustundir, m&iacute;n&uacute;tur og sek&uacute;ndur.</p>\n', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(29, '<p><img alt="" class="img-polaroid" src="http://rasmus.is/Is/T/U/stm09k1m1.gif" style="width: 130px; height: 206px;" /></p>\n\n<p>&THORN;etta armbands&uacute;r s&yacute;nir a&eth; klukkan er 25 m&iacute;n&uacute;tur yfir 10, &THORN;&uacute; veist a&eth; litli v&iacute;sirinn telur klukkut&iacute;mana, st&oacute;ri v&iacute;sirinn m&iacute;n&uacute;turnar og s&aacute; mj&oacute;i sek&uacute;ndur.</p>\n\n<p>Nokkrar sta&eth;reyndir um t&iacute;mann.</p>\n\n<table border="0" cellpadding="1" cellspacing="1" class="table">\n	<tbody>\n		<tr>\n			<td>&Aacute;ri&eth; er 365 dagar ( 366 ) dagar ef hlaup&aacute;r.</td>\n		</tr>\n		<tr>\n			<td>&Aacute;ri&eth; er 12 m&aacute;nu&eth;ir.</td>\n		</tr>\n		<tr>\n			<td>M&aacute;nu&eth;ur er ca. 30 dagar. ( &thorn;eir eru mislangir )</td>\n		</tr>\n		<tr>\n			<td>&Aacute;ri&eth; er 52 vikur.</td>\n		</tr>\n		<tr>\n			<td>Vikan er 7 s&oacute;larhringar.</td>\n		</tr>\n		<tr>\n			<td>S&oacute;larhringur er 24 klukkustundir.</td>\n		</tr>\n		<tr>\n			<td>Ein klukkustund er 60 m&iacute;n&uacute;tur.</td>\n		</tr>\n		<tr>\n			<td>Ein m&iacute;n&uacute;ta er 60 sek&uacute;ndur.</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>H&eacute;r &aacute; eftir koma nokkur d&aelig;mi um t&iacute;ma&uacute;treikning, &thorn;au hj&aacute;lpa &thorn;&eacute;r kannski, en annars notar &thorn;&uacute; &thorn;&aelig;r lei&eth;ir sem &thorn;&eacute;r finnast au&eth;veldastar e&eth;a &aacute;rangursr&iacute;kastar.&nbsp;</p>\n\n<p>&nbsp;</p>\n', 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `individual`
--

CREATE TABLE IF NOT EXISTS `individual` (
  `IndividualId` int(11) NOT NULL,
  `Name` varchar(127) NOT NULL,
  `SSN` varchar(127) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Email` varchar(127) NOT NULL,
  `Address` varchar(127) NOT NULL,
  `Zip` varchar(127) NOT NULL,
  `City` varchar(127) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `LanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `Code` varchar(200) NOT NULL,
  `ImageSrc` varchar(200) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UsersId` int(11) NOT NULL,
  PRIMARY KEY (`LanguageId`),
  UNIQUE KEY `short_name` (`Code`),
  UNIQUE KEY `name` (`Name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`LanguageId`, `Name`, `Code`, `ImageSrc`, `Created`, `Updated`, `UsersId`) VALUES
(12, 'Dansk', 'dk', 'Denmark.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(11, 'English (UK)', 'en', 'UK.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, 'Íslenska', 'is', 'Iceland.png', '2010-07-18 17:00:00', '2010-07-19 12:26:19', 1),
(13, 'Svenska', 'sv', 'Sweden.png', '2010-07-19 12:36:56', '2010-07-19 12:38:55', 1),
(14, 'Norsk', 'no', 'Norway.png', '2010-07-19 12:38:34', '2010-07-19 12:38:34', 1),
(15, 'Русский', 'ru', 'Russia.png', '2010-07-19 12:42:52', '2010-07-19 12:42:52', 1),
(16, 'Español (Spain)', 'es', 'Spain.png', '2010-07-19 12:45:07', '2010-07-19 12:45:07', 1),
(17, 'Polski', 'pl', 'Poland.png', '2010-07-19 12:47:15', '2010-07-19 12:47:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE IF NOT EXISTS `lesson` (
  `LessonId` int(11) NOT NULL AUTO_INCREMENT,
  `Text` text,
  `PagesId` int(11) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `OldLesson` tinyint(1) NOT NULL,
  `IFrame` varchar(127) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`LessonId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`LessonId`, `Text`, `PagesId`, `LanguageId`, `OldLesson`, `IFrame`, `Created`, `Updated`) VALUES
(3, '<p>samasem merki&eth; (=) &thorn;&yacute;&eth;ir a&eth; &thorn;a&eth; &aacute; a&eth; vera jafn miki&eth; b&aacute;&eth;um megin vi&eth; merki&eth;.</p>\n\n<ul>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»2«/mn»«mo»§#160;«/mo»«mo»=«/mo»«mo»§#160;«/mo»«mn»2«/mn»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=b6e848ae1e0934b61124d3e3bb2b2a00.png" /></li>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»4«/mn»«mo»§#160;«/mo»«mo»-«/mo»«mo»§#160;«/mo»«mn»1«/mn»«mo»§#8201;«/mo»«mo»=«/mo»«mo»§#160;«/mo»«mn»3«/mn»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=95823012b773376c6aa88887a14a9b3a.png" /></li>\n	<li><img align="middle" class="Wirisformula" data-mathml="«math xmlns=¨http://www.w3.org/1998/Math/MathML¨»«mn»5«/mn»«mo»§#160;«/mo»«mo»=«/mo»«mo»§#160;«/mo»«mn»2«/mn»«mo»+«/mo»«mn»3«/mn»«/math»" src="/assets/ckeditor//plugins/ckeditor_wiris/integration/showimage.php?formula=cab01c4c67ea1420092bda773bfdacd3.png" /></li>\n</ul>\n', 96, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '<ul>\n	<li>= jafnt og</li>\n	<li>&lt; minna en</li>\n	<li>&gt; st&aelig;rra en</li>\n</ul>\n\n<table border="0" cellspacing="10" style="width: 500px;">\n	<tbody>\n		<tr>\n			<td><img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="vertical-align: middle; height: 30px; width: 25px;" />+<img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="opacity: 0.9; text-align: center; vertical-align: middle; height: 30px; width: 25px;" />&nbsp;&gt;<img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="opacity: 0.9; text-align: center; vertical-align: middle; height: 30px; width: 25px;" /></td>\n			<td>Oddurinn v&iacute;sar &aacute; &thorn;a&eth; sem er minna</td>\n		</tr>\n		<tr>\n			<td><img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="text-align: center; vertical-align: middle; height: 30px; width: 25px;" /> =&nbsp;<img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="text-align: center; vertical-align: middle; height: 30px; width: 25px;" /></td>\n			<td>Jafn st&oacute;rt b&aacute;&eth;um megin vi&eth; merki&eth;</td>\n		</tr>\n		<tr>\n			<td><img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="vertical-align:middle;height:30px;width:25px;" />&nbsp;&lt;&nbsp;<img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="vertical-align:middle;height:30px;width:25px;" />&nbsp;+&nbsp;<img alt="" src="http://www.rasmus.is/Is/T/Y/AN01124_.gif" style="vertical-align:middle;height:30px;width:25px;" /></td>\n			<td>Oddurinn v&iacute;sar &aacute; &thorn;a&eth; sem er minna</td>\n		</tr>\n		<tr>\n			<td>3 &gt; 1</td>\n			<td>Oddurinn v&iacute;sar &aacute; &thorn;a&eth; sem er minna</td>\n		</tr>\n		<tr>\n			<td>4 = 4</td>\n			<td>Jafn miki&eth; b&aacute;&eth;um megin vi&eth; merki&eth;</td>\n		</tr>\n		<tr>\n			<td>2 &lt; 3</td>\n			<td>Oddurinn v&iacute;sar &aacute; &thorn;a&eth; sem er minna</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n', 97, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '<p>T&aacute;kna m&aacute; brot me&eth; myndum og t&ouml;lust&ouml;fum&nbsp;</p>\n', 106, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '<p>&Iacute; september nota&eth;i Palli &thorn;rj&uacute; mj&oacute;lkurmi&eth;akort og einn &thorn;ri&eth;ja af fj&oacute;r&eth;a kortinu, &iacute; sk&oacute;lanum s&iacute;num.</p>\n\n<p><img alt="Mjólk, Mjólk" class="middle" lang="is" src="http://rasmus.is/Is/T/m/stm01k01m01.gif" style="width: 75px; height: 350px;" /> +<img alt="Mjólk, Mjólk" class="middle" lang="is" src="http://rasmus.is/Is/T/m/stm01k01m01.gif" style="width: 75px; height: 350px;" />+<img alt="Mjólk, Mjólk" class="middle" lang="is" src="http://rasmus.is/Is/T/m/stm01k01m01.gif" style="width: 75px; height: 350px;" /></p>\n', 94, 10, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `LinksId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(127) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `Publish` tinyint(1) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Parent` int(11) NOT NULL DEFAULT '0',
  `Order` int(11) NOT NULL DEFAULT '0',
  `Deleted` tinyint(1) NOT NULL,
  `DeletedIp` varchar(127) DEFAULT NULL,
  `PagesId` int(11) NOT NULL,
  PRIMARY KEY (`LinksId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`LinksId`, `Title`, `LanguageId`, `Publish`, `UserId`, `Created`, `Updated`, `Parent`, `Order`, `Deleted`, `DeletedIp`, `PagesId`) VALUES
(1, 'Yngsta stig', 10, 1, 1, '2002-05-13 14:01:01', '2002-08-13 07:59:35', 0, 1, 0, NULL, 1),
(2, 'Miðstig', 10, 1, 1, '2002-05-13 14:13:33', '2002-05-13 14:13:33', 0, 2, 1, NULL, 2),
(3, 'Mellanstadiet', 13, 0, 1, '2002-05-13 15:46:15', '2002-08-13 08:21:53', 0, 1, 1, NULL, 2),
(4, 'Miðstig', 10, 1, 1, '2002-09-13 13:24:10', '2002-09-13 13:24:10', 0, 2, 0, NULL, 2),
(5, 'Algebra og reiknireglur', 10, 1, 1, '2002-11-13 15:43:49', '2013-02-24 22:25:53', 4, 1, 0, NULL, 5),
(6, 'Jöfnur og ójöfnur', 10, 1, 1, '2002-11-13 15:45:09', '2013-02-24 22:25:56', 4, 2, 0, NULL, 6),
(7, 'Almenn brot', 10, 1, 1, '2002-11-13 15:45:14', '2013-02-24 22:25:58', 4, 3, 0, NULL, 7),
(8, 'Prósentur og vextir', 10, 1, 1, '2002-11-13 15:46:30', '2002-12-13 09:44:28', 4, 4, 0, NULL, 8),
(9, 'Hnitakerfið', 10, 1, 1, '2002-11-13 15:47:13', '2002-12-13 09:46:16', 4, 5, 0, NULL, 93),
(10, 'Unglingastig', 10, 1, 1, '2002-11-13 16:42:22', '2002-11-13 16:42:22', 0, 3, 0, NULL, 3),
(11, 'Hornafræði', 10, 1, 1, '2002-12-13 09:46:53', '2002-12-13 09:46:53', 4, 6, 0, NULL, 9),
(12, 'Rúm og flatarmál', 10, 1, 1, '2002-12-13 09:48:09', '2002-12-13 09:48:09', 4, 7, 0, NULL, 10),
(13, 'Tölfræði og líkindareikningur', 10, 1, 1, '2002-12-13 09:49:11', '2002-12-13 09:49:11', 4, 8, 0, NULL, 11),
(14, 'Tíminn', 10, 1, 1, '2002-12-13 09:49:59', '2002-12-13 09:50:18', 4, 9, 0, NULL, 12),
(15, 'Yfirlitsverkefni', 10, 0, 1, '2002-12-13 09:51:58', '2002-12-13 09:51:58', 4, 10, 0, NULL, 71),
(16, 'Algebra og reiknireglur', 10, 0, 1, '2002-12-13 09:53:28', '2002-12-13 09:53:28', 10, 1, 0, NULL, 72),
(17, 'Almenn brot', 10, 0, 1, '2002-12-13 09:54:33', '2002-12-13 09:54:33', 10, 2, 0, NULL, 73),
(18, 'Veldareglur', 10, 0, 1, '2002-12-13 09:55:46', '2002-12-13 09:55:46', 10, 3, 0, NULL, 74),
(19, 'Hornafræði', 10, 0, 1, '2002-12-13 09:56:15', '2002-12-13 09:57:21', 10, 4, 0, NULL, 75),
(20, 'Prósentur og vextir', 10, 0, 1, '2002-12-13 09:57:12', '2002-12-13 09:57:12', 10, 5, 0, NULL, 76),
(21, 'Tölfræði og líkindareikningur', 10, 0, 1, '2002-12-13 09:58:06', '2002-12-13 09:58:06', 10, 6, 0, NULL, 77),
(22, 'Jöfnur og ójöfnur', 10, 0, 1, '2002-12-13 09:59:23', '2002-12-13 09:59:23', 10, 7, 0, NULL, 78),
(23, 'Rúm og flatarmál', 10, 0, 1, '2002-12-13 10:00:14', '2002-12-13 10:00:14', 10, 8, 0, NULL, 79),
(24, 'Föll mengi og hnitakerfi', 10, 0, 1, '2002-12-13 10:00:43', '2002-12-13 10:00:43', 10, 9, 0, NULL, 80),
(25, 'Tími hraði vegalengd', 10, 0, 1, '2002-12-13 10:01:42', '2002-12-13 10:01:42', 10, 10, 0, NULL, 81),
(26, 'Framhaldsskólinn', 10, 0, 1, '2002-12-13 10:04:05', '2002-12-13 10:04:05', 0, 4, 0, NULL, 4),
(27, 'Veldareglur', 10, 0, 1, '2002-12-13 10:06:37', '2002-12-13 10:06:37', 26, 1, 0, NULL, 82),
(28, 'Almenn brot', 10, 0, 1, '2002-12-13 10:07:17', '2002-12-13 10:07:17', 26, 2, 0, NULL, 83),
(29, 'Hornafræði', 10, 0, 1, '2002-12-13 10:08:08', '2002-12-13 10:08:08', 26, 3, 0, NULL, 84),
(30, 'Algebra og reiknireglur', 10, 0, 1, '2002-12-13 10:08:35', '2002-12-13 10:08:35', 26, 4, 0, NULL, 85),
(31, 'Prósentur og vextir', 10, 0, 1, '2002-12-13 10:09:20', '2002-12-13 10:09:20', 26, 5, 0, NULL, 86),
(32, 'Tölfræði og líkindareikningur', 10, 0, 1, '2002-12-13 10:10:02', '2002-12-13 10:10:02', 26, 6, 0, NULL, 87),
(33, 'Jöfnur og ójöfnur', 10, 0, 1, '2002-12-13 10:10:40', '2002-12-13 10:10:40', 26, 7, 0, NULL, 88),
(34, 'Rúm og flatarmál', 10, 0, 1, '2002-12-13 11:41:55', '2002-12-13 11:41:55', 26, 8, 0, NULL, 89),
(35, 'Föll mengi og hnitakerfi', 10, 0, 1, '2002-12-13 11:42:34', '2002-12-13 11:42:34', 26, 9, 0, NULL, 90),
(36, 'Stærðfræðigreining', 10, 0, 1, '2002-12-13 11:43:06', '2002-12-13 11:43:06', 26, 10, 0, NULL, 91),
(37, 'Ýmis verkefni', 10, 0, 1, '2002-12-13 11:43:41', '2002-12-13 11:43:41', 26, 11, 0, NULL, 92),
(38, 'Röð aðgerða', 10, 1, 1, '2002-12-13 11:49:03', '0000-00-00 00:00:00', 5, 2, 0, NULL, 13),
(39, 'Mínus tölur', 10, 1, 1, '2002-12-13 11:50:34', '0000-00-00 00:00:00', 5, 3, 0, NULL, 14),
(40, 'Námundun', 10, 1, 1, '2002-12-13 11:51:16', '0000-00-00 00:00:00', 5, 4, 0, NULL, 15),
(41, 'Jöfnur grunnatriði', 10, 1, 1, '2002-12-13 11:52:11', '2002-12-13 11:52:11', 6, 1, 0, NULL, 16),
(42, 'Almenn brot grunnur', 10, 1, 1, '2002-12-13 11:53:01', '2002-12-13 11:53:01', 7, 1, 0, NULL, 17),
(43, 'Prósentur og vextir', 10, 1, 1, '2002-12-13 11:53:49', '2002-12-13 11:53:49', 8, 1, 0, NULL, 18),
(44, 'Hnitakerfið', 10, 1, 1, '2002-12-13 11:54:27', '2002-12-13 11:54:27', 9, 1, 0, NULL, 19),
(45, 'Grunnreglur um horn', 10, 1, 1, '2002-12-13 11:55:01', '2002-12-13 11:55:01', 11, 1, 0, NULL, 20),
(46, 'Mælieiningar', 10, 1, 1, '2002-12-13 11:55:58', '2002-12-13 11:55:58', 12, 1, 0, NULL, 21),
(47, 'Flatarmál', 10, 1, 1, '2002-12-13 20:00:00', '2002-12-13 20:00:00', 12, 2, 0, NULL, 22),
(48, 'Rúmmál', 10, 1, 1, '2002-12-13 20:00:37', '2002-12-13 20:00:37', 12, 3, 0, NULL, 23),
(49, 'Tölfræði', 10, 1, 1, '2002-12-13 20:01:20', '2002-12-13 20:01:20', 13, 1, 0, NULL, 24),
(50, 'Líkindareikningur', 10, 1, 1, '2002-12-13 20:01:51', '2002-12-13 20:01:51', 13, 2, 0, NULL, 45),
(51, 'Tími hraði vegalengd', 10, 1, 1, '2002-12-13 20:02:51', '2002-12-13 20:02:51', 14, 1, 0, NULL, 26),
(52, 'Yfirlitspróf', 10, 0, 1, '2002-12-13 20:04:07', '2002-12-13 20:04:07', 15, 1, 0, NULL, 27),
(53, 'Röð aðgerða', 10, 0, 1, '2002-12-13 20:04:48', '2002-12-13 20:04:48', 16, 1, 0, NULL, 28),
(54, 'Liðastærðir', 10, 0, 1, '2002-12-13 20:05:41', '2002-12-13 20:05:41', 16, 2, 0, NULL, 29),
(55, 'Einföldun stæða', 10, 0, 1, '2002-12-13 20:06:31', '2002-12-13 20:06:31', 16, 3, 0, NULL, 30),
(56, 'Þáttun liðastærða', 10, 0, 1, '2002-12-13 20:07:04', '2002-12-13 20:07:04', 16, 5, 0, NULL, 31),
(57, 'Röð aðgerða grunnur', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 1, 0, NULL, 95),
(58, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 57, 1, 0, NULL, 96),
(59, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 57, 2, 0, NULL, 97),
(60, 'Kynning 03', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 57, 3, 0, NULL, 98),
(61, 'Kynning 04', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 57, 4, 0, NULL, 99),
(62, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 38, 1, 0, NULL, 94),
(63, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 39, 1, 0, NULL, 100),
(64, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 39, 2, 0, NULL, 102),
(65, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 40, 1, 0, NULL, 103),
(66, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 41, 1, 0, NULL, 104),
(67, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 41, 2, 0, NULL, 105),
(68, 'Kynning 01', 10, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 42, 1, 0, NULL, 106),
(69, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 42, 2, 0, NULL, 107),
(70, 'Kynning 03', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 42, 3, 0, NULL, 108),
(71, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 43, 1, 0, NULL, 109),
(72, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 43, 2, 0, NULL, 110),
(73, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 44, 1, 0, NULL, 111),
(74, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 45, 1, 0, NULL, 112),
(75, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 46, 1, 0, NULL, 113),
(76, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 46, 2, 0, NULL, 114),
(77, 'Kynning 03', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 46, 3, 0, NULL, 115),
(78, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 47, 1, 0, NULL, 116),
(79, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 47, 2, 0, NULL, 117),
(80, 'Kynning 03', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 47, 3, 0, NULL, 118),
(81, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 48, 1, 0, NULL, 119),
(82, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 49, 1, 0, NULL, 120),
(83, 'Kynning 02', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 49, 2, 0, NULL, 121),
(84, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 50, 1, 0, NULL, 122),
(85, 'Kynning 01', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 51, 1, 0, NULL, 123),
(86, 'Almenn brot grunnur', 10, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 3, 0, NULL, 49),
(87, 'Kynning 01', 10, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 86, 1, 0, NULL, 106),
(88, 'Primary', 11, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, NULL, 2),
(89, 'Secondary', 11, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 2, 0, NULL, 4),
(90, 'Próf 01', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-24 22:28:05', 38, 1, 0, NULL, 130),
(91, 'Próf 01', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:46:21', 39, 1, 0, NULL, 125),
(92, 'Próf 01', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:45:14', 57, 1, 0, NULL, 126),
(93, 'Próf 02', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:45:32', 57, 2, 0, NULL, 127),
(94, 'Próf 03', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:45:42', 57, 3, 0, NULL, 128),
(95, 'Próf 04', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:45:48', 57, 4, 0, NULL, 129),
(96, 'Próf 02', 10, 1, 1, '0000-00-00 00:00:00', '2013-02-25 08:47:11', 39, 2, 0, NULL, 131),
(97, 'Próf 01', 10, 0, 1, '0000-00-00 00:00:00', '2013-02-25 08:47:50', 40, 1, 0, NULL, 132),
(98, 'Próf 01', 10, 0, 1, '0000-00-00 00:00:00', '2013-02-25 08:48:13', 41, 1, 0, NULL, 133),
(99, 'Próf 02', 10, 0, 1, '0000-00-00 00:00:00', '2013-02-24 22:29:16', 41, 2, 0, NULL, 134),
(100, 'Próf 03', 10, 0, 1, '0000-00-00 00:00:00', '2013-02-25 08:49:02', 42, 3, 0, NULL, 137),
(101, 'Próf 01', 10, 0, 1, '0000-00-00 00:00:00', '2013-02-24 22:30:57', 43, 1, 0, NULL, 138),
(102, 'Próf 01', 12, 0, 1, '2013-02-25 08:49:52', '2013-02-25 08:49:52', 42, 1, 0, NULL, 135),
(103, 'Próf 02', 12, 0, 1, '2013-02-25 08:51:01', '2013-02-25 08:51:01', 42, 2, 0, NULL, 136),
(104, 'Próf 02', 12, 0, 1, '2013-02-25 08:51:40', '2013-02-25 08:51:40', 43, 2, 0, NULL, 139),
(105, 'Próf 01', 12, 0, 1, '2013-02-25 08:52:28', '2013-02-25 08:52:28', 44, 1, 0, NULL, 140),
(106, 'Próf 01', 12, 0, 1, '2013-02-25 08:53:27', '2013-02-25 08:53:27', 45, 1, 0, NULL, 141),
(107, 'Próf 01', 12, 0, 1, '2013-02-25 08:54:27', '2013-02-25 08:54:27', 46, 1, 0, NULL, 142),
(108, 'Próf 02', 12, 1, 1, '2013-02-25 08:54:45', '2013-02-25 08:54:45', 46, 2, 0, NULL, 143),
(109, 'Próf 03', 12, 0, 1, '2013-02-25 08:55:07', '2013-02-25 08:55:07', 46, 3, 0, NULL, 144),
(110, 'Próf 01', 12, 0, 1, '2013-02-25 08:55:43', '2013-02-25 08:55:43', 47, 1, 0, NULL, 145),
(111, 'Próf 02', 12, 0, 1, '2013-02-25 08:56:02', '2013-02-25 08:56:02', 47, 2, 0, NULL, 146),
(112, 'Próf 03', 12, 0, 1, '2013-02-25 08:56:50', '2013-02-25 08:56:50', 47, 3, 0, NULL, 147),
(113, 'Próf 01', 12, 0, 1, '2013-02-25 08:57:20', '2013-02-25 08:57:20', 48, 1, 0, NULL, 148),
(114, 'Próf 01', 12, 0, 1, '2013-02-25 08:57:57', '2013-02-25 08:57:57', 49, 1, 0, NULL, 149),
(115, 'Próf 02', 12, 0, 1, '2013-02-25 08:58:20', '2013-02-25 08:58:20', 49, 2, 0, NULL, 150),
(116, 'Próf 01', 12, 0, 1, '2013-02-25 08:59:12', '2013-02-25 08:59:12', 50, 1, 0, NULL, 151),
(117, 'Próf 01', 12, 0, 1, '2013-02-25 08:59:48', '2013-02-25 08:59:48', 51, 1, 0, NULL, 152),
(118, 'Algebra and Calculation', 11, 0, 1, '2013-02-25 10:19:55', '2013-02-25 10:19:55', 88, 1, 0, NULL, 5),
(119, 'Equations and Inequality', 11, 0, 1, '2013-02-25 10:21:52', '2013-02-25 10:21:52', 88, 2, 0, NULL, 6),
(120, 'Fractions', 11, 0, 1, '2013-02-25 10:22:22', '2013-02-25 10:22:22', 88, 3, 0, NULL, 7),
(121, 'Percentages and Interest', 11, 0, 1, '2013-02-25 10:22:54', '2013-02-25 10:22:54', 88, 4, 0, NULL, 8),
(122, 'Coordinate system', 11, 0, 1, '2013-02-25 10:28:14', '2013-02-25 10:28:14', 88, 5, 0, NULL, 93),
(123, 'Trigonometry', 11, 0, 1, '2013-02-25 10:34:16', '2013-02-25 10:34:16', 88, 6, 0, NULL, 9),
(124, 'Geometry and Volume', 11, 0, 1, '2013-02-25 10:34:49', '2013-02-25 10:34:49', 88, 7, 0, NULL, 10),
(125, 'Statistics and Probability', 11, 0, 1, '2013-02-25 10:35:17', '2013-02-25 10:35:17', 88, 8, 0, NULL, 11),
(126, 'Time', 11, 0, 1, '2013-02-25 10:35:56', '2013-02-25 10:35:56', 88, 9, 0, NULL, 12),
(127, 'Order of Operations', 11, 0, 1, '2013-02-25 10:36:58', '2013-02-25 10:36:58', 118, 1, 0, NULL, 13),
(128, 'Negative Numbers', 11, 0, 1, '2013-02-25 10:37:40', '2013-02-25 10:37:40', 118, 2, 0, NULL, 14);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `NotificationId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(127) NOT NULL,
  `Text` varchar(400) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `Publish` tinyint(1) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`NotificationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`NotificationId`, `Title`, `Text`, `LanguageId`, `Publish`, `Created`, `Updated`, `Deleted`, `UserId`) VALUES
(4, 'O my friend', '<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, sans-serif; font-size: 10px;">O my friend -- but it is too much for my strength -- I sink under the weight of the splendour of these visions!</p>\n\n<p style="margin: 0px 0px 10px; padding: 0px; line-height: normal; color: rgb(102, 102, 102); font-family: Verdana, Geneva, san', 11, 0, '2002-03-13 13:46:19', '2002-08-13 12:19:08', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `PagesId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(127) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Deleted` bit(1) NOT NULL DEFAULT b'0',
  `BranchOrder` int(11) NOT NULL,
  PRIMARY KEY (`PagesId`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `Name_2` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=153 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`PagesId`, `Name`, `Created`, `Updated`, `Deleted`, `BranchOrder`) VALUES
(1, 'ys', '2002-04-13 10:33:14', '2002-05-13 09:36:15', b'1', 1),
(2, 'ms', '2002-04-13 10:33:38', '2002-05-13 09:28:13', b'1', 1),
(3, 'us', '2002-04-13 10:33:49', '2002-04-13 10:33:49', b'1', 1),
(4, 'fs', '2002-04-13 10:33:55', '2002-04-13 10:33:55', b'1', 1),
(5, 'stm01', '2002-09-13 13:48:24', '2002-09-13 13:48:24', b'1', 2),
(6, 'stm02', '2002-09-13 14:03:11', '2002-09-13 14:03:11', b'1', 2),
(7, 'stm03', '2002-09-13 14:03:27', '2002-09-13 14:03:27', b'1', 2),
(8, 'stm04', '2002-09-13 14:03:47', '2002-09-13 14:03:47', b'1', 2),
(9, 'stm06', '2002-09-13 14:04:10', '2002-09-13 14:04:10', b'1', 2),
(10, 'stm07', '2002-09-13 14:04:21', '2002-09-13 14:04:21', b'1', 2),
(11, 'stm08', '2002-09-13 14:04:32', '2002-09-13 14:04:32', b'1', 2),
(12, 'stm09', '2002-09-13 14:05:00', '2002-09-13 14:06:52', b'1', 2),
(13, 'stm01SC', '2002-11-13 18:01:17', '2002-11-13 18:01:17', b'1', 3),
(14, 'stm12SC', '2002-11-13 18:02:50', '2002-11-13 18:02:50', b'1', 3),
(15, 'st22SC', '2002-11-13 18:03:24', '2002-11-13 18:03:24', b'1', 3),
(16, 'stm08SC', '2002-11-13 18:04:01', '2002-11-13 18:04:01', b'1', 3),
(17, 'stm02SC', '2002-11-13 18:04:43', '2002-11-13 18:04:43', b'1', 3),
(18, 'stm03SC', '2002-11-13 18:07:20', '2002-11-13 18:07:20', b'1', 3),
(19, 'stm10SC', '2002-11-13 18:07:46', '2002-11-13 18:07:46', b'1', 3),
(20, 'stm13SC', '2002-11-13 18:08:19', '2002-11-13 18:08:19', b'1', 3),
(21, 'stm04SC', '2002-11-13 18:08:37', '2002-11-13 18:08:37', b'1', 3),
(22, 'stm05SC', '2002-11-13 18:11:02', '2002-11-13 18:11:02', b'1', 3),
(23, 'stm06SC', '2002-11-13 18:11:18', '2002-11-13 18:11:18', b'1', 3),
(24, 'st21SC', '2002-11-13 18:11:43', '2002-12-13 09:08:09', b'1', 3),
(25, 'st12SC', '2002-11-13 18:12:08', '2002-11-13 18:12:08', b'1', 3),
(26, 'stm09SC', '2002-11-13 18:13:05', '2002-11-13 18:13:05', b'1', 3),
(27, 'stm20SC', '2002-11-13 18:13:42', '2002-11-13 18:13:42', b'1', 3),
(28, 'st02SC', '2002-11-13 18:20:04', '2002-11-13 18:20:04', b'1', 3),
(29, 'st03SC', '2002-11-13 18:20:28', '2002-11-13 18:20:28', b'1', 3),
(30, 'st04SC', '2002-11-13 22:27:59', '2002-11-13 22:27:59', b'1', 3),
(31, 'st05SC', '2002-11-13 22:28:25', '2002-11-13 22:28:25', b'1', 3),
(32, 'st08SC', '2002-12-13 09:14:03', '2002-12-13 09:14:03', b'1', 3),
(33, 'su09SC', '2002-12-13 09:14:36', '2002-12-13 09:14:36', b'1', 3),
(34, 'su52SC', '2002-12-13 09:14:55', '2002-12-13 09:14:55', b'1', 3),
(35, 'st11SC', '2002-12-13 09:15:12', '2002-12-13 09:15:12', b'1', 3),
(36, 'st16SC', '2002-12-13 09:15:30', '2002-12-13 09:15:30', b'1', 3),
(37, 'su07SC', '2002-12-13 09:15:53', '2002-12-13 09:15:53', b'1', 3),
(38, 'st14SC', '2002-12-13 09:16:22', '2002-12-13 09:16:22', b'1', 3),
(39, 'st15SC', '2002-12-13 09:16:39', '2002-12-13 09:16:39', b'1', 3),
(40, 'st10SC', '2002-12-13 09:16:57', '2002-12-13 09:16:57', b'1', 3),
(41, 'su31SC', '2002-12-13 09:17:15', '2002-12-13 09:17:15', b'1', 3),
(42, 'su43SC', '2002-12-13 09:17:32', '2002-12-13 09:17:32', b'1', 3),
(43, 'su42SC', '2002-12-13 09:17:48', '2002-12-13 09:17:48', b'1', 3),
(44, 'stm09k2SC', '2002-12-13 09:18:50', '2002-12-13 09:18:50', b'1', 3),
(45, 'st01SC', '2002-12-13 09:19:39', '2002-12-13 09:19:39', b'1', 3),
(46, 'su50SC', '2002-12-13 09:19:58', '2002-12-13 09:19:58', b'1', 3),
(47, 'st24SC', '2002-12-13 09:20:29', '2002-12-13 09:20:29', b'1', 3),
(48, 'su51SC', '2002-12-13 09:20:50', '2002-12-13 09:20:50', b'1', 3),
(49, 'st18SC', '2002-12-13 09:21:09', '2002-12-13 09:21:09', b'1', 3),
(50, 'st07SC', '2002-12-13 09:21:25', '2002-12-13 09:21:25', b'1', 3),
(51, 'st06SC', '2002-12-13 09:21:44', '2002-12-13 09:21:44', b'1', 3),
(52, 'su06SC', '2002-12-13 09:22:02', '2002-12-13 09:22:02', b'1', 3),
(53, 'su54SC', '2002-12-13 09:24:32', '2002-12-13 09:24:32', b'1', 3),
(54, 'st20SC', '2002-12-13 09:24:48', '2002-12-13 09:24:48', b'1', 3),
(55, 'su30SC', '2002-12-13 09:25:26', '2002-12-13 09:25:26', b'1', 3),
(56, 'su57SC', '2002-12-13 09:26:24', '2002-12-13 09:26:24', b'1', 3),
(57, 'su55SC', '2002-12-13 09:26:46', '2002-12-13 09:26:46', b'1', 3),
(58, 'su56SC', '2002-12-13 09:27:04', '2002-12-13 09:27:04', b'1', 3),
(59, 'st19SC', '2002-12-13 09:28:14', '2002-12-13 09:28:14', b'1', 3),
(60, 'st23SC', '2002-12-13 09:28:31', '2002-12-13 09:28:31', b'1', 3),
(61, 'su44SC', '2002-12-13 09:28:54', '2002-12-13 09:28:54', b'1', 3),
(62, 'su58SC', '2002-12-13 09:29:12', '2002-12-13 09:29:12', b'1', 3),
(63, 'su60SC', '2002-12-13 09:29:26', '2002-12-13 09:29:26', b'1', 3),
(64, 'st13SC', '2002-12-13 09:30:09', '2002-12-13 09:30:09', b'1', 3),
(65, 'su53SC', '2002-12-13 09:31:47', '2002-12-13 09:31:47', b'1', 3),
(66, 'su68SC', '2002-12-13 09:33:36', '2002-12-13 09:33:36', b'1', 3),
(67, 'su62SC', '2002-12-13 09:34:06', '2002-12-13 09:34:06', b'1', 3),
(68, 'su64SC', '2002-12-13 09:34:20', '2002-12-13 09:34:20', b'1', 3),
(69, 'su41SC', '2002-12-13 09:34:37', '2002-12-13 09:34:37', b'1', 3),
(70, 'su66SC', '2002-12-13 09:34:50', '2002-12-13 09:34:50', b'1', 3),
(71, 'stm10', '2002-12-13 09:37:45', '2002-12-13 09:37:45', b'1', 2),
(72, 'st01', '2002-12-13 09:38:20', '2002-12-13 09:38:20', b'1', 2),
(73, 'st02', '2002-12-13 09:38:30', '2002-12-13 09:38:30', b'1', 2),
(74, 'st03', '2002-12-13 09:38:41', '2002-12-13 09:38:41', b'1', 2),
(75, 'st04', '2002-12-13 09:38:53', '2002-12-13 09:38:53', b'1', 2),
(76, 'st05', '2002-12-13 09:39:00', '2002-12-13 09:39:00', b'1', 2),
(77, 'st06', '2002-12-13 09:39:08', '2002-12-13 09:39:08', b'1', 2),
(78, 'st07', '2002-12-13 09:39:18', '2002-12-13 09:39:18', b'1', 2),
(79, 'st08', '2002-12-13 09:39:26', '2002-12-13 09:39:26', b'1', 2),
(80, 'st09', '2002-12-13 09:39:49', '2002-12-13 09:39:49', b'1', 2),
(81, 'st10', '2002-12-13 09:39:59', '2002-12-13 09:39:59', b'1', 2),
(82, 'su01', '2002-12-13 09:40:27', '2002-12-13 09:40:27', b'1', 2),
(83, 'su02', '2002-12-13 09:40:35', '2002-12-13 09:40:35', b'1', 2),
(84, 'su03', '2002-12-13 09:40:42', '2002-12-13 09:40:42', b'1', 2),
(85, 'su04', '2002-12-13 09:40:51', '2002-12-13 09:40:51', b'1', 2),
(86, 'su05', '2002-12-13 09:41:00', '2002-12-13 09:41:00', b'1', 2),
(87, 'su06', '2002-12-13 09:41:10', '2002-12-13 09:41:10', b'1', 2),
(88, 'su07', '2002-12-13 09:41:19', '2002-12-13 09:41:19', b'1', 2),
(89, 'su08', '2002-12-13 09:41:30', '2002-12-13 09:41:30', b'1', 2),
(90, 'su09', '2002-12-13 09:41:41', '2002-12-13 09:41:41', b'1', 2),
(91, 'su10', '2002-12-13 09:41:50', '2002-12-13 09:41:50', b'1', 2),
(92, 'su11', '2002-12-13 09:42:01', '2002-12-13 09:42:01', b'1', 2),
(93, 'stm05', '2002-12-13 09:45:32', '2002-12-13 09:45:32', b'1', 2),
(94, 'stm01k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(95, 'sty01SC', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 3),
(96, 'sty01k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(97, 'sty01k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(98, 'sty01k03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(99, 'sty01k04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(100, 'stm12k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(101, 'stm12k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(102, 'st19k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(103, 'st22k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(104, 'stm08k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(105, 'stm08k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(106, 'st18k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(107, 'st18k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(108, 'st18k03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(109, 'st13k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(110, 'st13k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(111, 'st10k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(112, 'stm13k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(113, 'st16k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(114, 'st16k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(115, 'st16k03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(116, 'st14k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(117, 'st14k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(118, 'st14k03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(119, 'stm06k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(120, 'st21k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(121, 'st21k02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(122, 'st12k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(123, 'stm09k01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'1', 4),
(124, 'st01p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(125, 'stm12p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(126, 'sty01p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(127, 'sty01p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(128, 'sty01p03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(129, 'sty01p04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(130, 'stm01p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(131, 'st19p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(132, 'st22p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(133, 'stm08p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(134, 'stm08p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(135, 'st18p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(136, 'st18p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(137, 'st18p03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(138, 'st13p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(139, 'st13p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(140, 'st10p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(141, 'stm13p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(142, 'st16p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(143, 'st16p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(144, 'st16p03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(145, 'st14p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(146, 'st14p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(147, 'st14p03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(148, 'stm06p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(149, 'st21p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(150, 'st21p02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(151, 'st12p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5),
(152, 'stm09p01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', b'0', 5);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `QuizId` int(11) NOT NULL AUTO_INCREMENT,
  `PagesId` int(11) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UserId` int(11) NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`QuizId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`QuizId`, `PagesId`, `LanguageId`, `Created`, `Updated`, `UserId`, `Deleted`) VALUES
(45, 130, 10, '2013-02-25 12:21:38', '2013-02-25 12:21:38', 1, 0),
(46, 131, 10, '2013-02-25 12:22:39', '2013-02-25 18:32:40', 1, 0),
(47, 126, 10, '2013-02-25 12:24:09', '2013-02-25 12:24:09', 1, 0),
(48, 127, 10, '2013-02-25 12:28:04', '2013-02-25 12:31:15', 1, 0),
(49, 125, 10, '2013-02-25 16:26:41', '2013-02-25 18:31:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quizoptions`
--

CREATE TABLE IF NOT EXISTS `quizoptions` (
  `QuizOptionsId` int(11) NOT NULL AUTO_INCREMENT,
  `Text` text NOT NULL,
  `CorrectAnswer` tinyint(1) NOT NULL,
  `Problem` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `PagesId` int(11) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `QuizId` int(11) NOT NULL,
  `DeletedId` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedIp` varchar(127) DEFAULT NULL,
  `DeletedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`QuizOptionsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=369 ;

--
-- Dumping data for table `quizoptions`
--

INSERT INTO `quizoptions` (`QuizOptionsId`, `Text`, `CorrectAnswer`, `Problem`, `Created`, `Updated`, `PagesId`, `LanguageId`, `QuizId`, `DeletedId`, `DeletedIp`, `DeletedDate`) VALUES
(297, '', 0, 1, '2013-02-25 12:21:39', '2013-02-25 12:21:39', 130, 10, 45, 0, NULL, NULL),
(298, '', 0, 2, '2013-02-25 12:21:40', '2013-02-25 12:21:40', 130, 10, 45, 0, NULL, NULL),
(299, '', 0, 3, '2013-02-25 12:21:41', '2013-02-25 12:21:41', 130, 10, 45, 0, NULL, NULL),
(300, '', 0, 4, '2013-02-25 12:21:42', '2013-02-25 12:21:42', 130, 10, 45, 0, NULL, NULL),
(301, '', 0, 5, '2013-02-25 12:21:43', '2013-02-25 12:21:43', 130, 10, 45, 0, NULL, NULL),
(302, '', 0, 6, '2013-02-25 12:21:43', '2013-02-25 12:21:43', 130, 10, 45, 0, NULL, NULL),
(303, '', 0, 7, '2013-02-25 12:21:44', '2013-02-25 12:21:44', 130, 10, 45, 0, NULL, NULL),
(304, '', 0, 8, '2013-02-25 12:21:46', '2013-02-25 12:21:46', 130, 10, 45, 0, NULL, NULL),
(305, '', 0, 9, '2013-02-25 12:21:47', '2013-02-25 12:21:47', 130, 10, 45, 0, NULL, NULL),
(306, '', 0, 10, '2013-02-25 12:21:48', '2013-02-25 12:21:48', 130, 10, 45, 0, NULL, NULL),
(307, '', 0, 1, '2013-02-25 12:22:40', '2013-02-25 18:32:40', 131, 10, 46, 0, NULL, NULL),
(308, '', 0, 2, '2013-02-25 12:22:41', '2013-02-25 12:22:41', 125, 10, 46, 0, NULL, NULL),
(309, '', 0, 3, '2013-02-25 12:22:42', '2013-02-25 12:22:42', 125, 10, 46, 0, NULL, NULL),
(310, '', 0, 4, '2013-02-25 12:22:43', '2013-02-25 12:22:43', 125, 10, 46, 0, NULL, NULL),
(311, '', 0, 5, '2013-02-25 12:22:44', '2013-02-25 12:22:44', 125, 10, 46, 0, NULL, NULL),
(312, '', 0, 6, '2013-02-25 12:22:44', '2013-02-25 12:22:44', 125, 10, 46, 0, NULL, NULL),
(313, '', 0, 7, '2013-02-25 12:22:45', '2013-02-25 12:22:45', 125, 10, 46, 0, NULL, NULL),
(314, '', 0, 8, '2013-02-25 12:22:46', '2013-02-25 12:22:46', 125, 10, 46, 0, NULL, NULL),
(315, '', 0, 9, '2013-02-25 12:22:46', '2013-02-25 12:22:46', 125, 10, 46, 0, NULL, NULL),
(316, '', 0, 10, '2013-02-25 12:22:47', '2013-02-25 12:22:47', 125, 10, 46, 0, NULL, NULL),
(317, '', 0, 1, '2013-02-25 12:24:10', '2013-02-25 12:24:10', 126, 10, 47, 0, NULL, NULL),
(318, '', 0, 2, '2013-02-25 12:24:11', '2013-02-25 12:24:11', 126, 10, 47, 0, NULL, NULL),
(319, '', 0, 3, '2013-02-25 12:24:12', '2013-02-25 12:24:12', 126, 10, 47, 0, NULL, NULL),
(320, '', 0, 4, '2013-02-25 12:24:13', '2013-02-25 12:24:13', 126, 10, 47, 0, NULL, NULL),
(321, '', 0, 5, '2013-02-25 12:24:14', '2013-02-25 12:24:14', 126, 10, 47, 0, NULL, NULL),
(322, '', 0, 6, '2013-02-25 12:24:15', '2013-02-25 12:24:15', 126, 10, 47, 0, NULL, NULL),
(323, '', 0, 7, '2013-02-25 12:24:16', '2013-02-25 12:24:16', 126, 10, 47, 0, NULL, NULL),
(324, '', 0, 8, '2013-02-25 12:24:16', '2013-02-25 12:24:16', 126, 10, 47, 0, NULL, NULL),
(325, '', 0, 9, '2013-02-25 12:24:18', '2013-02-25 12:24:18', 126, 10, 47, 0, NULL, NULL),
(326, '', 0, 10, '2013-02-25 12:24:19', '2013-02-25 12:24:19', 126, 10, 47, 0, NULL, NULL),
(327, '', 0, 1, '2013-02-25 12:28:05', '2013-02-25 12:31:15', 127, 10, 48, 0, NULL, NULL),
(328, '', 0, 2, '2013-02-25 12:28:06', '2013-02-25 12:31:15', 127, 10, 48, 0, NULL, NULL),
(329, '', 0, 3, '2013-02-25 12:28:07', '2013-02-25 12:31:16', 127, 10, 48, 0, NULL, NULL),
(330, '', 0, 4, '2013-02-25 12:28:08', '2013-02-25 12:31:16', 127, 10, 48, 0, NULL, NULL),
(331, '', 0, 5, '2013-02-25 12:28:09', '2013-02-25 12:31:16', 127, 10, 48, 0, NULL, NULL),
(332, '', 0, 6, '2013-02-25 12:28:10', '2013-02-25 12:31:17', 127, 10, 48, 0, NULL, NULL),
(333, '', 0, 7, '2013-02-25 12:28:11', '2013-02-25 12:31:17', 127, 10, 48, 0, NULL, NULL),
(334, '', 0, 8, '2013-02-25 12:28:12', '2013-02-25 12:31:17', 127, 10, 48, 0, NULL, NULL),
(335, '', 0, 9, '2013-02-25 12:28:13', '2013-02-25 12:31:17', 127, 10, 48, 0, NULL, NULL),
(336, '', 0, 10, '2013-02-25 12:28:14', '2013-02-25 12:31:17', 127, 10, 48, 0, NULL, NULL),
(337, '', 0, 1, '2013-02-25 12:31:15', '2013-02-25 12:31:15', 127, 10, 48, 0, NULL, NULL),
(338, '', 0, 1, '2013-02-25 12:31:15', '2013-02-25 12:31:15', 127, 10, 48, 0, NULL, NULL),
(339, '<p>=</p>\n', 0, 1, '2013-02-25 16:26:42', '2013-02-25 18:32:40', 131, 10, 49, 0, NULL, NULL),
(340, '<p>=</p>\n', 0, 2, '2013-02-25 16:26:44', '2013-02-25 18:31:01', 125, 10, 49, 0, NULL, NULL),
(341, '<p>&lt;</p>\n', 0, 3, '2013-02-25 16:26:44', '2013-02-25 18:31:01', 125, 10, 49, 0, NULL, NULL),
(342, '<p>9</p>\n', 0, 4, '2013-02-25 16:26:46', '2013-02-25 18:31:02', 125, 10, 49, 0, NULL, NULL),
(343, '<p>2</p>\n', 0, 5, '2013-02-25 16:26:47', '2013-02-25 18:31:02', 125, 10, 49, 0, NULL, NULL),
(344, '<p>2</p>\n', 0, 6, '2013-02-25 16:26:48', '2013-02-25 18:26:33', 131, 10, 49, 0, NULL, NULL),
(345, '<p>2</p>\n', 0, 7, '2013-02-25 16:26:50', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(346, '<p>0</p>\n', 0, 8, '2013-02-25 16:26:51', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(347, '<p>-2</p>\n', 0, 9, '2013-02-25 16:26:52', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(348, '<p>0</p>\n', 0, 10, '2013-02-25 16:26:53', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(349, '<p>&lt;</p>\n', 0, 1, '2013-02-25 16:41:35', '2013-02-25 18:32:39', 131, 10, 49, 0, NULL, NULL),
(350, '<p>&gt;</p>\n', 0, 1, '2013-02-25 16:41:36', '2013-02-25 18:32:40', 131, 10, 49, 0, NULL, NULL),
(351, '<p>&lt;</p>\n', 0, 2, '2013-02-25 18:17:01', '2013-02-25 18:31:01', 125, 10, 49, 0, NULL, NULL),
(352, '<p>&gt;</p>\n', 0, 2, '2013-02-25 18:17:01', '2013-02-25 18:31:01', 125, 10, 49, 0, NULL, NULL),
(353, '<p>=</p>\n', 0, 3, '2013-02-25 18:17:01', '2013-02-25 18:31:01', 125, 10, 49, 0, NULL, NULL),
(354, '<p>&gt;</p>\n', 0, 3, '2013-02-25 18:17:01', '2013-02-25 18:31:02', 125, 10, 49, 0, NULL, NULL),
(355, '<p>5</p>\n', 0, 4, '2013-02-25 18:17:02', '2013-02-25 18:31:02', 125, 10, 49, 0, NULL, NULL),
(356, '<p>-2</p>\n', 0, 5, '2013-02-25 18:17:02', '2013-02-25 18:26:33', 131, 10, 49, 0, NULL, NULL),
(357, '<p>7</p>\n', 0, 4, '2013-02-25 18:17:02', '2013-02-25 18:31:02', 125, 10, 49, 0, NULL, NULL),
(358, '<p>0</p>\n', 0, 5, '2013-02-25 18:17:02', '2013-02-25 18:26:33', 131, 10, 49, 0, NULL, NULL),
(359, '<p>0</p>\n', 0, 6, '2013-02-25 18:17:03', '2013-02-25 18:26:33', 131, 10, 49, 0, NULL, NULL),
(360, '<p>-8</p>\n', 0, 6, '2013-02-25 18:17:03', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(361, '<p>4</p>\n', 0, 7, '2013-02-25 18:17:03', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(362, '<p>6</p>\n', 0, 7, '2013-02-25 18:17:04', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(363, '<p>6</p>\n', 0, 8, '2013-02-25 18:20:33', '2013-02-25 18:26:34', 131, 10, 49, 0, NULL, NULL),
(364, '<p>-6</p>\n', 0, 8, '2013-02-25 18:20:34', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(365, '<p>-12</p>\n', 0, 9, '2013-02-25 18:26:35', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(366, '<p>2</p>\n', 0, 9, '2013-02-25 18:26:35', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(367, '<p>-16</p>\n', 0, 10, '2013-02-25 18:26:35', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL),
(368, '<p>16</p>\n', 0, 10, '2013-02-25 18:26:35', '2013-02-25 18:26:35', 131, 10, 49, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quizquestion`
--

CREATE TABLE IF NOT EXISTS `quizquestion` (
  `QuizQuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `Text` text NOT NULL,
  `Problem` int(11) NOT NULL,
  `PagesId` int(11) NOT NULL,
  `LanguageId` int(11) NOT NULL,
  `QuizId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UserId` int(11) NOT NULL,
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `DeletedIp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`QuizQuestionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=306 ;

--
-- Dumping data for table `quizquestion`
--

INSERT INTO `quizquestion` (`QuizQuestionId`, `Text`, `Problem`, `PagesId`, `LanguageId`, `QuizId`, `Created`, `Updated`, `UserId`, `Deleted`, `DeletedIp`) VALUES
(256, '', 1, 130, 10, 45, '2013-02-25 12:21:38', '2013-02-25 12:21:38', 1, 0, NULL),
(257, '', 2, 130, 10, 45, '2013-02-25 12:21:39', '2013-02-25 12:21:39', 1, 0, NULL),
(258, '', 3, 130, 10, 45, '2013-02-25 12:21:40', '2013-02-25 12:21:40', 1, 0, NULL),
(259, '', 4, 130, 10, 45, '2013-02-25 12:21:41', '2013-02-25 12:21:41', 1, 0, NULL),
(260, '', 5, 130, 10, 45, '2013-02-25 12:21:42', '2013-02-25 12:21:42', 1, 0, NULL),
(261, '', 6, 130, 10, 45, '2013-02-25 12:21:43', '2013-02-25 12:21:43', 1, 0, NULL),
(262, '', 7, 130, 10, 45, '2013-02-25 12:21:44', '2013-02-25 12:21:44', 1, 0, NULL),
(263, '', 8, 130, 10, 45, '2013-02-25 12:21:45', '2013-02-25 12:21:45', 1, 0, NULL),
(264, '', 9, 130, 10, 45, '2013-02-25 12:21:46', '2013-02-25 12:21:46', 1, 0, NULL),
(265, '', 10, 130, 10, 45, '2013-02-25 12:21:47', '2013-02-25 12:21:47', 1, 0, NULL),
(266, '', 1, 131, 10, 46, '2013-02-25 12:22:40', '2013-02-25 18:32:40', 1, 0, NULL),
(267, '', 2, 125, 10, 46, '2013-02-25 12:22:41', '2013-02-25 12:22:41', 1, 0, NULL),
(268, '', 3, 125, 10, 46, '2013-02-25 12:22:42', '2013-02-25 12:22:42', 1, 0, NULL),
(269, '', 4, 125, 10, 46, '2013-02-25 12:22:43', '2013-02-25 12:22:43', 1, 0, NULL),
(270, '', 5, 125, 10, 46, '2013-02-25 12:22:43', '2013-02-25 12:22:43', 1, 0, NULL),
(271, '', 6, 125, 10, 46, '2013-02-25 12:22:44', '2013-02-25 12:22:44', 1, 0, NULL),
(272, '', 7, 125, 10, 46, '2013-02-25 12:22:45', '2013-02-25 12:22:45', 1, 0, NULL),
(273, '', 8, 125, 10, 46, '2013-02-25 12:22:45', '2013-02-25 12:22:45', 1, 0, NULL),
(274, '', 9, 125, 10, 46, '2013-02-25 12:22:46', '2013-02-25 12:22:46', 1, 0, NULL),
(275, '', 10, 125, 10, 46, '2013-02-25 12:22:46', '2013-02-25 12:22:46', 1, 0, NULL),
(276, '', 1, 126, 10, 47, '2013-02-25 12:24:09', '2013-02-25 12:24:09', 1, 0, NULL),
(277, '', 2, 126, 10, 47, '2013-02-25 12:24:10', '2013-02-25 12:24:10', 1, 0, NULL),
(278, '', 3, 126, 10, 47, '2013-02-25 12:24:11', '2013-02-25 12:24:11', 1, 0, NULL),
(279, '', 4, 126, 10, 47, '2013-02-25 12:24:12', '2013-02-25 12:24:12', 1, 0, NULL),
(280, '', 5, 126, 10, 47, '2013-02-25 12:24:13', '2013-02-25 12:24:13', 1, 0, NULL),
(281, '', 6, 126, 10, 47, '2013-02-25 12:24:14', '2013-02-25 12:24:14', 1, 0, NULL),
(282, '', 7, 126, 10, 47, '2013-02-25 12:24:15', '2013-02-25 12:24:15', 1, 0, NULL),
(283, '', 8, 126, 10, 47, '2013-02-25 12:24:16', '2013-02-25 12:24:16', 1, 0, NULL),
(284, '', 9, 126, 10, 47, '2013-02-25 12:24:17', '2013-02-25 12:24:17', 1, 0, NULL),
(285, '', 10, 126, 10, 47, '2013-02-25 12:24:18', '2013-02-25 12:24:18', 1, 0, NULL),
(286, '', 1, 127, 10, 48, '2013-02-25 12:28:04', '2013-02-25 12:31:15', 1, 0, NULL),
(287, '', 2, 127, 10, 48, '2013-02-25 12:28:05', '2013-02-25 12:31:15', 1, 0, NULL),
(288, '', 3, 127, 10, 48, '2013-02-25 12:28:06', '2013-02-25 12:31:15', 1, 0, NULL),
(289, '', 4, 127, 10, 48, '2013-02-25 12:28:07', '2013-02-25 12:31:16', 1, 0, NULL),
(290, '', 5, 127, 10, 48, '2013-02-25 12:28:08', '2013-02-25 12:31:16', 1, 0, NULL),
(291, '', 6, 127, 10, 48, '2013-02-25 12:28:09', '2013-02-25 12:31:16', 1, 0, NULL),
(292, '', 7, 127, 10, 48, '2013-02-25 12:28:10', '2013-02-25 12:31:17', 1, 0, NULL),
(293, '', 8, 127, 10, 48, '2013-02-25 12:28:11', '2013-02-25 12:31:17', 1, 0, NULL),
(294, '', 9, 127, 10, 48, '2013-02-25 12:28:12', '2013-02-25 12:31:17', 1, 0, NULL),
(295, '', 10, 127, 10, 48, '2013-02-25 12:28:13', '2013-02-25 12:31:17', 1, 0, NULL),
(296, '<p>Veldu r&eacute;tt merki:&nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;?&nbsp;-5</p>\n', 1, 125, 10, 49, '2013-02-25 16:26:42', '2013-02-25 18:31:00', 1, 0, NULL),
(297, '<p>Veldu r&eacute;tt merki:&nbsp;-3 ?&nbsp;3</p>\n', 2, 125, 10, 49, '2013-02-25 16:26:43', '2013-02-25 18:31:00', 1, 0, NULL),
(298, '<p>Veldu r&eacute;tt merki:&nbsp;&nbsp;&nbsp;&nbsp;-5 ?&nbsp;-6</p>\n', 3, 125, 10, 49, '2013-02-25 16:26:44', '2013-02-25 18:31:01', 1, 0, NULL),
(299, '<p>Einn morguninn var tveggja stiga frost</p>\n\n<p><img alt="" src="http://www.rasmus.is/Is/T/U/stm12p01m02.gif" style="width: 45px; height: 291px;" />Eftir h&aacute;degi haf&eth;i hitinn h&aelig;kka&eth; um 7 stig.&nbsp; Hvert var hitastigi&eth;?</p>\n', 4, 125, 10, 49, '2013-02-25 16:26:45', '2013-02-25 18:31:01', 1, 0, NULL),
(300, '<p>&nbsp;Reikna&eth;u:&nbsp;&nbsp;5 - 7 =</p>\n', 5, 125, 10, 49, '2013-02-25 16:26:46', '2013-02-25 18:31:02', 1, 0, NULL),
(301, '<p>Reikna&eth;u:&nbsp;&nbsp;1 - 5 - 4 =&nbsp;</p>\n', 6, 131, 10, 49, '2013-02-25 16:26:48', '2013-02-25 18:26:33', 1, 0, NULL),
(302, '<p>Reikna&eth;u:&nbsp;4 - 5 + 3 =</p>\n', 7, 131, 10, 49, '2013-02-25 16:26:49', '2013-02-25 18:26:34', 1, 0, NULL),
(303, '<p>Hver er mismunurinn &aacute;&nbsp;3 og -3?</p>\n', 8, 131, 10, 49, '2013-02-25 16:26:50', '2013-02-25 18:26:34', 1, 0, NULL),
(304, '<p>Reikna&eth;u:&nbsp;&nbsp;-5 - 7=</p>\n', 9, 131, 10, 49, '2013-02-25 16:26:51', '2013-02-25 18:26:35', 1, 0, NULL),
(305, '<p>Reikna&eth;u:&nbsp;-4 - 4 - 4 - 4 =</p>\n', 10, 131, 10, 49, '2013-02-25 16:26:53', '2013-02-25 18:26:35', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
  `SchoolId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(127) NOT NULL,
  `SSN` varchar(127) NOT NULL,
  `Address` varchar(127) NOT NULL,
  `Address2` varchar(127) NOT NULL,
  `Postcode` varchar(127) NOT NULL,
  `City` varchar(127) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `Reference` varchar(127) NOT NULL,
  `InstitutionNr` varchar(127) NOT NULL,
  `PayerId` int(11) NOT NULL,
  `StudentsCounts` int(11) NOT NULL,
  `TeachersCounts` int(11) NOT NULL,
  `SubscriptionType` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  `DeletedIp` varchar(127) NOT NULL,
  PRIMARY KEY (`SchoolId`),
  UNIQUE KEY `SSN` (`SSN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `StudentId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(127) NOT NULL,
  `LastName` varchar(127) NOT NULL,
  `SSN` varchar(127) NOT NULL,
  `Deleted` int(11) NOT NULL,
  `DeletedIp` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`StudentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE IF NOT EXISTS `superadmin` (
  `SuperAdminId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(127) NOT NULL,
  `LastName` varchar(127) NOT NULL,
  `Email` varchar(127) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  `UsersId` int(11) NOT NULL,
  PRIMARY KEY (`SuperAdminId`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `TeacherId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(127) NOT NULL,
  `LastName` varchar(127) NOT NULL,
  `SSN` varchar(127) NOT NULL,
  `PhoneNumber` varchar(127) NOT NULL,
  `Email` varchar(127) NOT NULL,
  `Position` varchar(127) NOT NULL,
  `Created` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`TeacherId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(127) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `profile_fields` varchar(255) NOT NULL,
  `last_login` varchar(25) NOT NULL,
  `login_hash` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `Updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Username` (`username`),
  UNIQUE KEY `Email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `group`, `profile_fields`, `last_login`, `login_hash`, `created_at`, `Updated`) VALUES
(1, 'jondi', 'jondi@formastudios.com', '7Zq9gD72wlz38f4h8aIKie/HZBZ8tSIwk0G1pFlfveM=', 6, 'a:0:{}', '1361808606', '28e03a85c3b5daa01d99043f2071fc0a229e7015', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `usersschool`
--

CREATE TABLE IF NOT EXISTS `usersschool` (
  `UsersSchoolId` int(11) NOT NULL AUTO_INCREMENT,
  `UsersId` int(11) NOT NULL,
  `SchoolId` int(11) NOT NULL,
  PRIMARY KEY (`UsersSchoolId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usersstudent`
--

CREATE TABLE IF NOT EXISTS `usersstudent` (
  `UsersStudentId` int(11) NOT NULL AUTO_INCREMENT,
  `UsersId` int(11) NOT NULL,
  `StudentId` int(11) NOT NULL,
  PRIMARY KEY (`UsersStudentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
